# Accés SAML

Es prem el botó ![](saml_access.images/button.png){width="5%"} de la pàgina principal d'IsardVDI.

![](local_access.ca.images/local_access1.png){width="80%"}

A partir d'aquí, s'ha de continuar amb el procés d'inici de sessió conegut per l'usuari, que és diferent segons cada servidor SAML. 


## Inscripció per codi de registre

S'introdueix el codi proporcionat per un administrador o gestor de la plataforma i es prem el botó ![](oauth_access.ca.images/codi2.png){width="30%"}

![](oauth_access.ca.images/codi1.png){width="30%"}

I s'accedeix a la pàgina principal.

![](oauth_access.ca.images/home1.png){width="80%"}

!!! note "Nota"
    Una vegada autenticat l'usuari, **no** fa falta tornar a repetir el procés de registre. Al pròxim inici de sessió només s'ha de prémer en el botó ![](saml_access.images/button.png){width="5%"} i utilitzar les **credencials de SAML**.
