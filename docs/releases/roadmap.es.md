# Hoja de ruta

El equipo de IsardVDI es un grupo de desarrolladores y administradores de sistemas dedicados a proporcionar una solución de infraestructura de escritorio virtual potente y flexible. Estamos trabajando constantemente para mejorar el software y añadir nuevas características basadas en los comentarios de los usuarios y las tecnologías emergentes.

Estas son algunas de las características que vienen pronto:

1. **Redes personalizadas con OpenVSwitch**: IsardVDI está desarrollando un nuevo contenedor *isardvdi-ovs* que permitirá llevar *OpenVSwitch a IsardVDI a otro nivel. Con el objetivo de dar autonomía a los usuarios, esta función permitirá crear redes propias configuradas entre escritorios, con servicios opcionales como DHCP o conexión de red con NATO.

Podéis seguir su desarrollo principalmente a [https://gitlab.com/isard/isardvdi/-/merge_requests/1045](https://gitlab.com/isard/isardvdi/-/merge_requests/1045)

2. **Unidades personales**: Esta característica conectará automáticamente las unidades de almacenamiento de red desde una instancia del Nextcloud a los escritorios virtuales del usuario. También permite acceder a este almacenamiento fuera de los escritorios virtuales, desde la interfaz web de IsardVDI o incluso con la aplicación cliente Nextcloud en cualquier dispositivo. Con esta característica, la característica *escritorios temporales* tendrá más sentido, puesto que el usuario tendrá su propia unidad de red personal conectada a sus escritorios virtuales como almacenamiento persistente, permitiendo eliminar el escritorio virtual al pararse, reduciendo así el almacenamiento utilizado.

Podéis seguir su desarrollo en [https://gitlab.com/isard/isardvdi/-/merge_requests/1259](https://gitlab.com/isard/isardvdi/-/merge_requests/1259)

3. **Sistema de cola de acontecimientos**: Hay muchas características que vienen con el sistema de cola de acontecimientos. Esto permitirá tener contenedores *isard-storage* que consumirán las acciones añadidas por el usuario o el programador. Estas acciones pueden tardar mucho de tiempo y se gestionarán basándose en la prioridad de las colas y la carga del consumidor. Por ejemplo, haciendo una *sparsify de disco, instantánea, *convert, subida, bajada, ...

Podéis seguir su desarrollo en [https://gitlab.com/isard/isardvdi/-/merge_requests/1815](https://gitlab.com/isard/isardvdi/-/merge_requests/1815)

4. **Migración en tiempo real**: Cuando escaláis el IsardVDI a múltiplos hipervisors en infraestructura, queréis reducir los servidores en línea segundos la demanda. Para permitir tiempos más pequeños de parada de la hipervisor, los escritorios virtuales que se utilizan que estan iniciados en un hipervisor se migrarán en directo a otro, sin que casi el usuario lo note.

Se están desarrollando y probando otras muchas enmiendas de errores, mejoras y funcionalidades menores, como:

- Permitir roles avanzados de usuario para eliminar plantillas
- Acceder al escritorio virtual http/https desde el dominio principal IsardVDI
- Importar vuestros escritorios VirtualBox como escritorios IsardVDI
- Exportar el escritorio IsardVDI a KVM
- Instantáneas del escritorio virtual
- Editar los escritorios de despliegue
- Nuevo panel de estado del sistema y administrador
- Grupos de almacenamiento y funciones para mover los discos entre ellos
- Registros sobre las acciones de escritorio y usuarios
- Eventos de correo electrónico
- ...