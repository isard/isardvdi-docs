# Infrastructure setup

IsardVDI allows for different setups depending on your requirements. From easier one to the more complex scenario:

- **all-in-one**: One server with everything. The easier one, start with this.
- **hypervisors**: One all-in-one and other hypervisors, all with Internet access that can be setup at border router (different ports for video at each hypervisor).
- **hypervisors-standalone**: One all-in-one visible from Internet and other hypervisors without Internet access. Useful when you are not able to open more ports are your border NAT router
- **web-hypers**: One VPS/minimal host with the web+monitoring services and hypervisors, that can be standalone or full, depending on the Internet access they've got.
- **web-monitor-hypers**: One VPS host for web, another for monitoring and hypervisors, standalone or full.

## all-in-one

Just follow [Quick start guide](https://isard.gitlab.io/isardvdi-docs/install/configuration/)

## Hypervisors

NOTE: All servers should have /opt/isard shared between them. What technology you use for that it's up to you. We use NFS most of the times.

You can setup your border NAT router to redirect ports like this for example (all can be setup in cfgs):

- all-in-one
  - 80 TCP (http to https redirect and spice video)
  - 443 TCP (https and HTML5 videos)
  - 9999 TCP (RDP native video)
  - 443 UDP (Users wireguard VPN to connect to guests remotely)

- hypervisors
  - 808X/844X TCP (one X for each hypervisor not being the in the all-in-one)

Although this setup it's recommended if you have a public IP for each server to use the automatic LetsEncrypt certificates. Then you can open 80/443 to each hypervisor. If not (most of the situations) you have to open different NAT ports as stated before (808X/844X) and you'd better use a wilcard certificate or a buyed one (refer to certificates section to install yours) as the hypervisors won't renew it automatically, neither they will request it successfully.

Internally there is traffic that should go between them:

- all-in-one to hypervisors
  - 2022 TCP (Engine control of hypervisors)

- hypervisors to all-in-one
  - 4443 UDP (Wireguard Geneve VLANs tunnel for guests)
  - 6379 TCP (Redis tasks for isard-storage)

If you enable stats & logs then the remote hypervisors should also access stats & logs server in all-in-one:
  - 3100 TCP (Loki logs server)
  - 9090 TCP (Prometheus/Grafana stats server)


With that in mind, you can forward in your all in one ports to each docker service in all-in-one. We use different internal networks for this traffic if possible, but can be the same one:


    firewall-cmd --new-zone=intra --permanent
    firewall-cmd --zone=intra --add-source=172.31.2.0/24 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=3100:proto=tcp:toport=3100:toaddr=172.31.255.67 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=9090:proto=tcp:toport=9090:toaddr=172.31.255.68 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=6379:proto=tcp:toport=6379:toaddr=172.31.255.12 --permanent

And at hypervisors:

    firewall-cmd --new-zone=intra --permanent
    firewall-cmd --zone=intra --add-source=172.31.2.0/24 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=2022:proto=tcp:toport=2022:toaddr=172.31.255.17 --permanent

Note that for security reasons all those ports are not opened by default at yml with docker and should be forwarded in the firewall. You could also edit the yml and open them, but the build.sh will overwrite yml.

So, if you are mapping different video ports (808X/844X) to hypervisors you will need to forward to the corresponding container:

    firewall-cmd --zone=intra --add-forward-port=port=8080:proto=tcp:toport=80:toaddr=172.31.255.22 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=8443:proto=tcp:toport=443:toaddr=172.31.255.22 --permanent

Take into account also that the network used for all this SHOULD be blocked access to guests. To do this you SHOULD setup `BLACKLIST_IPTABLES=172.31.0.0/22` (or whatever internal ranges you want to block spaced by commas) in your hypervisors cfg or take it into account (I dont't know how as guest traffic is natted) in your infrastructure firewall.

The cfg at all-in-one can be like the default configuration for an all-in-one. the cfg for the hypervisors should setup the same SECRETS as setup in the all-in-one and at least this variables:

    DOMAIN=external-domain-name
    FLAVOUR=hypervisor
    LETSENCRYPT_EMAIL=info@isardvdi.com
    API_DOMAIN=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    STATIC_DOMAIN=external-all-in-one-domain-name # where the js/html for viewers to this hypers will be loaded by users browser
    VIEWER_SPICE=80 # If you only have one IP, then another port you opened
    VIEWER_BROWSER=443 # same
    VPN_DOMAIN=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    REDIS_HOST=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    PROMETHEUS_ADDRESS=http://172.31.2.10:9090 # Your all-in-one internal IP reacheable from this host (stats)
    LOKI_ADDRESS=http://172.31.2.10:3100 # Your all-in-one internal IP reacheable from this host (loki logs)


## Hypervisors standalone

NOTE: All servers should have /opt/isard shared between them. What technology you use for that it's up to you. We use NFS most of the times.

With this setup you have only one host with this opened ports at the NAT router:

- all-in-one
  - 80 TCP (http to https redirect and spice video)
  - 443 TCP (https and HTML5 videos)
  - 9999 TCP (RDP native video)
  - 443 UDP (Users wireguard VPN to connect to guests remotely)

And hypervisors that do not need any external Internet opened.

Internally there is traffic that should go between them:

- all-in-one to hypervisors
  - 2022 TCP (Engine control of hypervisors)
  - 5900-7899 TCP (Spice/HTML5 video ports)

- hypervisors to all-in-one
  - 4443 UDP (Wireguard Geneve VLANs tunnel for guests)
  - 6379 TCP (Redis tasks for isard-storage)

If you enable stats & logs then the remote hypervisors should also access stats & logs server in all-in-one:
  - 3100 TCP (Loki logs server)
  - 9090 TCP (Prometheus/Grafana stats server)

So you need to open at your all-in-one host:

    firewall-cmd --new-zone=intra --permanent
    firewall-cmd --zone=intra --add-source=172.31.2.0/24 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=3100:proto=tcp:toport=3100:toaddr=172.31.255.67 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=9090:proto=tcp:toport=9090:toaddr=172.31.255.68 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=6379:proto=tcp:toport=6379:toaddr=172.31.255.12 --permanent

And at hypervisors:

    firewall-cmd --new-zone=intra --permanent
    firewall-cmd --zone=intra --add-source=172.31.2.0/24 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=2022:proto=tcp:toport=2022:toaddr=172.31.255.17 --permanent
    firewall-cmd --zone=intra --add-forward-port=port=5900-7899:proto=tcp:toport=5900-7899:toaddr=172.31.255.17 --permanent

Note that for security reasons all those ports are not opened by default at yml with docker and should be forwarded in the firewall. You could also edit the yml and open them, but the build.sh will overwrite yml.

Take into account also that the network used for all this SHOULD be blocked access to guests. To do this you SHOULD setup `BLACKLIST_IPTABLES=172.31.2.0/22` in your hypervisors cfg or take it into account (I dont't know how as guest traffic is natted) in your infrastructure firewall.

The cfg at all-in-one can be like the default configuration for an all-in-one, except that now it will act also as a proxy for videos going to other hypervisors, so in it's cfg you should allow the hypervisors IP (and isard-hypervisor itself also) where the video proxy should redirect all the video traffic to guests running in those hypervisors:

    VIDEO_HYPERVISOR_HOSTNAMES=isard-hypervisor,172.31.2.11,172.31.2.12

The cfg for the hypervisors should setup the same SECRETS as setup in the all-in-one and at least this variables:

    DOMAIN=internal-hyper-ip # reacheable from all-in-one
    FLAVOUR=hypervisor-standalone
    API_DOMAIN=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    STATIC_DOMAIN=external-all-in-one-domain-name # where the js/html for viewers to this hypers will be loaded by users browser
    VIEWER_SPICE=80
    VIEWER_BROWSER=443
    VPN_DOMAIN=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    REDIS_HOST=172.31.2.10 # Your all-in-one internal IP reacheable from this host
    PROMETHEUS_ADDRESS=http://172.31.2.10:9090 # Your all-in-one internal IP reacheable from this host (stats)
    LOKI_ADDRESS=http://172.31.2.10:3100 # Your all-in-one internal IP reacheable from this host (loki logs)

## Web - Hypervisors

NOTE: All hypervisors should have /opt/isard shared between them. What technology you use for that it's up to you. We use NFS most of the times.

Well, you setup the web flavour at your VPS and follow any all-in-one setup.

Of course, hypervisors now should have their own public IP, so the configuration is like the `hypervisor` flavour explained before.

Firewall should follow also previous flows between web (like all-in-one's) and hypervisors.

## Web - Monitor - Hypervisors

NOTE: All servers should have /opt/isard shared between them. What technology you use for that it's up to you. We use NFS most of the times.

Like before, but this time you don't setup stats in your web server and you setup flavour to monitor in your monitor host. The hypervisors as stated before.

The stats/loki variables now should point to your monitor host in web and hypervisor hosts. And the stats ports now should be opened at monitor host instead of web host.

For the rest it's pretty much the same.