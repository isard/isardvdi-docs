# Despliegues

!!! Info
    Esta sección es solo para **administradores**, **gestores** y usuarios **avanzados**

Los despliegues son una herramienta potente que permite a los usuarios avanzados crear un conjunto de escritorios basados en una única plantilla. Todos los usuarios incluidos en un despliegue tendrán acceso a su propio escritorio único.

Con los despliegues, se pueden crear y gestionar varios escritorios simultáneamente para todos los usuarios seleccionados. Como creador, se puede gestionar fácilmente todos los escritorios en un solo lugar a través de la sección Despliegues. Incluso existe la posibilidad de interactuar con cada escritorio y ver todas las pantallas simultáneamente.

## Como crear despliegues

Para crear un despliegue, al apartado ![Botón superior de despliegues](./deployments.es.images/deployments1.png){width="11%"} se pulsa el botón ![Nuevo despliegue](./deployments.es.images/deployments3.png){width="11%"}

Se dirigirá a un formulario donde introducir la información necesaria para el nuevo despliegue, incluyendo el nombre del despliegue, el nombre del escritorio que verán los usuarios y una breve descripción.

![formulario de despliegue nuevo](./deployments.es.images/deployments6.png)

Allí, se tiene que seleccionar una plantilla desde donde crear los escritorios. Una vez creado, los escritorios se harán visibles para los usuarios, pulsando en ![no visible / visible](./deployments.es.images/deployments5.png)

También se debe especificar los usuarios que se tienen que incluir en el despliegue. En la sección final del formulario, se puede seleccionar el grupo necesario de usuarios, así como cualquier usuario adicional.

La última sección son las opciones avanzadas. Aquí podéis cambiar los visores de los escritorios, el hardware, la imagen de la miniatura y los permisos de usuario.

!!! important Importante
        1. El **propietario** del despliegue es **el único** que puede editar su escritorio **individualmente** sin que afecte a los demás escritorios.
        2. También se creará un escritorio para vuestro usuario dentro del despliegue.


## Opciones de un despliegue

### Permisos de usuario

Aquí puedes editar las acciones que el usuario puede hacer con su escritorio.

![Selector de permisos](./deployments.es.images/deployments40.png)

#### Recrear escritorio

Esta opción permite al usuario recrear su escritorio. Esto significa que el usuario podrá restablecer su escritorio al estado inicial en el que se creó.

Se mostrará al usuario como un botón ![Botón de recrear escritorio](./deployments.images/deployments31.png) en el desplegable del escritorio.

![Recreate desktop user](./deployments.es.images/deployments41.png)

### Establecer copropietarios

!!! Warning "Cuidado"
    Los usuarios establecidos como copropietarios tendrán los mismos permisos que el propietario del despliegue. Podrán iniciar y parar escritorios, ver los escritorios y acceder a la configuración del despliegue. **No podrán borrar el despliegue ni cambiar el propietario y copropietarios**.

Para establecer copropietarios, se pulsa el botón ![botón de copropietarios](./deployments.images/deployments29.png). Aparecerá una ventana de diálogo donde se pueden seleccionar los usuarios que se quieren establecer como copropietarios.

![diálogo de copropietarios](./deployments.es.images/deployments39.png)

### Inicio y parada

Para acceder al despliegue, se pulsa en el nombre del despliegue.

![lista de despliegues](./deployments.es.images/deployments7.png)

Para iniciar un escritorio, se pulsa el botón ![Iniciar](./deployments.ca.images/deployments8.png)

Una vez iniciado, detectará la "IP" del escritorio (si tiene), el "Estado" (si está iniciado o parado), y el tipo de visor se puede seleccionar pulsando el menú desplegable:

![lista desplegable de los visores](./deployments.es.images/deployments10.png){width="30%"}

Para parar un escritorio iniciado, simplemente se pulsa en el botón ![parar](./deployments.es.images/deployments12.png) que aparecerá. Esto cambiará su estado a "Parando". Si bien se puede utilizar la opción "Forzar parada" para pararlo inmediatamente, pero ésto puede provocar problemas de cierre.

Para parar todos los escritorios a la vez, se pulsa el icono ![icono de parada](./deployments.ca.images/deployments16.png)

### Hacer visible/invisible

Al hacer visible o invisible el despliegue, se determina si los usuarios que tienen acceso al mismo podrán ver y acceder a su escritorio, o no.

#### Visible

Para hacerlo visible, hacer clic en el botón ![botón de ojo tachado](./deployments.ca.images/deployments23.png)

Aparecerá una ventana de diálogo

![hacer visible](./deployments.es.images/deployments20.png)

Una vez "Visible" es seleccionado, el despliegue se volverá de color verde

![despliegue verde](./deployments.es.images/deployments21.png)

#### Invisible

Para hacerlo invisible, hacer clic en el botón ![botón ojo abierto](./deployments.ca.images/deployments26.png)

Aparecerá una ventana de diálogo

![hacer invisible](./deployments.es.images/deployments23.png)

Hay dos opciones para elegir:

- **No parar escritorios**: Esta opción hará que el despliegue sea invisible para el usuario. Sin embargo, si un usuario está utilizando actualmente su escritorio en el momento en que se hace invisible, todavía podrán utilizarlo hasta que se decida pararlo. Una vez parado, el usuario no podrá acceder de nuevo hasta que el despliegue se vuelva a hacer visible.

- **Parar escritorios**: Esta opción también hará invisible el despliegue. Sin embargo, si cualquier usuario ya está utilizando su escritorio, se apagará inmediatamente.

Una vez seleccionado "Invisible", el despliegue se volverá de color rojo

![despliegue rojo](./deployments.es.images/deployments18.png)

### Videowall

Videowall es una función que permite ver todas las pantallas de los escritorios de un despliegue particular simultáneamente y en tiempo real. También permite interactuar con cualquiera de los escritorios mostrados.

!!! Warning "Cuidado"
    Entrar dentro de la pantalla de otro usuario tomará el control del usuario propietario.

!!! Danger "Aviso legal"
    Se requiere el permiso del propietario antes de acceder a su escritorio. El acceso no autorizado puede tener consecuencias legales.

Para acceder al Videowall, hacer clic al botón ![icono del videowall](./deployments.ca.images/deployments18.png)

Se redirigirá a una página donde poder ver los escritorios de todos los usuarios.

![pantalla del videowall](./deployments.es.images/deployments16.png)

Para interactuar con un escritorio, hacer clic al botón ![maximiza](./deployments.ca.images/deployments21.png) que redirigirá a una vista a pantalla completa del escritorio seleccionado.

![pantalla completa del videowall](./deployments.es.images/deployments17.png)

!!! Warning "Visores RDP"
    En Videowall no se mostraran escritorios que utilicen visores RDP.


### Descargar fichero de visores directos

En este archivo se muestra el listado de usuarios incluidos en el despliegue, así como las URLs correspondientes a los visores de sus respectivos escritorios.:

Para lograr esto, se ha de hacer clic en el botón ![icono de descarga](./deployments.ca.images/deployments34.png)

Aparecerá una notificación:

![notificación](./deployments.es.images/deployments27.png)

Confirmar la notificación para descargar el fichero CSV.

Este fichero contiene cada nombre de usuario, su nombre, su correo electrónico y un enlace que redirige al escritorio del usuario.

![contenido del fichero csv](./deployments.ca.images/deployments36.png)


### Recrear despliegue

La función "Recrear despliegue" tiene como objetivo realizar cambios en el despliegue mediante la adición o eliminación de escritorios, en base a las modificaciones realizadas en los permisos del usuario. En caso de que se agregue o elimine un usuario, esta función actualizará el despliegue incorporando o eliminando los escritorios correspondientes.

Para hacer esto, hacer clic en el botón ![icono recrear](./deployments.ca.images/deployments31.png)

![notificación](./deployments.es.images/deployments25.png)

### Editar despliegue

Para editar un despliegue, se puede hacer de dos maneras pulsando el icono ![](../advanced/deployments.ca.images/deployments9.png):

1. Desde el listado de despliegues.

    ![](../advanced/deployments.es.images/deployments29.png){width="80%"}

2. Desde la vista de gestión de despliegues.

    ![](../advanced/deployments.es.images/deployments30.png){width="80%"}

Y redirigirá a la página de edición.

En esta página se puede editar todos los elementos (los mismos que se tiene al crear el despliegue)

![](../advanced/deployments.es.images/deployments33.png){width="80%"}

### Editar usuarios

Para editar los usuarios del despliegue, se puede hacer de dos maneras pulsando el icono ![](../advanced/deployments.ca.images/deployments10.png) 

1. Desde el listado de despliegues.

    ![](../advanced/deployments.es.images/deployments31.png){width="80%"}

2. Desde la vista de gestión de despliegues.

    ![](../advanced/deployments.es.images/deployments32.png){width="80%"}

Y saldrá una ventana donde se podrán asignar los grupos y/o usuarios.

![](../advanced/deployments.es.images/deployments34.png){width="80%"}

### Borrar

#### Despliegue

Para borrar un despliegue, se puede hacer de dos maneras pulsando el icono ![icono de la papelera](./deployments.ca.images/deployments37.png) 

!!! note Nota
        Se eliminarán todos los escritorios creados.

1. Desde el listado de despliegues.

    ![](../advanced/deployments.es.images/deployments35.png){width="80%"}

2. Desde la vista de gestión de despliegues.

    ![](../advanced/deployments.es.images/deployments36.png){width="80%"}

Y saldrá un mensaje dónde de si estás de acuerdo al borrar el despliegue.

![](../advanced/deployments.es.images/deployments37.png){width="30%"}

#### Usuarios del despliegue

Para borrar un usuario de un despliegue, se pulsa el icono ![icono de la papelera](./deployments.ca.images/deployments37.png) 

![](../advanced/deployments.es.images/deployments38.png){width="80%"}

!!! note Nota
        Para recuperar el usuario borrado, se tiene que pulsar el botón de [recrear](../advanced/deployments.es.md/#recrear-despliegue)