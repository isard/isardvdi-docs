# Plantilles

Una plantilla és un escriptori preconfigurat. El seu disc no es pot modificar, de manera que no es poden iniciar com escriptoris, però els seus paràmetres es poden personalitzar per satisfer les necessitats de cada usuari.

Les plantilles estan dissenyades per ser compartides amb altres usuaris perquè puguin crear els seus propis escriptoris.

Aquest és un exemple que il·lustra la relació de les plantilles i els discs:

**1.** Es crea un escriptori amb el disc d'emmagatzematge **D1**.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1]):::dk
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
  classDef dt stroke-width:2px
```

**2.** A continuació, es crea una plantilla a partir d'aquest escriptori. En crear la plantilla, el disc **D1** s'associarà amb la plantilla nova. Al mateix temps, es fa una còpia de **D1** i s'anomena **D1'**. Aquest disc copiat serà utilitzat per l'escriptori original en el futur, de manera que qualsevol canvi fet al disc original no l'afectarà.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.** Si es crea un escriptori nou a partir d'aquesta plantilla, es crea un disc nou **D2** per a l'escriptori nou. Aquest disc **D2** conté els canvis que es faran a l'escriptori respecte al disc de la plantilla **D1**. En altres paraules, **D2** està enllaçat a **D1** i l'escriptori obtindrà la seva informació de **D1** en el moment d'iniciar-lo.

Ës a dir, **D2** només conté els canvis fets a **D1** que són rellevants per a l'escriptori nou, i els dos discs romanen connectats entre si.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> dt2(Escriptori):::dt
  dt2 -.- dk3([D2]):::dk
  dk3 -- depèn de --> dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.** En duplicar una plantilla, no es crearà cap disc nou. En lloc d'això, la nova plantilla farà servir el mateix disc **D1** que la plantilla original.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> tp2(Plantilla):::tp
  tp2 -.- dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

Entendre la relació entre plantilles, escriptoris i discs és important per poder gestionar millor la infraestructura d'escriptoris virtual.

## Crear 

Per a crear una plantilla a partir d'un escriptori es prem la icona següent:

![](./templates.ca.images/templates1.png){width="80%"}

Se li pot assignar qualsevol nom/descripció, triar si es vol habilitar/deshabilitar (fer-la visible/invisible), i compartir-la amb grups/usuaris

![](./templates.ca.images/templates3.png){width="80%"}


## Les teves plantilles

Per a veure les plantilles que has creat, el docent s'ha de dirigir a l'apartat de les seves plantilles personals.

![](./templates.ca.images/templates4.png){width="80%"}

### Editar

Per a editar una plantilla, a l'apartat **Les teves plantilles** es prem la icona ![](./templates.es.images/templates10.png){width="3%"}, on redirigeix a la pàgina on poder editar la informació de la plantilla (mateix formulari i opcions com quan s'**[edita un escriptori](../user/edit_desktop.ca.md)**).

### Compartir

Per a compartir una plantilla, a l'apartat **Les teves plantilles** es prem la icona ![](./templates.es.images/templates13.png){width="3%"} i sortirà una finestra on es podran assignar els grups i/o usuaris.

![](./templates.ca.images/templates10.png){width="60%"}

### Convertir a escriptori

!!! Danger "Revisions de plantilles"
    Convertir una plantilla en un escriptori **eliminarà tots els escriptoris** creats a partir d'aquesta. Per fer una revisió de la plantilla, es recomana crear un escriptori a partir de la plantilla, fer els canvis necessaris i després fer una plantilla d'aquest escriptori.

Per a convertir una plantilla en un escriptori, a l'apartat **Les teves plantilles** es prem la icona ![](./templates.images/templates20.png){width="3%"}

![](./templates.ca.images/templates22.png){width="80%"}

**Per a plantilles sense dependències:**

Quan una plantilla no té dependències, això vol dir que no s'han creat escriptoris d'altres usuaris basats en aquesta plantilla, és possible convertir-la sense problemes en un escriptori. En aquest cas, si tens escriptoris fets amb aquesta plantilla, aquests s'esborraran permanentment i la plantilla es convertirà en un escriptori.

![](./templates.ca.images/templates23.png){width="80%"}
![](./templates.ca.images/templates24.png){width="80%"}

**Per a plantilles amb dependències:**

Si una plantilla té dependències, cosa que indica que s'han creat escriptoris d'altres usuaris que s'originen a partir d'aquesta plantilla, cal una acció diferent. En aquest cas, cal notificar al mànager o administrador corresponent sobre la intenció de convertir la plantilla en un escriptori. El mànager o administrador prendrà les mesures adequades per avaluar la situació i, si escau, procedir amb la conversió de la plantilla en un escriptori, eliminant els escriptoris d'altres usuaris en el procés.

![](./templates.ca.images/templates25.png){width="80%"}

Com a usuari **advanced**, si no tens els permisos necessaris per accedir a les dependències, aquestes es mostraran ocultes i podràs veure la quantitat de dependències. Si ets un usuari **manager**, indicarà noms d'usuaris i escriptoris de la teva pròpia categoria, mentre que si ets un usuari **administrador**, tindràs accés a la informació de totes les categories.

### Fer visible/invisible

Per modificar la visibilitat d'una plantilla, a l'apartat **Les teves plantilles**, es prem la icona ![](./templates.ca.images/templates15.png){width="3%"} o la icona ![](./templates.ca.images/templates16.png){width="3%"} depenent de la visibilitat actual d'aquesta.

**L'estat de l'ull determinarà la visibilitat de la plantilla**:

- Ull obert i botó blau ![](./templates.ca.images/templates15.png){width="3%"}: plantilla visible
- Ull negat i botó gris ![](./templates.ca.images/templates16.png){width="3%"}: plantilla invisible

Apareixerà un formulari on acceptar o negar el canvi de la visibilitat de la plantilla:

![](./templates.ca.images/templates17.png){width="25%"}
![](./templates.ca.images/templates18.png){width="25%"}

### Esborrar

!!! Danger "Esborrar plantilles"
    Quan es crea una plantilla des d'un escriptori, es duplica el seu disc. En generar un escriptori nou a partir d'una plantilla, es crea un disc addicional a l'emmagatzematge que depèn del disc de la plantilla original. Els **usuaris advanced només poden esborrar les seves pròpies plantilles**, si hi ha **dependències** amb altres usuaris, s'haurà d'informar l'usuari manager/administrador.

Per esborrar una plantilla es prem la icona ![](./media.es.images/media12.png){width="4%"}

![](./templates.ca.images/templates19.png){width="80%"}

La gestió de plantilles al sistema pot variar segons la presència de dependències. Depenent de si la plantilla té dependències o no, es generarà una notificació específica.

**Per a plantilles sense dependències:**

Quan una plantilla no té dependències, això vol dir que no s'han creat escriptoris d'altres usuaris basats en aquesta plantilla, és possible eliminar-la sense problemes. En aquest cas, es pot procedir amb l'eliminació de la plantilla directament.

![](./templates.ca.images/templates20.png){width="80%"}

**Per a plantilles amb dependències:**

Si una plantilla té dependències, cosa que indica que s'han creat escriptoris d'altres usuaris que s'originen a partir d'aquesta plantilla, cal una acció diferent. En aquest cas, cal notificar al mànager o administrador corresponent sobre la intenció d'eliminar la plantilla. El mànager o administrador prendrà les mesures adequades per avaluar la situació i, si escau, procedir amb l'eliminació de la plantilla, garantint que els escriptoris d'altres usuaris es vegin afectats o no en el procés.

Com a usuari **advanced**, si teniu dependències, ho **indicarà amb un "--"**, que es mostra quan no es compta amb els permisos necessaris per accedir a la informació dels usuaris. Com a usuari **manager**, indicarà noms d'usuaris i escriptoris de la teva pròpia categoria, mentre que sent usuari **administrador**, es tindrà accés a informació de totes les categories.

![](./templates.ca.images/templates21.png){width="80%"}


## Compartides amb tu

En aquest apartat es poden veure les plantilles que han estat compartides amb el teu usuari.

![](./templates.ca.images/templates6.png){width="80%"}

### Duplicar plantilla

Per a duplicar una plantilla i fer-la teva, a l'apartat **Plantilles compartides amb tu**, es selecciona la plantilla prement l'icona ![](./templates.images/templates14.png){width="3%"}

Es redirigeix a la pàgina on poder duplicar-la.

![](./templates.ca.images/templates14.png){width="80%"}

!!! Info "Important"
    La duplicació d'una plantilla compartida crea una **còpia** on l'usuari és el propietari. Això li permet **personalitzar** la plantilla, incloent-hi la modificació dels usuaris amb els quals es comparteix.

