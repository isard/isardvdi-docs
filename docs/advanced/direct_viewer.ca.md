# Visor directe

S'utilitza per a compartir un escriptori mitjançant un enllaç directe a aquest, **sense necessitat** de que l'usuari estigui **registrat** a la plataforma.

Es prem el botó ![](direct_viewer.es.images/visor_directo3.png) de l'escriptori que es vulgui compartir per enllaç

![](direct_viewer.ca.images/visor_directe1.png){width="80%"}

Sortirà una finestra de diàleg on s'ha de seleccionar la casella perquè aparegui l'enllaç i poder copiar-lo.

![](direct_viewer.ca.images/visor_directe3.png){width="80%"}
