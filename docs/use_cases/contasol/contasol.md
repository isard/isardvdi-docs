# Contasol (client)

This guide explains how to install **Contasol** software to the IsardVDI platform. 

## Pre-installation

To start the installation of Contasol, you can use any of our available templates for creating a virtual desktop. These templates provide you with a starting point to configure your work environment and begin installing Contasol software quickly and efficiently.

In this guide, a Windows 11 template is used.

## Installation

1. Download the software [**Contasol**](https://www.sdelsol.com/). To download it from the official website you need to have an account and be validated. In the help options you have the link to go to the download center. In this case you have access to Contasol, but it could be NominaSol or Factusol or any other.

    ![](./contasol.images/contasol1.png){width="45%"}
    ![](./contasol.images/contasol2.png){width="45%"}
    ![](./contasol.images/contasol3.png){width="28%"}
    ![](./contasol.images/contasol4.png){width="63%"}

2. Download the file and begin the installation.

    ![](./contasol.images/contasol7.png){width="45%"}
    ![](./contasol.images/contasol8.png){width="45%"}
    ![](./contasol.images/contasol9.png){width="45%"}
    ![](./contasol.images/contasol10.png){width="45%"}
    ![](./contasol.images/contasol11.png){width="45%"}
    ![](./contasol.images/contasol12.png){width="54%"}
    ![](./contasol.images/contasol13.png){width="45%"}

3. When we run the program for the first time, it tells us that the date format is not correct

    ![](./contasol.images/contasol14.png){width="32%"}
    ![](./contasol.images/contasol15.png){width="45%"}

4. Go to the control panel to add the format requested by Contasol. Within the dashboard we select 'Change number, date or time formats'

    ![](./contasol.images/contasol16.png){width="34%"}
    ![](./contasol.images/contasol17.png){width="60%"}

5. This is the current format that will have to be changed by clicking on "Additional settings"

    ![](./contasol.images/contasol18.png){width="45%"}
    ![](./contasol.images/contasol19.png){width="39%"}

6. Put the appropriate format in the 'Short Data' box and click on 'apply'. Now you will have a new format that can be selected.

    ![](./contasol.images/contasol20.png){width="38%"}
    ![](./contasol.images/contasol21.png){width="45%"}

7. Start Contasol again. The Microsoft Access database engine will have to be installed

    ![](./contasol.images/contasol23.png){width="50%"}
    ![](./contasol.images/contasol24.png){width="29%"}
    ![](./contasol.images/contasol25.png){width="45%"}
    ![](./contasol.images/contasol26.png){width="45%"}
    ![](./contasol.images/contasol27.png){width="28%"}
    ![](./contasol.images/contasol28.png){width="45%"}

8. And the Contasol window appears. You must choose a user: Test, user with credentials or collaborator, and the installation will be done

    ![](./contasol.images/contasol29.png){width="45%"}
    ![](./contasol.images/contasol30.png){width="45%"}
