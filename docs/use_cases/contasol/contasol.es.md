# Contasol (cliente)

Esta guía explica cómo instalar el software **Contasol** a la plataforma de IsardVDI.

## Pre-instalación

Para iniciar la instalación de Contasol, se puede utilizar cualquiera de nuestras plantillas disponibles para la creación de un escritorio virtual. Estas plantillas te proporcionan un punto de partida para configurar tu entorno de trabajo y empezar con la instalación del software de Contasol de forma rápida y eficiente.

En esta guía, se utiliza una plantilla de Windows 11.

## Instalación

1. Se descarga el software [**Contasol**](https://www.sdelsol.com/). Para descargarlo desde la página web oficial se necesita tener una cuenta y validarse. En las opciones de ayuda tenemos el link para ir al centro de descargas. En este caso tenemos acceso a Contasol, pero podría ser NominaSol o Factusol o cualquier otro.

    ![](./contasol.images/contasol1.png){width="45%"}
    ![](./contasol.images/contasol2.png){width="45%"}
    ![](./contasol.images/contasol3.png){width="28%"}
    ![](./contasol.images/contasol4.png){width="63%"}

2. Se descarga el archivo y se inicia la instalación.

    ![](./contasol.images/contasol7.png){width="45%"}
    ![](./contasol.images/contasol8.png){width="45%"}
    ![](./contasol.images/contasol9.png){width="45%"}
    ![](./contasol.images/contasol10.png){width="45%"}
    ![](./contasol.images/contasol11.png){width="45%"}
    ![](./contasol.images/contasol12.png){width="54%"}
    ![](./contasol.images/contasol13.png){width="45%"}

3. Al ejecutar el programa por primera vez, nos dice que el formato de la fecha no es el correcto

    ![](./contasol.images/contasol14.png){width="32%"}
    ![](./contasol.images/contasol15.png){width="45%"}

4. Vamos al panel de control para añadir el formato que pide Contasol. Dentro del tablero seleccionamos 'Cambiar los formatos de números, de la fecha o de la hora'

    ![](./contasol.images/contasol16.png){width="34%"}
    ![](./contasol.images/contasol17.png){width="60%"}

5. Este es el formato actual que se tendrá que cambiar pulsando en "Configuracions addicionals"

    ![](./contasol.images/contasol18.png){width="45%"}
    ![](./contasol.images/contasol19.png){width="39%"}

6. Se pone el formato adecuado en el cuadro 'Data Corta' y se pulsa sobre 'aplicar'. Ahora se tendrá un nuevo formato que se puede seleccionar.

    ![](./contasol.images/contasol20.png){width="38%"}
    ![](./contasol.images/contasol21.png){width="45%"}

7. Se vuelve a iniciar Contasol.Se tendrá que instalar el motor de base de datos de Microsoft Access

    ![](./contasol.images/contasol23.png){width="50%"}
    ![](./contasol.images/contasol24.png){width="29%"}
    ![](./contasol.images/contasol25.png){width="45%"}
    ![](./contasol.images/contasol26.png){width="45%"}
    ![](./contasol.images/contasol27.png){width="28%"}
    ![](./contasol.images/contasol28.png){width="45%"}

8. Y aparece la ventana de Contasol. Se deberá elegir usuario: Prueba, usuario con credenciales o colaborador, y ya estaría la instalación

    ![](./contasol.images/contasol29.png){width="45%"}
    ![](./contasol.images/contasol30.png){width="45%"}

