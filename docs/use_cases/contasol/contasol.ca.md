# Contasol (client)

Aquesta guia explica com instal·lar el programari **Contasol** a la plataforma d'IsardVDI.

## Pre-instal·lació

Per iniciar la instal·lació de Contasol, podeu utilitzar qualsevol de les nostres plantilles disponibles per a la creació d'un escriptori virtual. Aquestes plantilles us proporcionen un punt de partida per configurar el vostre entorn de treball i començar amb la instal·lació del programari de Contasol de forma ràpida i eficient.

Aquesta guia utilitza una plantilla de Windows 11.

## Instal·lació

1. Es descarrega el programari [**Contasol**](https://www.sdelsol.com/). Per descarregar-lo des de la pàgina web oficial cal tenir un compte i validar-se. A les opcions d'ajuda tenim el link per anar al centre de baixades. En aquest cas tenim accés a Contasol, però podria ser NominaSol o Factusol o qualsevol un altre.

    ![](./contasol.images/contasol1.png){width="45%"}
    ![](./contasol.images/contasol2.png){width="45%"}
    ![](./contasol.images/contasol3.png){width="28%"}
    ![](./contasol.images/contasol4.png){width="63%"}

2. Es descarrega el fitxer i s'inicia la instal·lació.

    ![](./contasol.images/contasol7.png){width="45%"}
    ![](./contasol.images/contasol8.png){width="45%"}
    ![](./contasol.images/contasol9.png){width="45%"}
    ![](./contasol.images/contasol10.png){width="45%"}
    ![](./contasol.images/contasol11.png){width="45%"}
    ![](./contasol.images/contasol12.png){width="54%"}
    ![](./contasol.images/contasol13.png){width="45%"}

3. En executar el programa per primer cop, ens diu que el format de la data no és el correcte

    ![](./contasol.images/contasol14.png){width="32%"}
    ![](./contasol.images/contasol15.png){width="45%"}

4. Anem al tauler de control per afegir el format que demana Contasol. Dins del tauler seleccionem 'Canviar els formats de números, de la data o de l'hora'

    ![](./contasol.images/contasol16.png){width="34%"}
    ![](./contasol.images/contasol17.png){width="60%"}

5. Este es el formato actual que se tendrá que cambiar pulsando en "Configuracions addicionals"

    ![](./contasol.images/contasol18.png){width="45%"}
    ![](./contasol.images/contasol19.png){width="39%"}

6. Se pone el formato adecuado en el cuadro 'Data Corta' y se pulsa sobre 'aplicar'. Ahora se tendrá un nuevo formato que se puede seleccionar.

    ![](./contasol.images/contasol20.png){width="38%"}
    ![](./contasol.images/contasol21.png){width="45%"}

7. Se vuelve a iniciar Contasol.Se tendrá que instalar el motor de base de datos de Microsoft Access

    ![](./contasol.images/contasol23.png){width="50%"}
    ![](./contasol.images/contasol24.png){width="29%"}
    ![](./contasol.images/contasol25.png){width="45%"}
    ![](./contasol.images/contasol26.png){width="45%"}
    ![](./contasol.images/contasol27.png){width="28%"}
    ![](./contasol.images/contasol28.png){width="45%"}

8. Y aparece la ventana de Contasol. Se deberá elegir usuario: Prueba, usuario con credenciales o colaborador, y ya estaría la instalación

    ![](./contasol.images/contasol29.png){width="45%"}
    ![](./contasol.images/contasol30.png){width="45%"}