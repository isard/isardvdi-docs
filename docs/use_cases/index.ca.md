# Casos d'ús

Compilació de casos útils que es poden adaptar a les vostres necessitats o obtenir idees per construir els vostres propis laboratoris utilitzant IsardVDI.

En la secció **Instal·lacions de sistemes operatius**, trobareu guies detallades per a la instal·lació dels sistemes operatius d'escriptoris virtuals per a Linux i Windows. Aquestes guies estan optimitzades per a un entorn de virtualització, assegurant la seva eficiència i correcte funcionament.

En la secció **Instal·lacions de programari**, trobareu guies detallades per a la instal·lació de diferents programaris per als escriptoris virtuals.