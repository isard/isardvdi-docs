# a3ERP 

## a3ERP (Gestor de licencias)

Esta guía explica cómo instalar el gestor de licencias de **a3ERP** en un escritorio virtual dentro de la plataforma de IsardVDI.

1. Desde la web de [Wolters](miwolterskluwer.com/home), es necesario ir a "Mis soluciones", a "componentes & herramientas" y "componentes genéricos"

    ![](./a3erp.images/a3erp1.png){width="45%"}
    ![](./a3erp.images/a3erp2.png){width="32%"}
    ![](./a3erp.images/a3erp4.png){width="32%"}
    ![](./a3erp.images/a3erp5.png){width="62%"}

2. Se obtendrá un código para la instalación, que más adelante se nos pedirá.

    ![](./a3erp.images/a3erp6.png){width="45%"}

3. Una vez descargado, se procede a realizar la instalación

    ![](./a3erp.images/a3erp7.png){width="45%"}
    ![](./a3erp.images/a3erp8.png){width="45%"}
    ![](./a3erp.images/a3erp9.png){width="45%"}
    ![](./a3erp.images/a3erp10.png){width="45%"}

4. Ahora es el momento para introducir la clave, pero al pulsar sobre el botón 'siguiente' el programa nos avisa de que falta por instalar el .Net Framework de MS, la versión 3.5. Hagamos la instalación.

    ![](./a3erp.images/a3erp11.png){width="45%"}
    ![](./a3erp.images/a3erp12.png){width="45%"}
    ![](./a3erp.images/a3erp13.png){width="45%"}

5. Ahora se podrá seguir el proceso

    ![](./a3erp.images/a3erp14.png){width="45%"}
    ![](./a3erp.images/a3erp15.png){width="45%"}
    ![](./a3erp.images/a3erp16.png){width="45%"}

6. Una vez finalizado, podremos ejecutarlo, pero todavía no se tiene ninguna licencia instalada

    ![](./a3erp.images/a3erp17.png){width="45%"}
    ![](./a3erp.images/a3erp18.png){width="45%"}
    ![](./a3erp.images/a3erp19.png){width="45%"}
    
## a3ERP (Servidor)

Con el gestor de [Wolters kluver](miwolterskluwer.com/home) iniciamos la descarga del instalador de a3ERP.

1. Se realiza la descarga y se ejecuta el gestor

    ![](./a3erp.images/a3erp20.png){width="45%"}
    ![](./a3erp.images/a3erp21.png){width="45%"}
    ![](./a3erp.images/a3erp22.png){width="45%"}

2. Se empieza la instalación del a3ERP

    ![](./a3erp.images/a3erp25.png){width="45%"}
    ![](./a3erp.images/a3erp26.png){width="45%"}
    ![](./a3erp.images/a3erp27.png){width="45%"}
    ![](./a3erp.images/a3erp28.png){width="45%"}

3. Se elige la opción "Servicio de Windows HOST" y se selecciona para funcionar como servidor en la "red local"

    ![](./a3erp.images/a3erp29.png){width="45%"}
    ![](./a3erp.images/a3erp30.png){width="45%"}

4. Si no se tiene SQL server en el sistema, el configurador puede instalarlo. Se pulsa en "buscar instancias" e instalar "SQL en a3ERP"

    ![](./a3erp.images/a3erp31.png){width="45%"}
    ![](./a3erp.images/a3erp32.png){width="45%"}

5. Una vez instalado, ya se puede crear una instancia de SQL para A3.

    ![](./a3erp.images/a3erp33.png){width="45%"}
    ![](./a3erp.images/a3erp34.png){width="45%"}
    ![](./a3erp.images/a3erp35.png){width="45%"}

6. Durante el proceso, se proporciona y verifica una clave para acceder posteriormente a la aplicación

    ![](./a3erp.images/a3erp36.png){width="45%"}
    ![](./a3erp.images/a3erp37.png){width="45%"}

7. La instalación seguirá creando algunas carpetas compartidas para los clientes: "Listados"

    ![](./a3erp.images/a3erp38.png){width="45%"}

8. Se puede utilizar los certificados que tenemos o bien dejar que A3 ponga los suyos

    ![](./a3erp.images/a3erp39.png){width="45%"}
    ![](./a3erp.images/a3erp40.png){width="45%"}
    ![](./a3erp.images/a3erp41.png){width="45%"}

9. Al finalizar la instalación, se muestra una ventana con los datos de configuración del programa

    ![](./a3erp.images/a3erp42.png){width="45%"}
    ![](./a3erp.images/a3erp43.png){width="45%"}


