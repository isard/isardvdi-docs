# a3ERP 

## a3ERP (Gestor de llicències)

Aquesta guia explica com instal·lar el gestor de llicències de **a3ERP** en un escriptori virtual dins la plataforma IsardVDI.

1. Des de la web de [Wolters](miwolterskluwer.com/home), s'ha d'anar a "Les meves solucions", a "components & eines" i "components genèrics"

    ![](./a3erp.images/a3erp1.png){width="45%"}
    ![](./a3erp.images/a3erp2.png){width="32%"}
    ![](./a3erp.images/a3erp4.png){width="32%"}
    ![](./a3erp.images/a3erp5.png){width="62%"}

2. S'obtindrà un codi per a la instal·lació, que més endavant se’ns demanarà.

    ![](./a3erp.images/a3erp6.png){width="45%"}

3. Un cop descarregat, es procedeix a fer la instal·lació

    ![](./a3erp.images/a3erp7.png){width="45%"}
    ![](./a3erp.images/a3erp8.png){width="45%"}
    ![](./a3erp.images/a3erp9.png){width="45%"}
    ![](./a3erp.images/a3erp10.png){width="45%"}

4. Ara és el moment per introduir la clau, però en prémer sobre el botó 'siguiente' el programa ens avisa que falta per instal·lar el .Net Framework de MS, la versió 3.5. Fem la instal·lació.

    ![](./a3erp.images/a3erp11.png){width="45%"}
    ![](./a3erp.images/a3erp12.png){width="45%"}
    ![](./a3erp.images/a3erp13.png){width="45%"}

5. Ara es podrà seguir el procés

    ![](./a3erp.images/a3erp14.png){width="45%"}
    ![](./a3erp.images/a3erp15.png){width="45%"}
    ![](./a3erp.images/a3erp16.png){width="45%"}

6. Un cop finalitzat, el podrem ejecutar, però encara no es té cap llicència instal·lada

    ![](./a3erp.images/a3erp17.png){width="45%"}
    ![](./a3erp.images/a3erp18.png){width="45%"}
    ![](./a3erp.images/a3erp19.png){width="45%"}
    
## a3ERP (Server)

Amb el gestor de [Wolters kluver](miwolterskluwer.com/home) iniciem la descàrrega de l’instal·lador de a3ERP.

1. Es fa la descàrrega i s'executa el gestor

    ![](./a3erp.images/a3erp20.png){width="45%"}
    ![](./a3erp.images/a3erp21.png){width="45%"}
    ![](./a3erp.images/a3erp22.png){width="45%"}

2. Es comença la instal·lació de l'a3ERP

    ![](./a3erp.images/a3erp25.png){width="45%"}
    ![](./a3erp.images/a3erp26.png){width="45%"}
    ![](./a3erp.images/a3erp27.png){width="45%"}
    ![](./a3erp.images/a3erp28.png){width="45%"}

3. Es tria l'opció "Servicio de Windows HOST" i seleccionem per funcionar com a servidor a la "xarxa local"

    ![](./a3erp.images/a3erp29.png){width="45%"}
    ![](./a3erp.images/a3erp30.png){width="45%"}

4. Si no es té SQL server al sistema, el configurador pot instal·lar-lo. Es prém a "buscar instancias" i instal·lar "SQL a a3ERP"

    ![](./a3erp.images/a3erp31.png){width="45%"}
    ![](./a3erp.images/a3erp32.png){width="45%"}

5. Un cop instal·lat, ja es pot crear una instància de SQL per a l'A3. 

    ![](./a3erp.images/a3erp33.png){width="45%"}
    ![](./a3erp.images/a3erp34.png){width="45%"}
    ![](./a3erp.images/a3erp35.png){width="45%"}

6. Durant el procés, es proporciona i verifica una clau per accedir posteriorment a l'aplicació 

    ![](./a3erp.images/a3erp36.png){width="45%"}
    ![](./a3erp.images/a3erp37.png){width="45%"}

7. La instal·laciócontinuarà creant algunes carpetes compartides per als clients: "Listados"

    ![](./a3erp.images/a3erp38.png){width="45%"}

8. Es pot fer servir els certificats que tenim o bé deixar que A3 posi els seus

    ![](./a3erp.images/a3erp39.png){width="45%"}
    ![](./a3erp.images/a3erp40.png){width="45%"}
    ![](./a3erp.images/a3erp41.png){width="45%"}

9. Al finalitzar la instal·lació, es mostra una finestra amb les dades de configuració del programa

    ![](./a3erp.images/a3erp42.png){width="45%"}
    ![](./a3erp.images/a3erp43.png){width="45%"}


