# a3ERP 

## a3ERP (License management)

This guide explains how to install the **a3ERP** license manager on a virtual desktop within the IsardVDI platform.

1. From the [Wolters](miwolterskluwer.com/home) website, it is necessary to go to "My solutions", "components & tools" and "generic components"

    ![](./a3erp.images/a3erp1.png){width="45%"}
    ![](./a3erp.images/a3erp2.png){width="32%"}
    ![](./a3erp.images/a3erp4.png){width="32%"}
    ![](./a3erp.images/a3erp5.png){width="62%"}

2. A code will be obtained for the installation, which we will be asked for later.

    ![](./a3erp.images/a3erp6.png){width="45%"}

3. Once downloaded, ypou can proceed with the installation

    ![](./a3erp.images/a3erp7.png){width="45%"}
    ![](./a3erp.images/a3erp8.png){width="45%"}
    ![](./a3erp.images/a3erp9.png){width="45%"}
    ![](./a3erp.images/a3erp10.png){width="45%"}

4. Now is the time to enter the code, but when clicking on the 'next' button the program warns us that the MS .Net Framework, version 3.5, remains to be installed. Let's do the installation.

    ![](./a3erp.images/a3erp11.png){width="45%"}
    ![](./a3erp.images/a3erp12.png){width="45%"}
    ![](./a3erp.images/a3erp13.png){width="45%"}

5. Now you can follow the process

    ![](./a3erp.images/a3erp14.png){width="45%"}
    ![](./a3erp.images/a3erp15.png){width="45%"}
    ![](./a3erp.images/a3erp16.png){width="45%"}

6. Once finished, we can run it, but it still does not have any license installed

    ![](./a3erp.images/a3erp17.png){width="45%"}
    ![](./a3erp.images/a3erp18.png){width="45%"}
    ![](./a3erp.images/a3erp19.png){width="45%"}
    
## a3ERP (Server)

With the [Wolters kluver](miwolterskluwer.com/home) manager, we start the download of the a3ERP installer.

1. Carry out the download and run the Wolter manager 

    ![](./a3erp.images/a3erp20.png){width="45%"}
    ![](./a3erp.images/a3erp21.png){width="45%"}
    ![](./a3erp.images/a3erp22.png){width="45%"}

2. Begin the installation of a3ERP

    ![](./a3erp.images/a3erp25.png){width="45%"}
    ![](./a3erp.images/a3erp26.png){width="45%"}
    ![](./a3erp.images/a3erp27.png){width="45%"}
    ![](./a3erp.images/a3erp28.png){width="45%"}

3. Choose the "Windows HOST Service" option and select to function as a server on the "local network"

    ![](./a3erp.images/a3erp29.png){width="45%"}
    ![](./a3erp.images/a3erp30.png){width="45%"}

4. "SQL en a3ERP" If you do not have SQL server on the system, the configurator can install it. Click on "search for instances" and install "SQL in a3ERP"

    ![](./a3erp.images/a3erp31.png){width="45%"}
    ![](./a3erp.images/a3erp32.png){width="45%"}

5. Once installed, you can now create an instance of SQL for A3.

    ![](./a3erp.images/a3erp33.png){width="45%"}
    ![](./a3erp.images/a3erp34.png){width="45%"}
    ![](./a3erp.images/a3erp35.png){width="45%"}

6. During the process, you need to provide and verify a password to later access the application

    ![](./a3erp.images/a3erp36.png){width="45%"}
    ![](./a3erp.images/a3erp37.png){width="45%"}

7. The installation will still create some shared folders for clients: "Listings"

    ![](./a3erp.images/a3erp38.png){width="45%"}

8. You can use the certificates you have or let A3 add theirs

    ![](./a3erp.images/a3erp39.png){width="45%"}
    ![](./a3erp.images/a3erp40.png){width="45%"}
    ![](./a3erp.images/a3erp41.png){width="45%"}

9. Upon completion of the installation, a window is displayed with the program configuration data

    ![](./a3erp.images/a3erp42.png){width="45%"}
    ![](./a3erp.images/a3erp43.png){width="45%"}


