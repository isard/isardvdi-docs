# Use Cases

Compilation of useful cases done that you can adapt to your needs or get ideas to build your own labs using IsardVDI.

In the **Operating system installations** section, you will find detailed guides for installing virtual desktop operating systems for Linux and Windows. These guides are optimized for a virtualization environment, ensuring their efficiency and correct operation.

In the **Software Installations** section, you will find detailed guides for installing different software for virtual desktops.