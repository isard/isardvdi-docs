# Solidworks (Cliente)

Esta guía explica cómo instalar el software **Solidworks** con un servidor de licencias externo a la plataforma de IsardVDI.

## Pre-instalación

Para iniciar la instalación de Solidworks, se puede utilizar cualquiera de nuestras plantillas disponibles para la creación de un escritorio virtual. Estas plantillas te proporcionan un punto de partida para configurar tu entorno de trabajo y empezar con la instalación del software de Solidworks de forma rápida y eficiente.

En esta guía, se utiliza una plantilla de Windows 11.

## Instalación

1. Se descarga el software [**Solidworks**](https://www.solidworks.com/sw/support/downloads.htm). Para descargarlo desde la página web oficial se necesita tener una cuenta y validarse.

    ![](solidworks.images/solidworks1.png){width="45%"}
    ![](solidworks.images/solidworks2.png){width="45%"}

2. Una vez descargado, comienza la instalación.

    ![](solidworks.images/solidworks3.png){width="45%"}
    ![](solidworks.images/solidworks4.png){width="45%"}
    ![](solidworks.images/solidworks5.png){width="45%"}
    ![](solidworks.images/solidworks6.png){width="45%"}

3. Momento de poner la clave de licensia proporcionada por el distribuidor de Solidworks

    ![](solidworks.images/solidworks7.png){width="45%"}
    ![](solidworks.images/solidworks8.png){width="45%"}

4. Se hace la selección de lo que se quiere modificar de la instalación, junto con la selección de idioma.

    ![](solidworks.images/solidworks9.png){width="45%"}
    ![](solidworks.images/solidworks10.png){width="45%"}

## Conexión con el servidor de licencias local

### Instalación de la VPN de Wireguard

!!! Info
    * Se debe crear una red VPN remota dentro de IsardVDI, para ello, ver este [manual](/docs/admin/domains_resources.es.md/#remote-vpns).
    * Esta red sólo la puede crear el **administrador** de IsardVDI.

Descargue el archivo con la configuración de la VPN remota para usar Wireguard. Si no es **administrador** de la plataforma, solicite el archivo al administrador.

**Se debe realizar la instalación de wireguard en el ordenador local donde se tiene el servidor de licencias.**

![](solidworks.images/solidworks25.png){width="50%"}

Una vez descargado, debe ir a la carpeta de "Descargas" e iniciar la instalación.

![](solidworks.images/solidworks27.png){width="45%"}
![](solidworks.images/solidworks28.png){width="45%"}

Desde Wireguard, se abre el archivo de la VPN remota descargado desde IsardVDI para configurar la VPN y se pulsa el botón "Import tunnel(s) from file".

![](solidworks.images/solidworks29.png){width="45%"}
![](solidworks.images/solidworks30.png){width="45%"}

Una vez importado, se muestra la configuración en la pantalla

![](solidworks.images/solidworks31.png){width="45%"}

Se debe pulsar el botón "Activar" para aplicar la configuración e iniciar el túnel.

### Conexión con VPN

En esta parte es cuando debe conectarse con el servidor de licencias que tenemos físicamente. Se debe poner la IP del servidor en la VPN. Se puede modificar posteriormente desde el "panel del control -> añadir o quitar programas -> solidworks" seleccionando la opción "cambiar" y modificando sólo la parte de conexión con el servidor.

![](solidworks.images/solidworks13.png){width="45%"}

Una vez que ha establecido la comunicación con el servidor, comienza la instalación.

![](solidworks.images/solidworks14.png){width="45%"}
![](solidworks.images/solidworks15.png){width="45%"}
![](solidworks.images/solidworks16.png){width="45%"}


### Conexión con Solidworks License Manager Client

Para cambiar el servidor de licencias se busca el software "SolidNetWork License Manager Client"

![](solidworks.images/solidworks17.png){width="45%"}
![](solidworks.images/solidworks18.png){width="45%"}
![](solidworks.images/solidworks19.png){width="45%"}
![](solidworks.images/solidworks20.png){width="45%"}

Puede verse que el servidor no está bien configurado porque no se tiene ninguna licencia disponible. Lo cambiamos.

![](solidworks.images/solidworks21.png){width="45%"}

Se revisa en 'Server List' la lista de servidores a la que se puede acceder desde este ordenador.

![](solidworks.images/solidworks22.png){width="45%"}

Se añade un nuevo servidor con la IP del servidor físico. 

![](solidworks.images/solidworks23.png){width="45%"}

Una vez conectado, ya se pueden ver las licencias disponibles y se puede utilizar Solidworks.

![](solidworks.images/solidworks24.png){width="45%"}