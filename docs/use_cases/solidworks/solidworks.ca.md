# Solidworks (Client)

Aquesta guia explica com instal·lar el programari **Solidworks** amb un servidor de llicències extern a la plataforma d'IsardVDI.

## Pre-instal·lació

Per a iniciar la instal·lació de Solidworks, es pot emprar qualsevol de les nostres plantilles disponibles per a la creació d'un escriptori virtual. Aquestes plantilles et proporcionen un punt de partida per a configurar el teu entorn de treball i començar amb la instal·lació del programari de Solidworks de manera ràpida i eficient.

En aquesta guia, s'utilitza una plantilla de Windows 11.

## Instal·lació

1. Es descarrega el programari [**Solidworks**](https://www.solidworks.com/sw/support/downloads.htm). Per poder descarregar-ho des de la pàgina web oficial es necessita tenir un compte.

    ![](solidworks.images/solidworks1.png){width="45%"}
    ![](solidworks.images/solidworks2.png){width="45%"}

2. Un cop descarregat, comença la instal·lació.

    ![](solidworks.images/solidworks3.png){width="45%"}
    ![](solidworks.images/solidworks4.png){width="45%"}
    ![](solidworks.images/solidworks5.png){width="45%"}
    ![](solidworks.images/solidworks6.png){width="45%"}

3. Moment de posar la clau de llicència proporcionada pel distribuïdor de Solidworks

    ![](solidworks.images/solidworks7.png){width="45%"}
    ![](solidworks.images/solidworks8.png){width="45%"}

4. Es fa la selecció del que es vol modificar de la instal·lació, juntament amb la selecció d'idioma.

    ![](solidworks.images/solidworks9.png){width="45%"}
    ![](solidworks.images/solidworks10.png){width="45%"}

## Connexió amb el servidor de llicències local

### Instal·lació de la VPN de Wireguard

!!! Info
    * S'ha de crear una xarxa VPN remota dins d'IsardVDI, per això veure aquest [manual](/docs/admin/domains_resources.ca.md/#vpns-remotes).
    * Aquesta xarxa solament la pot crear l'**administrador** d'IsardVDI.

Descarregui l'arxiu amb la configuració de la VPN remota per a usar Wireguard. Si no és **administrador** de la plataforma, sol·liciti l'arxiu a l'administrador.

**S'ha de fer la instal·lació de wireguard a l'ordinador local on es té el servidor de llicències.**

![](solidworks.images/solidworks25.png){width="50%"}

Un cop descarregat, s'ha d'anar a la carpeta de "Baixades" i iniciar la instal·lació.

![](solidworks.images/solidworks27.png){width="45%"}
![](solidworks.images/solidworks28.png){width="45%"}

Des de Wireguard, s'obre el fitxer VPN remot que s'ha descarregat des d'IsardVDI per configurar la VPN, es prem el botó "Import tunnel(s) from file"

![](solidworks.images/solidworks29.png){width="45%"}
![](solidworks.images/solidworks30.png){width="45%"}

Un cop importat, es mostra la configuració a la pantalla

![](solidworks.images/solidworks31.png){width="45%"}

S'ha de prémer el botó "Activar" per aplicar la configuració i iniciar el túnel.

### Connexió amb VPN

En aquesta part és quan s'ha de connectar amb el servidor de llicències que tenim físicament. S'ha de posar la IP del servidor a la VPN. Es pot modificar posteriorment des del "panell del control -> afegir o treure programes -> solidworks" seleccionant l'opció "canviar" i modificant només la part de connexió amb el servidor.

![](solidworks.images/solidworks13.png){width="45%"}

Un cop ha establer la comunicació amb el servidor, comença la instal·lació.

![](solidworks.images/solidworks14.png){width="45%"}
![](solidworks.images/solidworks15.png){width="45%"}
![](solidworks.images/solidworks16.png){width="45%"}


### Connexió amb Solidworks License Manager Client

Per canviar el servidor de llicències se cerca el programari "SolidNetWork License Manager Client"

![](solidworks.images/solidworks17.png){width="45%"}
![](solidworks.images/solidworks18.png){width="45%"}
![](solidworks.images/solidworks19.png){width="45%"}
![](solidworks.images/solidworks20.png){width="45%"}

Es pot veure que el servidor no està ben configurat perquè no es té cap llicència disponible. Ho canviem.

![](solidworks.images/solidworks21.png){width="45%"}

Es revisa a 'Server List' la llista de servidors al qual es pot accedir des d'aquest ordinador.

![](solidworks.images/solidworks22.png){width="45%"}

S'afegeix un nou servidor amb la IP del servidor físic. 

![](solidworks.images/solidworks23.png){width="45%"}

Un cop connectat, ja es poden veure les llicències disponibles i es pot fer servir el Solidworks.

![](solidworks.images/solidworks24.png){width="45%"}