# Solidworks (Client)

This guide explains how to install **Solidworks** software with a license server external to the IsardVDI platform.

## Pre-installation

To start the Solidworks installation, you can use any of our available templates to create a virtual desktop. These templates provide you with a starting point to configure your work environment and begin installing Solidworks software quickly and efficiently.

In this guide, a Windows 11 template is used.

## Installation

1. Download the [**Solidworks**](https://www.solidworks.com/sw/support/downloads.htm) software. To download it from the official website you need to have an account.

    ![](solidworks.images/solidworks1.png){width="45%"}
    ![](solidworks.images/solidworks2.png){width="45%"}

2. Once downloaded, the installation begins.

    ![](solidworks.images/solidworks3.png){width="45%"}
    ![](solidworks.images/solidworks4.png){width="45%"}
    ![](solidworks.images/solidworks5.png){width="45%"}
    ![](solidworks.images/solidworks6.png){width="45%"}

3. Time to enter the llicense key provided by the Solidworks distributor

    ![](solidworks.images/solidworks7.png){width="45%"}
    ![](solidworks.images/solidworks8.png){width="45%"}

4. Select what you want to modify in the installation, along with the language selection.

    ![](solidworks.images/solidworks9.png){width="45%"}
    ![](solidworks.images/solidworks10.png){width="45%"}

## Connecting to the local license server

### Installing Wireguard VPN

!!! Info
    * A remote VPN network must be created within IsardVDI, to do so, see this [manual](/docs/admin/domains_resources.md/#remote-vpns).
    * This network can only be created by the IsardVDI **administrator**.

Download the remote VPN configuration file to use Wireguard. If you are not an **administrator** of the platform, request the file from the administrator.

**Wireguard must be installed on the local computer where the license server is located.**

![](solidworks.images/solidworks25.png){width="50%"}

Once downloaded, you must go to the "Downloads" folder and start the installation.

![](solidworks.images/solidworks27.png){width="45%"}
![](solidworks.images/solidworks28.png){width="45%"}

From Wireguard, open the remote VPN file downloaded from IsardVDI to configure the VPN and press the "Import tunnel(s) from file" button.

![](solidworks.images/solidworks29.png){width="45%"}
![](solidworks.images/solidworks30.png){width="45%"}

Once imported, the configuration is displayed on the screen

![](solidworks.images/solidworks31.png){width="45%"}

The "Enable" button must be pressed to apply the configuration and start the tunnel.

### Connection with VPN

This part is when you must connect to the license server that you physically have. You must put the server's IP in the VPN. It can be modified later from the "control panel -> add or remove programs -> solidworks" by selecting the "change" option and modifying only the connection part with the server.

![](solidworks.images/solidworks13.png){width="45%"}

Once you have established communication with the server, the installation begins.

![](solidworks.images/solidworks14.png){width="45%"}
![](solidworks.images/solidworks15.png){width="45%"}
![](solidworks.images/solidworks16.png){width="45%"}


### Connecting to Solidworks License Manager Client

To change the license server, search for the "SolidNetWork License Manager Client" software

![](solidworks.images/solidworks17.png){width="45%"}
![](solidworks.images/solidworks18.png){width="45%"}
![](solidworks.images/solidworks19.png){width="45%"}
![](solidworks.images/solidworks20.png){width="45%"}

You can see that the server is not configured correctly because there is no license available. We change it.

![](solidworks.images/solidworks21.png){width="45%"}

You can review the list of servers that can be accessed from this computer in 'Server List'.

![](solidworks.images/solidworks22.png){width="45%"}

Add a new server with the IP of the physical server.

![](solidworks.images/solidworks23.png){width="45%"}

Once connected, you can see the available licenses and can use Solidworks.

![](solidworks.images/solidworks24.png){width="45%"}