# Guia d'instal·ació de Microsoft Office 365

## Pre-instal·lació

Per a iniciar la instal·lació de Microsoft Office, es pot emprar qualsevol de les nostres plantilles disponibles per a la creació d'un escriptori virtual. Aquestes plantilles et proporcionen un punt de partida per a configurar el teu entorn de treball i començar amb la instal·lació del programari de Microsoft Office de manera ràpida i eficient.

## Instal·lació

1. Per obtenir el programari, s'ha d'anar a la web [d'Office](www.office.com). És necessari iniciar sessió a Microsoft:

    ![](microsoft_office.images/office1.png){width="45%"}
    ![](microsoft_office.images/office2.png){width="45%"}

2. Després d'iniciar sessió, redirigeix a una altra pàgina on es prem el botó "Instal·lació d'aplicacions"

    ![](microsoft_office.images/office3.png){width="90%"}

3. Se selecciona la primera opció, "Aplicacions de Microsoft 365" i es descarrega el fitxer al navegador

    ![](microsoft_office.images/office4.png){width="45%"}
    ![](microsoft_office.images/office5.png){width="45%"}

4. S'obre la carpeta "Descàrregues" i s'executa el fitxer

    ![](microsoft_office.images/office7.png){width="45%"}
    ![](microsoft_office.images/office8.png){width="45%"}

5. Comença la instal·lació amb la baixada del programari

    ![](microsoft_office.images/office9.png){width="45%"}
    ![](microsoft_office.images/office10.png){width="45%"}

6. Per poder iniciar qualsevol de les aplicacions d'Office i activar el producte, cal iniciar sessió al teu compte de Microsoft. Si s'omet aquest pas, es té un període de gràcia de cinc dies durant els quals es pot fer servir Office sense cap altra limitació. També, si es disposa d'un número de sèrie vàlid, es pot registrar ara.

    ![](microsoft_office.images/office11.png){width="45%"}

    ![](microsoft_office.images/office12.png){width="45%"}

### Inici de sessió

1. S'inicia sessió amb un compte Microsoft

    ![](microsoft_office.images/office13.png){width="45%"}

2. Es pot iniciar sessió a totes les aplicacions de Microsoft o només a la que s'ha accedit

    ![](microsoft_office.images/office14.png){width="45%"}

3. Per sortir, es tanca la sessió al compte de Microsoft. El producte torna a quedar desactivat.

    ![](microsoft_office.images/office15.png){width="45%"}
