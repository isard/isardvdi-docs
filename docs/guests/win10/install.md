# Windows 10 installation guide

## Pre-installation

1. Download [Windows 10](https://www.microsoft.com/en-us/software-download/windows10ISO) 64 bits version ISO image.

2. [Generate a desktop from that media](../../../advanced/media/#generate-desktop-from-media) with the following virtual hardware:

    - Viewers: **SPICE**
    - vCPUS: **4**
    - Memory (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Disk size (GB): **100**
    - Networks: **Default**
    - Template: **Microsoft windows 10 with Virtio devices UEFI**

3. [Edit desktop and assign more Media](../../../user/edit_desktop/#media), so that it will end up with the following:

    - **Win10_22H2 - ES** (installer)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (optimization software for Windows O.S.)


## Installation

### Windows 10 Pro

1. **Send "Ctr+Alt+Supr"** and press any keyboard key on second screen

    ![](install.es.images/1.png){width="46%"}
    ![](install.es.images/2.png){width="45%"}

2. Installation type

    ![](install.es.images/3.png){width="45%"}
    ![](install.es.images/4.png){width="45%"}

    ![](install.es.images/5.png){width="45%"}
    ![](install.es.images/6.png){width="45%"}

    ![](install.es.images/7.png){width="45%"}

3. Load operating system drivers

    ![](install.es.images/8.png){width="45%"}
    ![](install.es.images/9.png){width="45%"}

    ![](install.es.images/10.png){width="45%"}
    ![](install.es.images/11.png){width="45%"}

    ![](install.es.images/12.png){width="45%"}


### System

1. Shut-down the desktop and [edit it](../../../user/edit_desktop/#hardware) to modify virtual hardware **Boot** option from **CD/DVD** to **Hard Disk**

2. Start the desktop, it will take a few seconds to start the operating system:

    ![](install.es.images/13.png){width="45%"}

3. Select the following options on agent following steps

    ![](install.es.images/14.png){width="45%"}
    ![](install.es.images/15.png){width="45%"}

    ![](install.es.images/16.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

    ![](install.es.images/18.png){width="45%"}
    ![](install.es.images/19.png){width="45%"}

    ![](install.es.images/20.png){width="45%"}
    ![](install.es.images/21.png){width="45%"}

    ![](install.es.images/22.png){width="45%"}
    ![](install.es.images/23.png){width="45%"}

    ![](install.es.images/24.png){width="45%"}
    ![](install.es.images/25.png){width="45%"}

    ![](install.es.images/26.png){width="45%"}
    ![](install.es.images/27.png){width="45%"}

    ![](install.es.images/28.png){width="45%"}
    ![](install.es.images/29.png){width="45%"}

    ![](install.es.images/30.png){width="45%"}
    ![](install.es.images/31.png){width="45%"}

4. Shut-down desktop and edit it to assign only the following media (if you want to skip this step, you will still have to shut down the desktop and restart it, **do not reboot**):

    - **virtio-win-X** (drivers)
    - **Optimization Tools** (optimization software for Windows O.S.)


## Configuration

### Update and install

1. Install **virtio** drivers from assigned desktop Media. Both installations are fast and have to only press **Next** button.

    ![](install.es.images/32.png){width="45%"}
    ![](install.es.images/33.png){width="45%"}

    ![](install.es.images/34.png){width="45%"}
    ![](install.es.images/35.png){width="45%"}

    ![](install.es.images/36.png){width="80%"}

2. Check **system updates**, which will take a long time to download and install, in order to be up to date with the latest version of Windows Server (it may be necessary to reboot the system several times to check for new updates).

    ![](install.es.images/41.png){width="80%"}

3. The following applications are installed and their installers are saved in the new **admin** folder in the path **C:\admin**.

    - Firefox
    - Google chrome
    - Libre Office
    - Gimp
    - Inkscape
    - LibreCAD
    - Geany
    - Adobe Acrobat Reader


### User admin and privilege changes

In a **Powershell** with administrator privilege:

1. Create **admin** user in **Administradores** group
    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administradores" -Member "admin"
    ```

2. Create **user** user in **Usuarios** group
    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Usuarios" -Member "user"
    ```

2. Change **C:\admin** folder privileges

    ![](install.es.images/folder_admin_1.png){width="60%"}

    Disable **inheritance** of the folder and check the first option

    ![](install.es.images/folder_admin_2.png){width="60%"}

    All other users with privileges are **removed** to leave only **Administradores**

    ![](install.es.images/folder_admin_3.png){width="60%"}


### Uninstall applications and modifying Microsoft settings

1. In a **CMD** with administrator privileges, uninstall **Microsoft Edge**:

    ```
    cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\9*\Installer
    setup --uninstall --force-uninstall --system-level
    ```

2. In a **Powershell** with administrator privileges, uninstall the following packages and applications:

    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```

3. Open **msconfig** by pressing **Windows + R** or from a **CMD**, and disable the following services (restart the system if requested):

    - Administrador de autenticación Xbox Live
    - Centro de seguridad
    - Firewall de Windows Defender
    - Mozilla Maintenance Service
    - Servicio de antivirus de Microsoft Defender
    - Servicio de Windows Update Medic
    - Windows Update
    - Adobe Acrobat Update Service
    - Servicio de Google Update (gupdate)
    - Servicio de Google Update (gupdatem)
    - Google Chrome Elevation Service

4. The **Adminitrador de tareas** is opened and in the **Inicio** tab **disable** (restart the system if requested):

    - Cortana
    - Microsoft OneDrive
    - Windows Security notifications

5. The items that appear on the right in the **Inicio**, **Productividad** and **Explorar** menus are removed:

    ![](install.es.images/37.png){width="50%"}

6. Windows notifications are disabled in **Configuración - Sistema - Notificaciones y acciones**

    ![](install.es.images/38.png){width="50%"}

7. Connections via **Remote Desktop** are enabled. The first checkbox is enabled, and the second checkbox is disabled in **Configuración avanzada**.

    ![](install.es.images/39.png){width="50%"}

    ![](../win20192022/install.es.images/21-es.png){width="38%"}
    ![](../win20192022/install.es.images/22-es.png){width="39%"}

8. Shut-down the desktop and [edit it](../../../user/edit_desktop) with the following virtual hardware:

    - Viewers: **RDP** and **Browser RDP**
    - Login RDP:
        - User: **isard**
        - Password: **pirineus**
    - vCPUS: **4**
    - Memory (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Networks: **Default** and **Wireguard VPN**


### Autologon

1. To enable automatic login at system startup, the **[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)** is installed.

2. Unzip the downloaded file and run **Autologon64**.

3. Enter the password **pirineus**.

    ![](install.es.images/40.png){width="50%"}


### Catalan (optional)

Before running Optimization Tools, change the language to Catalan by following the steps below; otherwise, you can skip this section.

1. In **Configuración - Hora e idioma - Idioma** add **Idioma preferido** **Català** (logout if requested)

    ![](install.es.images/idioma_preferido.png){width="60%"}

    ![](install.es.images/42.png){width="25%"}
    ![](install.es.images/43.png){width="25%"}
    ![](install.es.images/44.png){width="25%"}

2. The change is replicated throughout the system (reboot the system if requested).

    ![](install.es.images/idioma_administrativo.png){width="60%"}

    ![](install.es.images/45.png){width="30%"}
    ![](install.es.images/46.png){width="29%"}


## Optimization tools

1. Run file from desktop assigned media

    ![](install.es.images/47.png){width="45%"}

2. Press button **Analyze** and then **Common options**

    ![](install.es.images/48.png){width="45%"}
    ![](install.es.images/49.png){width="45%"}

3. Configure options as in images below

    ![](install.es.images/50.png){width="45%"}
    ![](install.es.images/51.png){width="45%"}

    ![](install.es.images/52.png){width="45%"}
    ![](install.es.images/53.png){width="45%"}

    ![](install.es.images/54.png){width="45%"}
    ![](install.es.images/55.png){width="45%"}

    ![](install.es.images/56.png){width="45%"}
    ![](install.es.images/57.png){width="45%"}

??? info "**IF CATALAN LANGUAGE IS SET, IF NOT SKIP**"
    Filter by the word **language** and deactivate the following 3 options.

    ![](install.es.images/58.png){width="45%"}

4. Press button **Optimize** and wait until second screen shows up with result resume

    ![](install.es.images/59.png){width="45%"}

5. Reboot the system

## Windows Defender

### Maximum CPU Usage

By default, Microsoft Defender antivirus uses a maximum of 50% CPU. This can be verified with the PowerShell order:

```
Get-MpPreference | select ScanAvgCPULoadFactor
```

| **Windows 10** | **Windows 11** |
|---------------| ----------------|
| ![](install.es.images/81.png) | ![](install.es.images/78.png) |

#### Specify maximum CPU usage in PowerShell

With the command ``` Set-MpPreference -ScanAvgCPULoadFactor 25``` [(Microsoft Documentation)](https://learn.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2022-ps#-scanavgcpuloadfactor) in administrator mode can be limited to 25% of the CPU. By changing the number, you can specify the maximum percentage with a number from 5 to 100 and 0 disabling it.

### Results

#### CPU Usage in a Quick Exam

When performing a system scan, with a machine set to 100% (left) and a machine set to 5% (right) the results are the same and the quick scan uses 25% of the processor.

![](install.es.images/79.png){width="80%"}

### Exclusions

The following low-risk file types and directories should be added to the exclusion list:

![](install.es.images/80.png){width="60%"}

!!! note "Note"
    Windows does not allow swap files to be added to exclusions

