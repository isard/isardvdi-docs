# Guía de instalación Windows 10

## Pre-instalación

1. Se descarga la imagen ISO de [Windows 10](https://www.microsoft.com/es-es/software-download/windows10ISO) para la versión de 64 bits.

2. Se [crea un escritorio a partir de ese media](../../../advanced/media.es/#generar-escritorio-a-partir-de-media) con el siguiente hardware virtual:

    - Visores: **SPICE**
    - vCPUS: **4**
    - Memoria (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Tamaño del disco (GB): **100**
    - Redes: **Default**
    - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

3. Se [edita el escritorio y se asignan más media](../../../user/edit_desktop.es/#media), de tal forma que acabe teniendo los siguientes:

    - **Win10_22H2_ES** (instalador)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (software de optimización para S.O. Windows)


## Instalación

### Windows 10 Pro

1. Se **envia "Ctr+Alt+Supr"** y se pulsa cualquier tecla del teclado en la segunda pantalla

    ![](install.es.images/1.png){width="46%"}
    ![](install.es.images/2.png){width="45%"}

2. Tipo de instalación

    ![](install.es.images/3.png){width="45%"}
    ![](install.es.images/4.png){width="45%"}

    ![](install.es.images/5.png){width="45%"}
    ![](install.es.images/6.png){width="45%"}

    ![](install.es.images/7.png){width="45%"}

3. Cargar drivers de sistema operativo 

    ![](install.es.images/8.png){width="45%"}
    ![](install.es.images/9.png){width="45%"}

    ![](install.es.images/10.png){width="45%"}
    ![](install.es.images/11.png){width="45%"}

    ![](install.es.images/12.png){width="45%"}


### Sistema

1. Se apaga y [edita el escritorio](../../../user/edit_desktop.es/#hardware) para modificar el hardware virtual **Boot** de la opción **CD/DVD** a **Hard Disk**

2. Se arranca el escritorio, tardará unos segundos en iniciar el sistema operativo

    ![](install.es.images/13.png){width="45%"}

3. Seleccionar las siguientes opciones en los siguientes pasos del agente

    ![](install.es.images/14.png){width="45%"}
    ![](install.es.images/15.png){width="45%"}

    ![](install.es.images/16.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

    ![](install.es.images/18.png){width="45%"}
    ![](install.es.images/19.png){width="45%"}

    ![](install.es.images/20.png){width="45%"}
    ![](install.es.images/21.png){width="45%"}

    ![](install.es.images/22.png){width="45%"}
    ![](install.es.images/23.png){width="45%"}

    ![](install.es.images/24.png){width="45%"}
    ![](install.es.images/25.png){width="45%"}

    ![](install.es.images/26.png){width="45%"}
    ![](install.es.images/27.png){width="45%"}

    ![](install.es.images/28.png){width="45%"}
    ![](install.es.images/29.png){width="45%"}

    ![](install.es.images/30.png){width="45%"}
    ![](install.es.images/31.png){width="45%"}

4. Se apaga el escritorio y se edita para dejarle solo los siguientes media (si se quiere obviar este paso, igualmente habrá que apagar el escritorio y volver a iniciarlo, **no reiniciar**):

    - **virtio-win-X** (drivers)
    - **Optimization Tools** (software de optimización para S.O. Windows)


## Configuración

### Actualizar e instalar

1. Se instalan los dos drivers **virtio** del media del escritorio. Ambas instalaciones son rápidas y sólo hay que darle al botón **Next**

    ![](install.es.images/32.png){width="45%"}
    ![](install.es.images/33.png){width="45%"}

    ![](install.es.images/34.png){width="45%"}
    ![](install.es.images/35.png){width="45%"}

    ![](install.es.images/36.png){width="80%"}

2. Se comprueban las **actualizaciones del sistema**, las cuales van a tardar bastante en descargarse e instalarse, para estar al día con la última versión de Windows (seguramente se necesite reiniciar el sistema varias veces en busca de nuevas actualizaciones).

    ![](install.es.images/41.png){width="80%"}

3. Se instalan los programas siguientes y se guardan sus instaladores en la carpeta **admin** nueva en la ruta **C:\admin**

    - Firefox
    - Google chrome
    - Libre Office
    - Gimp
    - Inkscape
    - LibreCAD
    - Geany
    - Adobe Acrobat Reader


### Usuario admin y cambio de permisos

En una **Powershell** con permisos de administrador:

1. Crear usuario **admin** en el grupo **Administradores**
    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administradores" -Member "admin"
    ```

2. Crear usuario **user** en el grupo **Usuarios**
    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Usuarios" -Member "user"
    ```

2. Se le cambian los permisos a la carpeta **C:\admin** 

    ![](install.es.images/folder_admin_1.png){width="60%"}

    Se **deshabilita la herencia** de la carpeta y se marca la primera opción.

    ![](install.es.images/folder_admin_2.png){width="60%"}

    Se **quitan** los demás usuarios con permisos para únicamente dejar **Administradores**

    ![](install.es.images/folder_admin_3.png){width="60%"}


### Desinstalar aplicaciones y modificar configuraciones de Microsoft 

1. En una **CMD** con permisos de administrador, se desinstala **Microsoft Edge**:

    ```
    cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\9*\Installer
    setup --uninstall --force-uninstall --system-level
    ```

2. En una **Powershell** con permisos de administrador, se desinstalan los siguientes paquetes y programas:

    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```

3. Se abre **msconfig** mediante las teclas **Windows + R** o desde una **CMD**, y se desactivan los servicios siguientes (se reinicia el equipo si lo pide):

    - Administrador de autenticación Xbox Live
    - Centro de seguridad
    - Firewall de Windows Defender
    - Mozilla Maintenance Service
    - Servicio de antivirus de Microsoft Defender
    - Servicio de Windows Update Medic
    - Windows Update
    - Adobe Acrobat Update Service
    - Servicio de Google Update (gupdate)
    - Servicio de Google Update (gupdatem)
    - Google Chrome Elevation Service

4. Se abre el **Administrador de tareas** y en la pestaña **Inicio** se **deshabilita** (se reinicia el equipo si lo pide):

    - Cortana
    - Microsoft OneDrive
    - Windows Security notifications

5. Se quitan los elementos que aparecen en la derecha en el menú **Inicio**, **Productividad** y **Explorar**:

    ![](install.es.images/37.png){width="50%"}

6. Se desactivan las notificaciones de Windows en **Configuración - Sistema - Notificaciones y acciones**

    ![](install.es.images/38.png){width="50%"}

7. Se habilitan las conexiones por **Escritorio Remoto**. Se habilita la primera casilla, y se deshabilita la segunda casilla en **Configuración avanzada**

    ![](install.es.images/39.png){width="50%"}

    ![](../win20192022/install.es.images/21-es.png){width="38%"}
    ![](../win20192022/install.es.images/22-es.png){width="39%"}

8. Se apaga y [edita el escritorio](../../../user/edit_desktop.es) con el siguiente hardware virtual

    - Visores: **RDP** y **RDP en el navegador**
    - Login RDP:
        - Usuario: **isard**
        - Contraseña: **pirineus**
    - vCPUS: **4**
    - Memoria (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Redes: **Default** y **Wireguard VPN**


### Autologon

1. Para habilitar el inicio de sesión automático conforme se inicia el sistema, se instala el **[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)**.

2. Se descomprime el archivo descargado y se ejecuta **Autologon64**

3. Se escribe la contraseña **pirineus**

    ![](install.es.images/40.png){width="50%"}


### Catalán (opcional)

Antes de pasar las Optimization Tools, se cambia el idioma al catalán siguiendo los pasos a continuación; si no, se puede obviar este apartado.

1. En **Configuración - Hora e idioma - Idioma** se añade el **Idioma preferido** **Català** (se cierra sesión si lo pide)

    ![](install.es.images/idioma_preferido.png){width="60%"}

    ![](install.es.images/42.png){width="25%"}
    ![](install.es.images/43.png){width="25%"}
    ![](install.es.images/44.png){width="25%"}

2. Se replica el cambio en todo el sistema (se reinicia el equipo si lo pide)

    ![](install.es.images/idioma_administrativo.png){width="60%"}

    ![](install.es.images/45.png){width="30%"}
    ![](install.es.images/46.png){width="29%"}


## Optimization tools

1. Se abre el ejecutable del media asignado al escritorio

    ![](install.es.images/47.png){width="45%"}

2. Se presiona el botón **Analizar** y seguidamente **Opciones comunes**

    ![](install.es.images/48.png){width="45%"}
    ![](install.es.images/49.png){width="45%"}

3. Se configuran las opciones como en las siguientes imágenes

    ![](install.es.images/50.png){width="45%"}
    ![](install.es.images/51.png){width="45%"}

    ![](install.es.images/52.png){width="45%"}
    ![](install.es.images/53.png){width="45%"}

    ![](install.es.images/54.png){width="45%"}
    ![](install.es.images/55.png){width="45%"}

    ![](install.es.images/56.png){width="45%"}
    ![](install.es.images/57.png){width="45%"}

??? info "**SI EL IDIOMA CATALÁN ESTÁ DEFINIDO, SI NO, OMITIR**"
    Se filtra por la palabra **idioma** y se desactivan las siguientes 3 opciones:

    ![](install.es.images/58.png){width="45%"}

4. Se presiona el botón **Optimizar** y se espera a que salga una pantalla como la siguiente con el resumen del resultado

    ![](install.es.images/59.png){width="45%"}

5. Se reinicia el sistema

## Windows Defender

### Uso máximo de CPU

Por defecto el antivirus de Microsoft Defender utiliza el 50% de CPU como máximo. Esto se puede comprobar con el pedido de PowerShell:

```
Get-MpPreference | select ScanAvgCPULoadFactor
```

| **Windows 10** | **Windows 11** |
|---------------| ----------------|
| ![](install.es.images/81.png) | ![](install.es.images/78.png) |

#### Especificar el uso máximo de CPU en PowerShell

Con el comando ``` Set-MpPreference -ScanAvgCPULoadFactor 25``` [(Documentación de Microsoft)](https://learn.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2022-ps#-scanavgcpuloadfactor) en modo administrador puede limitarse al 25% de la CPU. Cambiando el número, se puede especificar el porcentaje máximo con un número del 5 al 100 y 0 que lo desactiva.

### Resultados

#### Uso de CPU en un Examen Rápido

Al realizar un escaneo del sistema, con una máquina configurada a 100% (izquierda) y una máquina configurada a 5% (derecha) los resultados son los mismos y el examen rápido utiliza un 25% del procesador.

![](install.es.images/79.png){width="80%"}

### Exclusiones

Se añade los siguientes tipos de archivo y directorios de bajo riesgo a la lista de exclusiones:

![](install.es.images/80.png){width="60%"}

!!! note "Nota"
    Windows no deja añadir los archivos de swap a las exclusiones

