# Debian Desktop 11.6.0 install

## Instal·lació del SO

![](installation.images/4.png){width="45%"}
![](installation.images/6.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

**Inici de sessió - GNOME Xorg**

![](installation.images/gnome_xorg_big.png){width="70%"}


### Terminal

**Comandaments bàsic**
```
$ su -
$ visudo 
#add this at the end of file
isard   ALL=(ALL:ALL)   ALL
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```


### Paràmetres

**Privacitat - Pantalla de bloqueig**

![](installation.images/privacy-screen_lock.png){width="70%"}

**Energia - Supensió automàtica**

![](installation.images/power-automatic_suspend.png){width="70%"}

**Software - Menú dretà - Preferències d'actualitzacions**

![](installation.images/software-menu-update_preferences.png){width="70%"}


## Personalització
### Terminal

**Canviar el fons de pantalla del login**
```
$ sudo apt install flatpak
$ wget https://dl.flathub.org/repo/appstream/io.github.realmazharhussain.GdmSettings.flatpakref
$ flatpak install ./io.github.realmazharhussain.GdmSettings.flatpakref
$ flatpak run io.github.realmazharhussain.GdmSettings
```

![](installation.images/login_screen_background.png){width="70%"}


### Paràmetres

**Fons de pantalla**

![](installation.images/background.png){width="70%"}

**Usuaris**

![](installation.images/users.png){width="70%"}