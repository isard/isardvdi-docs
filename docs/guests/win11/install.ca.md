# Guia d'instal·lació Windows 11

Per a fer la instal·lació de Windows 11, es té que:

1. Anar a la pàgina web de Microsoft i descarregar la imatge ISO de [Windows 11](https://www.microsoft.com/es-es/software-download/windows11) versió 64 bits en l'idioma que es prefereixi.

2. Es descarrega l'última versió estable dels [drivers de virtio](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/)


## Pre-instal·lació

1. Es [crea un escriptori a partir del mitjà pujat](../../../advanced/media.ca/#crear-nou-escriptori-a-partir-de-mitja) amb el següent maquinari virtual:

    !!! tip "Recomanat"
        - Visors: **SPICE**/**VNC**
        - vCPUS: **4**
        - Memoria (GB): **6**
        - Videos: **Default**
        - Boot: **CD/DVD**
        - Disk Bus: **Default**
        - Tamany del disc (GB): **140**
        - Xarxes: **Default**
        - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

2. S'[edita l'escriptori i se li afegeixen aquests mitjans](../../../user/edit_desktop.ca/#media):

    - **Win11_22H2_ES** (instal·lador)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (programari d'optimització per a S.O. Windows)

!!! warning "Important"
    **Si es vol tenir xarxa durant el procés d'instal·lació, s'ha d'escollir la xarxa "Intel1000"**

### Edició XML

És important editar l'xml de l'escriptori ja que sinó la instal·lació no es farà. Per a poder editar l'xml, s'ha de tenir un [**rol "manager" o "admin".**](../../../manager/index.ca/#rols)

![](install.es.images/1.png){width="45%"}


#### OS

En l'apartat de "os" dins de l'xml s'ha de modificar i canviar de "pc-*i440fx-2.8" a "q35" i afegir uns elements:

```
<os>
    <boot dev="hd"/>
    <type arch="x86_64" machine="q35">hvm</type>
    <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
</os>
```

#### Hyperv

En l'apartat d'<hyperv> s'ha de posar això, ja que Windows 11 ho recomana per a un millor rendiment:

```
<hyperv>
    <relaxed state="on"/>
    <vapic state="on"/>
    <spinlocks state="on" retries="8191"/>
    <synic state="on"/>
    <stimer state="on"/>
    <vpindex state="on"/>
    <tlbflush state="on"/>
    <ipi state="on"/>
</hyperv>
    
```

#### TMP
    
És un requisit de Windows 11 i s'ha d'afegir:
    
```
<tpm model="tpm-tis">
    <backend type="emulator" version="2.0"/>
</tpm>
```

#### Audio
    
Forcem que l'àudio surti per Spice amb:

```
<sound model="ich9">
    <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
</sound>
<audio id="1" type="spice"/>
```

#### Channel

```
<channel type="spicevmc">
    <target type="virtio" name="com.redhat.spice.0"/>
    <address type="virtio-serial" controller="0" bus="0" port="1"/>
</channel>
```

#### Devices PCIe + USB

```
<controller type="usb" index="0" model="qemu-xhci" ports="15">
    <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
</controller>
    <controller type="pci" index="0" model="pcie-root"/>
    <controller type="pci" index="1" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="1" port="0x10"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="2" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="2" port="0x11"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
    </controller>
    <controller type="pci" index="3" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="3" port="0x12"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
    </controller>
    <controller type="pci" index="4" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="4" port="0x13"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
    </controller>
    <controller type="pci" index="5" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="5" port="0x14"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
    </controller>
    <controller type="pci" index="6" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="6" port="0x15"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
    </controller>
    <controller type="pci" index="7" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="7" port="0x16"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
    </controller>
    <controller type="pci" index="8" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="8" port="0x17"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
    </controller>
    <controller type="pci" index="9" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="9" port="0x18"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="10" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="10" port="0x19"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
    </controller>
    <controller type="pci" index="11" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="11" port="0x1a"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
    </controller>
    <controller type="pci" index="12" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="12" port="0x1b"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
    </controller>
    <controller type="pci" index="13" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="13" port="0x1c"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
    </controller>
    <controller type="pci" index="14" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="14" port="0x1d"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
    </controller>
    <controller type="sata" index="0">
      <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
    </controller>
    <controller type="virtio-serial" index="0">
      <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
    </controller>
    <serial type="pty">
      <target type="isa-serial" port="0">
        <model name="isa-serial"/>
      </target>
    </serial>
```

#### Memballoon
    
```
<memballoon model="virtio">
    <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
</memballoon>
```
    
#### Console
    
```
 <console type="pty">
    <target type="serial" port="0"/>
</console>
```


<details>
  <summary>Exemple de XML per a Windows 11</summary>
  <p><b>Els "#" representen ids, cada id és diferent</b></p>
```
    <domain type="kvm">
    <name>#########</name>
    <uuid>#########</uuid>
    <metadata>
        <libosinfo:libosinfo xmlns:libosinfo="http://libosinfo.org/xmlns/libvirt/domain/1.0">
        <libosinfo:os id="http://microsoft.com/win/10"/>
        </libosinfo:libosinfo>
    </metadata>
    <memory unit="KiB">######</memory>
    <currentMemory unit="KiB">######</currentMemory>
    <vcpu placement="static">#</vcpu>
    <os>
        <boot dev="hd"/>
        <type arch="x86_64" machine="q35">hvm</type>
        <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
    </os>
    <features>
        <acpi/>
        <apic/>
        <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
        <synic state="on"/>
        <stimer state="on"/>
        <vpindex state="on"/>
        <tlbflush state="on"/>
        <ipi state="on"/>
        </hyperv>
        <vmport state="off"/>
    </features>
    <cpu mode="host-model">
        <topology sockets="1" dies="1" cores="4" threads="1"/>
    </cpu>
    <clock offset="localtime">
        <timer name="rtc" tickpolicy="catchup"/>
        <timer name="pit" tickpolicy="delay"/>
        <timer name="hpet" present="no"/>
        <timer name="hypervclock" present="yes"/>
    </clock>
    <pm>
        <suspend-to-mem enabled="no"/>
        <suspend-to-disk enabled="no"/>
    </pm>
    <devices>
        <emulator>/usr/bin/qemu-kvm</emulator>
        <disk type="file" device="disk">
        <driver name="qemu" type="qcow2"/>
        <source file="/isard/groups/##############.qcow2"/>
        <target dev="vda" bus="virtio"/>
        </disk>
        <disk type="file" device="cdrom">
        <driver name="qemu" type="raw"/>
        <source file="/isard/media/###########.iso"/>
        <target dev="sda" bus="sata"/>
        <readonly/>
        </disk>
        <controller type="usb" index="0" model="qemu-xhci" ports="15">
        <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
        </controller>
        <controller type="pci" index="0" model="pcie-root"/>
        <controller type="pci" index="1" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="1" port="0x10"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="2" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="2" port="0x11"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
        </controller>
        <controller type="pci" index="3" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="3" port="0x12"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
        </controller>
        <controller type="pci" index="4" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="4" port="0x13"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
        </controller>
        <controller type="pci" index="5" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="5" port="0x14"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
        </controller>
        <controller type="pci" index="6" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="6" port="0x15"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
        </controller>
        <controller type="pci" index="7" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="7" port="0x16"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
        </controller>
        <controller type="pci" index="8" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="8" port="0x17"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
        </controller>
        <controller type="pci" index="9" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="9" port="0x18"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="10" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="10" port="0x19"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
        </controller>
        <controller type="pci" index="11" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="11" port="0x1a"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
        </controller>
        <controller type="pci" index="12" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="12" port="0x1b"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
        </controller>
        <controller type="pci" index="13" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="13" port="0x1c"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
        </controller>
        <controller type="pci" index="14" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="14" port="0x1d"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
        </controller>
        <controller type="sata" index="0">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
        </controller>
        <controller type="virtio-serial" index="0">
        <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
        </controller>
        <serial type="pty">
        <target type="isa-serial" port="0">
            <model name="isa-serial"/>
        </target>
        </serial>
        <console type="pty">
        <target type="serial" port="0"/>
        </console>
        <channel type="spicevmc">
        <target type="virtio" name="com.redhat.spice.0"/>
        <address type="virtio-serial" controller="0" bus="0" port="1"/>
        </channel>
        <interface type="network">
        <source network="default"/>
        <mac address="52:54:00:43:ba:0b"/>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <interface type="bridge">
        <source bridge="ovsbr0"/>
        <mac address="52:54:00:0f:0f:a5"/>
        <virtualport type="openvswitch"/>
        <vlan>
            <tag id="4095"/>
        </vlan>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <input type="tablet" bus="usb"/>
        <graphics type="spice" port="-1" tlsPort="-1" autoport="yes">
        <listen type="address" address="0.0.0.0"/>
        <image compression="auto_glz"/>
        <jpeg compression="always"/>
        <zlib compression="always"/>
        <playback compression="off"/>
        <streaming mode="all"/>
        </graphics>
        <sound model="ich9">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
        </sound>
        <audio id="1" type="spice"/>
        <video>
        <model type="qxl" ram="65536" vram="65536" vgamem="16384" heads="1" primary="yes"/>
        <alias name="video0"/>
        </video>
        <tpm model="tpm-tis">
        <backend type="emulator" version="2.0"/>
        </tpm>
        <redirdev bus="usb" type="spicevmc"/>
        <redirdev bus="usb" type="spicevmc"/>
        <memballoon model="virtio">
        <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
        </memballoon>
    </devices>
    </domain>
```
</details>


## Instal·lació

### Windows 11

Obrim l'escriptori amb Spice o VNC al navegador:

!!! Example "Visors"

    - Si es tria **Spice**, quan aparegui aquesta pantalla s'ha d'anar a "Send key" i prémer ++ctrl+alt+del++
    - Si es tria **VNC al navegador**, es prem el botó de la part superior dreta "Send ++ctrl+alt+del++ "

    === "Spice"
        ![](install.es.images/2.png){width="45%"}
        ![](install.es.images/3.png){width="45%"}

    === "VNC al navegador"
        ![](install.es.images/4.png)


Quan es prem el botó, apareixerà aquest missatge:

![](install.es.images/5.png){width="45%"}

En aquest moment es prem dins de la consola qualsevol tecla, per exemple ++enter++ i es força que s'inici l'instal·lador de windows 11

Apareix el boot d'efi i la càrrega de l'instal·lador de windows:

![](install.es.images/6.png){width="45%"}

1. Tipus d'instal·lació

    ![](install.ca.images/1.png){width="45%"}
    ![](install.es.images/8.png){width="45%"}
    ![](install.es.images/9.png){width="45%"}
    ![](install.es.images/10.png){width="45%"}
    ![](install.es.images/11.png){width="45%"}

2. Carregar drivers de sistema operatiu

    ![](install.es.images/12.png){width="45%"}
    ![](install.es.images/13.png){width="45%"}
    ![](install.es.images/14.png){width="45%"}
    ![](install.es.images/15.png){width="45%"}
    ![](install.es.images/16.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

### Sistema

1. S'atura i s'[edita l'escriptori](../../../user//edit_desktop.ca/#hardware), canviant així el mode **"Boot"** i s'escull "Hard disk"

    ![](install.es.images/18.png){width="45%"}

2. Es torna a iniciar l'escriptori, pot trigar uns minuts a iniciar el sistema.

    ![](install.es.images/19.png){width="45%"}

3. S'escullen les següents opcions, pot ser que no es redimensioni bé la pantalla en aquest moment i s'haurà de moure el contingut cap amunt o cap avall

    ![](install.es.images/20.png){width="45%"}
    ![](install.es.images/21.png){width="45%"}
    ![](install.es.images/22.png){width="45%"}
    ![](install.es.images/23.png){width="45%"}
    ![](install.es.images/24.png){width="45%"}
    ![](install.es.images/25.png){width="45%"}
    ![](install.es.images/26.png){width="45%"}
    ![](install.es.images/27.png){width="45%"}
    ![](install.es.images/28.png){width="45%"}
    ![](install.es.images/29.png){width="45%"}
    ![](install.es.images/30.png){width="45%"}
    ![](install.es.images/31.png){width="45%"}
    ![](install.es.images/32.png){width="45%"}
    ![](install.es.images/33.png){width="45%"}
    ![](install.es.images/34.png){width="45%"}
    ![](install.es.images/35.png){width="45%"}

4. S'atura l'escriptori, es treu la imatge d'instal·lació de Windows 11 i se li deixa aquests mitjans:

    - virtio-win-x (controladors)
    - Optimization Tools (programa d'optimització per a S.O Windows)


## Configuració

### Idioma Català (Opcional)

Abans de passar les **Optimization Tools**, es canvia l'idioma al català seguint els passos a continuació; si no, es pot obviar aquest apartat.

1. A **Configuración** --> **Hora e idioma** --> **Idioma** s'afegeix l'**Idioma preferido** Català (es tanca la sessió si ho demana)

    ![](install.ca.images/2.png){width="45%"}
    ![](install.ca.images/3.png){width="45%"}
    ![](install.ca.images/4.png){width="45%"}

2. Es replica el canvi a tot el sistema (es reinicia l'equip si ho demana)

    ![](install.ca.images/5.png){width="45%"}
    ![](install.ca.images/6.png){width="45%"}
    ![](install.ca.images/7.png){width="45%"}
    ![](install.ca.images/8.png){width="45%"}
    ![](install.ca.images/9.png){width="45%"}


### Actualitzar i instal·lar

1.  S'instal·len els dos controladors virtio del mitjà de l'escriptori (virtio-win-x). 

    ![](install.es.images/36.png){width="45%"}
    ![](install.es.images/37.png){width="45%"}
    ![](install.es.images/38.png){width="45%"}
    ![](install.es.images/39.png){width="45%"}
    ![](install.es.images/40.png){width="45%"}
    ![](install.es.images/41.png){width="45%"}
    ![](install.es.images/42.png){width="45%"}
    ![](install.es.images/43.png){width="45%"}
    ![](install.es.images/44.png){width="45%"}
    ![](install.es.images/45.png){width="45%"}

2.  Es comprova si hi ha actualitzacions del sistema i es descarreguen per a estar al dia amb l'última versió.

    ![](install.es.images/46.png){width="45%"}

3.  S'instal·len els següents programaris i es guarden els seus instal·ladors en una nova carpeta que anomenarem **admin** en la ruta **C:\admin**

    !!! info inline "Programaris"
        - Firefox
        - Google chrome
        - Libre Office
        - Gimp
        - Inkscape
        - LibreCAD
        - Geany
        - Adobe Acrobat Reader

    ![](install.ca.images/18.png){width="45%"}

### Usuari admin i canvi de permisos

S'obre una **Powershell** amb permisos d'administrador:

![](install.ca.images/19.png){width="45%"}

1. Es crea un usuari **admin** en el grup **Administradors**

    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administradors" -Member "admin"
    ```

    ![](install.es.images/50.png){width="45%"}

2. Es crea un usuari **user** en el grup **Usuaris**

    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Usuaris" -Member "user"
    ```
    ![](install.es.images/51.png){width="45%"}

3. Se li canvien els permisos a la carpeta **C:\admin**, botó dret en la carpeta i es selecciona **"Propietats"**. S'ha de **deshabilitar l'herència** de la carpeta i esborrar tots els usuaris que no són **Administradors**

    ![](install.ca.images/17.png){width="45%"}
    ![](install.es.images/53.png){width="45%"}
    ![](install.es.images/54.png){width="45%"}
    ![](install.es.images/55.png){width="45%"}
    ![](install.es.images/56.png){width="45%"}

### Desinstal·lar aplicacions i modificar configuracions de Microsoft

1. S'obre un **Powershell** amb permisos d'administrador, i es desinstal·len els següents paquets i programaris:

    ![](install.ca.images/19.png){width="45%"}

    !!! Warning "Informació important"
        La desinstal·lació d'aquests programaris comporta al fet que el Windows vagi més ràpidament i tingui més espai. És important notar que també s'eliminarà la **Microsoft Store**. Si no es vol eliminar, s'ha d'obviar l'ordre:
        ```
        Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
        ```
    <details>
        <summary>Ordres per a la desinstal·lació dels programaris en Powershell</summary>
        <p></p>
    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```
    </details>

2. S'obre **msconfig** mitjançant les tecles ++windows+r++ o des d'una CMD, i es desactiven els serveis següents (es reinicia l'equip si el demana):

    !!! info inline "Serveis"
        <font size="2">
            <ul>
            <li> Administrador d'autenticació Xbox Live</li>
            <li> Centre de seguretat</li>
            <li> Firewall de Windows Defender</li>
            <li> Mozilla Maintenance Service</li>
            <li> Servei d'antivirus de Microsoft Defensar</li>
            <li> Servei de Windows Update Medic</li>
            <li> Windows Update</li>
            <li> Adobe Acrobat Update Service</li>
            <li> Servei de Google Update (gupdate)</li>
            <li> Servei de Google Update (gupdatem)</li>
            <li> Google Chrome Elevation Service</li>
            </ul>
        </font>

    ![](install.es.images/58.png){width="70%"}


3. S'obre l'**Administrador de tasques** i en **aplicacions d'inici** s'inhabiliten:

    !!! info inline "Programaris"
        - Cortana
        - Teams
        - Microsoft OneDrive
        - Windows Security notifications

    ![](install.ca.images/15.png){width="50%"}

4. Es desactiven les notificacions de Windows en **Configuració** --> **Sistema** --> **Notificacions i accions**

    ![](install.ca.images/11.png){width="45%"}

5. S'habiliten les connexions per **Escriptori Remot**. S'habilita la primera casella, i es deshabilita la segona casella en **Configuració avançada**

    ![](install.ca.images/12.png){width="45%"}
    ![](install.ca.images/13.png){width="45%"}

6. Per a una millor visibilitat del punter del ratolí, és recomanable seleccionar el mode invertit:

    ![](install.ca.images/14.png){width="45%"}


7. Es desactiven aquests elements de la barra de tasques:

    ![](install.ca.images/10.png){width="45%"}


8. S'apaga i s'[edita l'escriptori](../../../user/edit_desktop.ca/) amb el següent maquinari virtual:

    !!! tip "Recomanat"
        - Visors: **RDP** y **RDP en el navegador**
        - Login RDP:
            - Usuari: **isard**
            - Contrasenya: **pirineus**
        - vCPUS: **4**
        - Memoria (GB): **8**
        - Videos: **Default**
        - Boot: **Hard Disk**
        - Disk Bus: **Default**
        - Xarxes: **Default** y **Wireguard VPN**


### Autologon

1. Per a habilitar l'inici de sessió automàtic, s'instal·la **l'[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)**.

    !!! Info
        Si ja es va afegir la imatge ISO de l'autlogon en l'apartat de mitjans en l'escriptori, no fa falta descarregar-lo.

2. Es descomprimeix l'arxiu descarregat i s'executa **Autologon64**. S'escriu la contrasenya **pirineus** (o la que es prefereixi).

    ![](install.es.images/63.png){width="45%"}


## Optimization tools

1. Es prem el botó **Analizar**, seguidament **Opciones comunes** i es configuren les opcions com en les següents imatges.

    ![](install.ca.images/20.png){width="45%"}
    ![](install.es.images/65.png){width="45%"}
    ![](install.es.images/66.png){width="45%"}
    ![](install.es.images/67.png){width="45%"}
    ![](install.es.images/68.png){width="45%"}
    ![](install.es.images/69.png){width="45%"}
    ![](install.es.images/70.png){width="45%"}
    ![](install.es.images/71.png){width="45%"}

    !!! Warning "Optimization Tools"
        - **Microsoft Store** : Si en el punt 1 de [la desinstal·lació d'aplicacions](../../../install.ca/#desinstallar-aplicacions-i-modificar-configuracions-de-microsoft) no s'ha esborrat la Microsoft Store, s'ha de desmarcar la casella d'**Aplicaciones de Store"**
        - **Idioma en Català** : Es filtra per la paraula **idioma** i es desactiven les següents 3 opcions

            === "Microsoft Store"
                ![](install.es.images/72.png){width="45%"}

            === "Idioma Català"
                ![](../win10/install.es.images/58.png){width="45%"}

    !!! Danger "Advertència!"
        S'ha de tenir cura en passar les **Optimization Tools** perquè si no es desmarquen les opcions assenyalades amunt, eliminarà el paquet d'idioma en català
 

2. S'exporta l'arxiu resultant en la carpeta **C:\admin**

    ![](install.es.images/73.png){width="45%"}
    ![](install.es.images/74.png){width="45%"}
    ![](install.es.images/75.png){width="45%"}

## Windows Defender

### Ús màxim de CPU

Per defecte l'antivirus de Microsoft Defender fa servir el 50% de CPU com a màxim. Això es pot comprovar amb la comanda de PowerShell:

```
Get-MpPreference | select ScanAvgCPULoadFactor
```

| **Windows 10** | **Windows 11** |
|---------------| ----------------|
| ![](install.es.images/81.png) | ![](install.es.images/78.png) |

#### Especificar l'ús màxim de CPU en PowerShell

Amb la comanda ``` Set-MpPreference -ScanAvgCPULoadFactor 25``` [(Documentació de Microsoft)](https://learn.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2022-ps#-scanavgcpuloadfactor) en mode administrador es pot limitar al 25% de la CPU. Canviant el número, es pot especificar el percentatge màxim amb un número del 5 al 100, i 0 que ho desactiva.

### Resultats

#### Ús de CPU en un Examen Ràpid

En fer un escaneig del sistema, amb una màquina configurada a 100% (esquerra) i una màquina configurada a 5% (dreta) els resultats són els mateixos i l'examen ràpid fa servir un 25% del processador.

![](install.es.images/79.png){width="80%"}

### Exclusions

S'afegeix els següents tipus d'arxiu i directoris de baix risc a la llista d'exclusions:

![](install.es.images/80.png){width="60%"}

!!! note "Nota"
    Windows no deixa afegir els arxius de swap a les exclusions