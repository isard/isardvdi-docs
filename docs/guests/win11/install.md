# Windows 11 Installation Guide

To install Windows 11, you have to:

1. Go to the Microsoft web and download the iso file of [Windows 11](https://www.microsoft.com/en-us/software-download/windows11) version 64 bits in the language you prefer.
2. Download the latest stable version of the [virtio drivers](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/)


## Pre Installation

1. [Create a desktop of the uploaded media](../../../advanced/media/#generate-desktop-from-media) with the following Hardware:

    !!! tip "Recommended"
        - Viewers: **SPICE**/**VNC**
        - vCPUS: **4**
        - Memory (GB): **6**
        - Videos: **Default**
        - Boot: **CD/DVD**
        - Disk Bus: **Default**
        - Disk Size (GB): **140**
        - Networks: **Default**
        - Template: **Microsoft windows 10 with Virtio devices UEFI**

2. [Edit the desktop and add](../../../user/edit_desktop/#media) the following media: 

    - **Win11_22H2_EN** (installer)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (optimization software for Windows O.S.)

!!! warning "Important"
    **If you want to have a network during the installation process, you have to select the network "Intel1000"**

### XML Edition

It's important to edit the xml of the desktop or else the installation will not work. To be able to edit the xml, you have to have a [**"manager" or "admin" role**](../../../manager/#roles) 

![](install.es.images/1.png){width="45%"}


#### OS

In the "os" section within the xml you have to modify and change from "pc-i440fx-2.8" to "q35" and add some elements:

```
<os>
    <boot dev="hd"/>
    <type arch="x86_64" machine="q35">hvm</type>
    <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
</os>
```

#### Hyperv

In the <hyperv> section you have to have this since Windows 11 recommends it for better performance:

```
<hyperv>
    <relaxed state="on"/>
    <vapic state="on"/>
    <spinlocks state="on" retries="8191"/>
    <synic state="on"/>
    <stimer state="on"/>
    <vpindex state="on"/>
    <tlbflush state="on"/>
    <ipi state="on"/>
</hyperv>
    
```

#### TMP
    
It is a requirement of Windows 11 and must be added:
    
```
<tpm model="tpm-tis">
    <backend type="emulator" version="2.0"/>
</tpm>
```

#### Audio
    
We force the audio to come out through Spice with this:

```
<sound model="ich9">
    <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
</sound>
<audio id="1" type="spice"/>
```

#### Channel

```
<channel type="spicevmc">
    <target type="virtio" name="com.redhat.spice.0"/>
    <address type="virtio-serial" controller="0" bus="0" port="1"/>
</channel>
```

#### Devices PCIe + USB

```
<controller type="usb" index="0" model="qemu-xhci" ports="15">
    <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
</controller>
    <controller type="pci" index="0" model="pcie-root"/>
    <controller type="pci" index="1" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="1" port="0x10"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="2" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="2" port="0x11"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
    </controller>
    <controller type="pci" index="3" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="3" port="0x12"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
    </controller>
    <controller type="pci" index="4" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="4" port="0x13"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
    </controller>
    <controller type="pci" index="5" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="5" port="0x14"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
    </controller>
    <controller type="pci" index="6" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="6" port="0x15"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
    </controller>
    <controller type="pci" index="7" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="7" port="0x16"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
    </controller>
    <controller type="pci" index="8" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="8" port="0x17"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
    </controller>
    <controller type="pci" index="9" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="9" port="0x18"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="10" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="10" port="0x19"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
    </controller>
    <controller type="pci" index="11" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="11" port="0x1a"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
    </controller>
    <controller type="pci" index="12" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="12" port="0x1b"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
    </controller>
    <controller type="pci" index="13" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="13" port="0x1c"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
    </controller>
    <controller type="pci" index="14" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="14" port="0x1d"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
    </controller>
    <controller type="sata" index="0">
      <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
    </controller>
    <controller type="virtio-serial" index="0">
      <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
    </controller>
    <serial type="pty">
      <target type="isa-serial" port="0">
        <model name="isa-serial"/>
      </target>
    </serial>
```

#### Memballoon
    
```
<memballoon model="virtio">
    <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
</memballoon>
```
    
#### Console
    
```
 <console type="pty">
    <target type="serial" port="0"/>
</console>
```


<details>
  <summary>Example of XML for Windows 11</summary>
  <p><b>The "#" represent ids, each id is different</b></p>
```
    <domain type="kvm">
    <name>#########</name>
    <uuid>#########</uuid>
    <metadata>
        <libosinfo:libosinfo xmlns:libosinfo="http://libosinfo.org/xmlns/libvirt/domain/1.0">
        <libosinfo:os id="http://microsoft.com/win/10"/>
        </libosinfo:libosinfo>
    </metadata>
    <memory unit="KiB">######</memory>
    <currentMemory unit="KiB">######</currentMemory>
    <vcpu placement="static">#</vcpu>
    <os>
        <boot dev="hd"/>
        <type arch="x86_64" machine="q35">hvm</type>
        <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
    </os>
    <features>
        <acpi/>
        <apic/>
        <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
        <synic state="on"/>
        <stimer state="on"/>
        <vpindex state="on"/>
        <tlbflush state="on"/>
        <ipi state="on"/>
        </hyperv>
        <vmport state="off"/>
    </features>
    <cpu mode="host-model">
        <topology sockets="1" dies="1" cores="4" threads="1"/>
    </cpu>
    <clock offset="localtime">
        <timer name="rtc" tickpolicy="catchup"/>
        <timer name="pit" tickpolicy="delay"/>
        <timer name="hpet" present="no"/>
        <timer name="hypervclock" present="yes"/>
    </clock>
    <pm>
        <suspend-to-mem enabled="no"/>
        <suspend-to-disk enabled="no"/>
    </pm>
    <devices>
        <emulator>/usr/bin/qemu-kvm</emulator>
        <disk type="file" device="disk">
        <driver name="qemu" type="qcow2"/>
        <source file="/isard/groups/##############.qcow2"/>
        <target dev="vda" bus="virtio"/>
        </disk>
        <disk type="file" device="cdrom">
        <driver name="qemu" type="raw"/>
        <source file="/isard/media/###########.iso"/>
        <target dev="sda" bus="sata"/>
        <readonly/>
        </disk>
        <controller type="usb" index="0" model="qemu-xhci" ports="15">
        <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
        </controller>
        <controller type="pci" index="0" model="pcie-root"/>
        <controller type="pci" index="1" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="1" port="0x10"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="2" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="2" port="0x11"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
        </controller>
        <controller type="pci" index="3" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="3" port="0x12"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
        </controller>
        <controller type="pci" index="4" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="4" port="0x13"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
        </controller>
        <controller type="pci" index="5" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="5" port="0x14"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
        </controller>
        <controller type="pci" index="6" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="6" port="0x15"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
        </controller>
        <controller type="pci" index="7" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="7" port="0x16"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
        </controller>
        <controller type="pci" index="8" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="8" port="0x17"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
        </controller>
        <controller type="pci" index="9" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="9" port="0x18"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="10" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="10" port="0x19"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
        </controller>
        <controller type="pci" index="11" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="11" port="0x1a"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
        </controller>
        <controller type="pci" index="12" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="12" port="0x1b"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
        </controller>
        <controller type="pci" index="13" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="13" port="0x1c"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
        </controller>
        <controller type="pci" index="14" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="14" port="0x1d"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
        </controller>
        <controller type="sata" index="0">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
        </controller>
        <controller type="virtio-serial" index="0">
        <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
        </controller>
        <serial type="pty">
        <target type="isa-serial" port="0">
            <model name="isa-serial"/>
        </target>
        </serial>
        <console type="pty">
        <target type="serial" port="0"/>
        </console>
        <channel type="spicevmc">
        <target type="virtio" name="com.redhat.spice.0"/>
        <address type="virtio-serial" controller="0" bus="0" port="1"/>
        </channel>
        <interface type="network">
        <source network="default"/>
        <mac address="52:54:00:43:ba:0b"/>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <interface type="bridge">
        <source bridge="ovsbr0"/>
        <mac address="52:54:00:0f:0f:a5"/>
        <virtualport type="openvswitch"/>
        <vlan>
            <tag id="4095"/>
        </vlan>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <input type="tablet" bus="usb"/>
        <graphics type="spice" port="-1" tlsPort="-1" autoport="yes">
        <listen type="address" address="0.0.0.0"/>
        <image compression="auto_glz"/>
        <jpeg compression="always"/>
        <zlib compression="always"/>
        <playback compression="off"/>
        <streaming mode="all"/>
        </graphics>
        <sound model="ich9">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
        </sound>
        <audio id="1" type="spice"/>
        <video>
        <model type="qxl" ram="65536" vram="65536" vgamem="16384" heads="1" primary="yes"/>
        <alias name="video0"/>
        </video>
        <tpm model="tpm-tis">
        <backend type="emulator" version="2.0"/>
        </tpm>
        <redirdev bus="usb" type="spicevmc"/>
        <redirdev bus="usb" type="spicevmc"/>
        <memballoon model="virtio">
        <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
        </memballoon>
    </devices>
    </domain>
```
</details>


## Installation

### Windows 11

We open the desktop with Spice or VNC browser viewer:

!!! Example "Viewers"

    - If **Spice** is chosen, when this screen appears, go to "Send key" and press ++ctrl+alt+del++
    - If **VNC browser** is chosen, press the button on the top right "Send ++ctrl+alt+del++ "

    === "Spice"
        ![](install.es.images/2.png){width="45%"}
        ![](install.es.images/3.png){width="45%"}

    === "VNC browser"
        ![](install.es.images/4.png)


When the button is pressed, this message will appear:

![](install.es.images/5.png){width="45%"}

At that moment, press any key within the console, for example ++enter++ and force the windows 11 installer to start.

The efi boot appears and the windows installer loads:

![](install.es.images/6.png){width="45%"}

1. Type of installation

    ![](install.images/1.png){width="45%"}
    ![](install.images/2.png){width="45%"}
    ![](install.images/3.png){width="45%"}
    ![](install.images/4.png){width="45%"}
    ![](install.images/5.png){width="45%"}

2. Load operating system drivers

    ![](install.images/6.png){width="45%"}
    ![](install.images/7.png){width="45%"}
    ![](install.images/8.png){width="45%"}
    ![](install.images/9.png){width="45%"}
    ![](install.images/10.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

### System

1. Shut down and [edit the desktop](../../../user/edit_desktop/#hardware), changing the **"Boot"** mode and select "Hard disk". 

    ![](install.es.images/18.png){width="45%"}

2. Reboot the desktop, it may take a few minutes for the system to boot.

    ![](install.images/11.png){width="45%"}

3. Select the following options, the screen may not resize well at this time and you will have to move the content up or down (scroll)

    ![](install.images/12.png){width="45%"}
    ![](install.images/13.png){width="45%"}
    ![](install.images/14.png){width="45%"}
    ![](install.images/15.png){width="45%"}
    ![](install.images/16.png){width="45%"}
    ![](install.images/17.png){width="45%"}
    ![](install.images/18.png){width="45%"}
    ![](install.images/19.png){width="45%"}
    ![](install.images/20.png){width="45%"}
    ![](install.images/21.png){width="45%"}
    ![](install.images/22.png){width="45%"}
    ![](install.images/23.png){width="45%"}
    ![](install.images/24.png){width="45%"}
    ![](install.images/25.png){width="45%"}
    ![](install.images/26.png){width="45%"}
    ![](install.images/27.png){width="45%"}

4. Turn off the desktop, remove the Windows 11 installation image and leave this media:

    - virtio-win-x (controllers)
    - Optimization Tools (optimization program for Windows O.S.)


## Configuration

### Update and install

1. Install the two virtio drivers from the desktop media (virtio-win-x). 

    ![](install.images/29.png){width="45%"}
    ![](install.images/30.png){width="45%"}
    ![](install.images/31.png){width="45%"}
    ![](install.images/32.png){width="45%"}
    ![](install.images/33.png){width="45%"}
    ![](install.images/34.png){width="45%"}
    ![](install.es.images/42.png){width="45%"}
    ![](install.es.images/43.png){width="45%"}
    ![](install.es.images/44.png){width="45%"}
    ![](install.es.images/45.png){width="45%"}

2. Check if there are system updates and downloaded to keep up with the latest version.

    ![](install.images/35.png){width="45%"}

3. Install the following programs and save their installers in a new folder that we will name **admin** in the path **C:\admin**

    !!! info inline "Applications"
        - Firefox
        - Google chrome
        - Libre Office
        - Gimp
        - Inkscape
        - LibreCAD
        - Geany
        - Adobe Acrobat Reader

    ![](install.images/36.png){width="45%"}

### User admin and privilege changes

Open a **Powershell** with administrator privilege:

![](install.images/37.png){width="45%"}


1. Create an **admin** user in the **Administrators** group

    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administrator" -Member "admin"
    ```

    ![](install.es.images/50.png){width="45%"}

2. Create an **user** user in the **Users** group

    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Standard User" -Member "user"
    ```
    ![](install.es.images/51.png){width="45%"}

3. Change the permissions to the folder **C:\admin**, right click on the folder and select **"Properties"**. You have to **disable inheritance** of the folder and delete all users who are not **Administrators**

    ![](install.images/58.png){width="45%"}
    ![](install.images/59.png){width="45%"}
    ![](install.images/60.png){width="45%"}
    ![](install.images/61.png){width="45%"}
    ![](install.images/62.png){width="45%"}

### Uninstall applications and modifying Microsoft settings

1. Open a **Powershell** with administrator privilege, and uninstall the following packages and programs:

    ![](install.images/37.png){width="45%"}

    !!! Warning "Important Information"
        Uninstalling these programs leads to faster Windows and more space. It is important to note that the **Microsoft Store** will also be removed. If you do not want to delete it, you have to ignore the command:

        ```
        Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
        ```
    <details>
        <summary>Commands for uninstalling programs in Powershell</summary>
        <p></p>
    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```
    </details>

2. Open **msconfig** using the keys ++windows+r++ or from a CMD, and disable the following services (restar the computer if requested):

    !!! info inline "Services"
        <font size="2">
            <ul>
            <li> Xbox Live Auth Manager</li>
            <li> Security center</li>
            <li> Windows Defender Firewall</li>
            <li> Mozilla Maintenance Service</li>
            <li> Microsoft Defender Antivirus Service</li>
            <li> Windows Update Medic Service</li>
            <li> Windows Update</li>
            <li> Adobe Acrobat Update Service</li>
            <li> Google Update Service(gupdate)</li>
            <li> Google Update Service(gupdatem)</li>
            <li> Google Chrome Elevation Service</li>
            </ul>
        </font>
    ![](install.images/38.png){width="70%"}


3. Open the **Task Manager** and in **startup apps** disable:

    !!! info inline "Applications"
        - Cortana
        - Teams
        - Microsoft OneDrive
        - Windows Security notifications

    ![](install.images/39.png){width="50%"}

4. Disable the Windows notifications in **Settings** --> **System** --> **Notifications**

    ![](install.images/40.png){width="45%"}

5. Enable the connections by **Remote Desktop**. Enable the first checkbox, and disable the second checkbox in **Advanced Settings**

    ![](install.images/41.png){width="45%"}
    ![](install.images/42.png){width="45%"}

6. For better visibility of the mouse pointer, it is recommended to select the inverted mode:

    ![](install.images/43.png){width="45%"}

7. Disable these taskbar items:

    ![](install.images/57.png){width="45%"}

8. Shut down and [edit the desktop](../../../user/edit_desktop) with the following virtual hardware:

    !!! tip "Recommended"
        - Viewers: **RDP** and **RDP browser**
        - Login RDP:
            - User: **isard**
            - Password: **pirineus**
        - vCPUS: **4**
        - Memory (GB): **8**
        - Videos: **Default**
        - Boot: **Hard Disk**
        - Disk Bus: **Default**
        - Network: **Default** y **Wireguard VPN**


### Autologon

1. To enable automatic logon as the system boots, install **[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)**.

    !!! Info
        If the autologon iso has already been added to the media section on the desktop, there is no need to download it.

2. Unzip the downloaded file and execute **Autologon64**. Enter the password **pirineus** (or whatever you prefer).

    ![](install.images/44.png){width="45%"}


## Optimization tools

1. Press the **Analyze** button, then **Common Options** and configure the options as in the following images.

    ![](install.images/45.png){width="45%"}
    ![](install.images/46.png){width="45%"}
    ![](install.images/47.png){width="45%"}
    ![](install.images/48.png){width="45%"}
    ![](install.images/49.png){width="45%"}
    ![](install.images/50.png){width="45%"}
    ![](install.images/51.png){width="45%"}
    ![](install.images/52.png){width="45%"}

    !!! warning "Important"
        If in point 1 of [uninstall of the applications](../../../install/#uninstall-applications-and-modifying-microsoft-settings) the Microsoft Store has not been deleted, you have to uncheck the box for **"Store Apps"**

        ![](install.images/53.png){width="45%"}

2. Export the resulting file in the folder **C:\admin**

    ![](install.images/54.png){width="45%"}
    ![](install.images/55.png){width="45%"}
    ![](install.images/56.png){width="45%"}

## Windows Defender

### Maximum CPU Usage

By default, Microsoft Defender antivirus uses a maximum of 50% CPU. This can be verified with the PowerShell order:

```
Get-MpPreference | select ScanAvgCPULoadFactor
```

| **Windows 10** | **Windows 11** |
|---------------| ----------------|
| ![](install.es.images/81.png) | ![](install.es.images/78.png) |

#### Specify maximum CPU usage in PowerShell

With the command ``` Set-MpPreference -ScanAvgCPULoadFactor 25``` [(Microsoft Documentation)](https://learn.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2022-ps#-scanavgcpuloadfactor) in administrator mode can be limited to 25% of the CPU. By changing the number, you can specify the maximum percentage with a number from 5 to 100 and 0 disabling it.

### Results

#### CPU Usage in a Quick Exam

When performing a system scan, with a machine set to 100% (left) and a machine set to 5% (right) the results are the same and the quick scan uses 25% of the processor.

![](install.es.images/79.png){width="80%"}

### Exclusions

The following low-risk file types and directories should be added to the exclusion list:

![](install.es.images/80.png){width="60%"}

!!! note "Note"
    Windows does not allow swap files to be added to exclusions