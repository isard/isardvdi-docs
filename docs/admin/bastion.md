# Bastion

The Bastion feature allows access to virtual desktops through the same server running the IsardVDI web interface, instead of relying exclusively on a VPN connection (Wireguard).

This is particularly useful in environments where public servers are required, as it allows these desktops to be accessible without needing a VPN.

When the Bastion is activated, the IsardVDI server acts as a gateway for external connections:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

The accessible default ports for the desktops are:

- **2222/TCP:** SSH Service
- **80/TCP:** Web
- **443/TCP:** Web TLS

## Set up

- IsardVDI version: Bastion is available from version [v14.30.0](https://gitlab.com/isard/isardvdi/-/releases/v14.30.0). Do an upgrade if needed to latest version.
- Add in your `isardvdi.cfg` the new bastion variables (or redo your cfg from new cfg.example):


```
# ------ Bastion -------------------------------------------------------------
BASTION_ENABLED=true
BASTION_SSH_PORT=2222
```

- rebuild your yml and bring the system up again (`bash build.sh; docker compose up -d`)

Now you will find in administration in `domains`-`resources` a new block to activate bastion based on roles/categories/groups/users you want.

![alt text](bastion.images/bastion-allowed.png)

You can select the roles/categories/groups/users you want to allow access through bastion to their own desktops.