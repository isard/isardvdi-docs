## Priority

En aquesta secció es poden crear prioritats depenent del que es necessiti en aquest moment. Aquí es poden crear regles amb un determinat temps màxim de reserva, nombre d'escriptoris, etc.

![](./domains.images/domains26.png)

Per a afegir una prioritat nova, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

Y saldrá una ventana de diálogo donde se pueden rellenar los campos, también per,ite compartir con otros roles, categorías, grupos o usuarios:

* **Name**: El nom que se li vol donar a la prioritat

* **Description**: Una petita descripció per a poder saber una mica més d'informació sobre aquesta prioritat

* **Rule**: És la regla que li donem a aquesta prioritat, això s'aplica en l'apartat de "Resources". És important tenir en compte que si es vol aplicar més d'una regla a una prioritat, que es diguin iguals, **"default"**

* **Priority**: És la prioritat que se li dona a la regla; com major sigui el número, més prioritat tindrà i com menor sigui el número, menys tindrà

* **Forbid time**: És el temps d'antelació que no es poden fer reserves.

* **Max time**: És el temps màxim d'una reserva.

* **Max items**: Són el màxim d'escriptoris que es permet tenir amb aquesta regla.

![](./domains.images/domains28.png)


### Compartir 

Si es vol compartir una prioritat amb un rol, categoria, grup o usuari es prem la icona ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

I s'emplena el formulari

![](./domains.images/domains31.png)


### Editar

Per a editar una prioritat, es prem la icona ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

Apareix una finestra de diàleg on es poden editar els camps:

![](./domains.images/domains34.png)


## Resources

En aquesta secció es pot trobar tots els perfils de GPU que s'hagin seleccionat en l'apartat de "Hypervisores"

Aquí se li poden aplicar les prioritats/regles que s'hagin creat anteriorment.

![](./domains.images/domains35.png)


### Compartir

Per a compartir un recurs, es prem la icona ![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

I s'emplena el formulari

![](./domains.images/domains37.png)


### Editar

Per a editar un recurs, es prem la icona ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

Apareix una finestra de diàleg on es poden editar els camps:

![](./domains.images/domains39.png)


## Events 

En aquesta secció es poden veure els esdeveniments que es generen, derivats de les accions de les GPU.