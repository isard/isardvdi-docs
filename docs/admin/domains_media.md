# Introduction

!!! info "Roles with access"
    Only **administrators** have access to all categories within the platform. The **manager** is limited to his own category.

This section shows all desktops created by users within one or more categories.

![](./domains.images/media1.png){width="80%"}

1. Media: This section shows all the media uploaded to the platform.

2. Media in other status: Media that have another type of status, such as those that have been deleted, those that have been downloaded incorrectly, or those that have been left in maintenance.


## Media

* **Show last task info** ![](./domains.images/media2.png) : Checks if the "Check media" task is running.

* **Check media status** ![](./domains.images/media3.png) : Checks if the media exists and if the ISO is physically on the disk. If it does not exist, it will be automatically deleted.

* **Create desktop from media** ![](./domains.images/media4.png) : Creates a virtual desktop from this media.

* **Changed allowed users** ![](./domains.images/media5.png) : Changes the sharing permissions of the media.

* **Change owners** ![](./domains.images/media6.png) : Changes ownership of the media.

* **Delete media** ![](./domains.images/media7.png) : Deletes the media.


## Media in other status

* **Deleted**: Media that have already been removed from the platform.
* **Download Aborting**: Media whose download has been aborted, for example, when a download in progress is cancelled.
* **Download Failed**: Media with failed downloads.
* **Download Failed Invalid Format**: Media whose download has failed due to an invalid ISO format.
* **Reset Downloading**: Media on which the download has been restarted.