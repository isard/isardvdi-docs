# Configuració de logs

!!! warning "Rols amb accés"
    Només els **administradors (admins)** tenen accés a aquesta característica.

Aquesta pàgina ofereix opcions de configuració per als logs d'**escriptoris** i **usuaris**.

## Logs antics

![Configuració de logs antics](./logs_config.images/logs_config1.png)

En aquesta secció, es pot configurar l'eliminació automàtica de logs que superin una certa antiguitat.
Aquest procés es realitzarà diàriament a les 00:30 UTC.

!!! tip "Consell"
    Es recomana mantenir els logs d'escriptori durant un període de temps més llarg que el temps màxim de **Desktop Timeout** configurat per evitar l'eliminació de logs d'escriptoris actius.
