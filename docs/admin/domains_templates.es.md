# Introducción

!!! info "Roles con acceso"
    Solo los **administradores** tienen acceso a todas las categorías dentro de la plataforma. El **gestor** está limitado a su propia categoría.

En esta sección se pueden ver todas las plantillas creadas por los usuarios dentro de una o varias categorías.

![](./domains.images/domains7.png){width="90%"}

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información sobre la plantilla.

![](./domains.images/domains8.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info**  |Información detallada sobre el estado actual del escritorio. En caso de un fallo en el escritorio, esta sección desplegará un mensaje de error específico que puede proporcionar detalles para identificar y resolver el problema que haya surgido.| 
|**Hardware**|Componentes físicos (virtuales) y recursos tecnológicos asignados específicamente al escritorio en particular.| 
|**Storage**|Indica el identificador único (UUID) del disco vinculado al escritorio, así como a la información sobre su capacidad y su uso actual.|
|**Allows**|Indica los usuarios o grupos específicos con los que se ha compartido la plantilla, determinando quiénes tienen acceso y pueden utilizarla.|  
|**Media**|Indica la presencia de archivos multimedia asociados específicamente al escritorio en cuestión, en caso de que existan.| 

## Enable

Al habilitar una plantilla, se permite a todos los usuarios con los cuales se comparta verla y utilizarla para crear escritorios.

Para habilitar o deshabilitar una plantilla, simplemente se tiene que pulsar en la casilla de la columna "Enabled" de la tabla y confirmar la selección en la notificación emergente

![Checkboxes de las plantillas habilitadas](./domains.images/domains53.png)

Al pulsar en el botón ![Ocultar desactivados](./domains.images/domains49.png) situado arriba a la derecha, las plantillas que no están habilitadas (visibles para los usuarios con los cuales están compartidas) no se mostrarán en la tabla. Al pulsar en el botón ![Visualizar Desactivados](./domains.images/domains50.png) se volverán a mostrar.

## [Forced hyp](../domains.es/#forced-hyp)

## [Favourite hyp](../domains.es/#favourite-hyp)
## Duplicados

Esta opción permite duplicar la plantilla en la base de datos, mientras se mantiene el disco original.

La duplicación de plantillas se puede realizar varias veces y es útil para diversos casos, como:

- Se quiere compartir una plantilla en otras categorías. Se podría duplicar la plantilla y asignarla a un usuario en otra categoría con permisos de compartición separados para la copia nueva, manteniendo la versión original.

!!! info "Información" 
    Eliminar una plantilla duplicada no eliminará la plantilla original.

Al duplicar una plantilla, se puede personalizar la nueva copia:

![](./domains.images/domains46.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Permite añadir nombre y descripción al escritorio| 
|**New user Owner**|Usuario designado como el nuevo propietario de la plantilla. Este usuario asume la responsabilidad y los privilegios de administración asociados con la plantilla.| 
|**Options - Enable**|Activa o desactiva la visibilidad y disponibilidad de la plantilla para los usuarios. Al marcar esta casilla, se habilita la plantilla para que esté activa y sea visible, de lo contrario, permanecerá oculta y no estará disponible para los usuarios| 
|**Allows**|Permite seleccionar los usuarios o grupos específicos con los que se desea compartir la plantilla, determinando quiénes tendrán acceso y podrán utilizarla.| 

## [Edit](../domains.es/#edit)

## [XML](../domains.es/#xml)

## [Change owner](../domains.es/#change-owner)
## Delete

!!! danger "Acción no reversible"
    Actualmente el borrado de una plantilla y sus escritorios y plantillas derivados se pueden recuperar durante un tiempo predefinido en la [papelera de reciclaje](./recycle_bin.ca.md)

Al eliminar una plantilla, aparecerá un modal que mostrará todos los escritorios creados a partir de ella y cualquier plantilla duplicada. Eliminar la plantilla comportará la eliminación de todos los dominios asociados y sus discos.

![](./domains.images/domains47.png)

!!! Warning "Dependencias"
    La eliminación de la plantilla comportará la eliminación de todos los dominios asociados y sus discos.

Aun así, si la plantilla a eliminar es una duplicada de otra plantilla, no hay que eliminarlas todas. Solo se tendrían que eliminar si también se elimina la plantilla de origen.

![](./domains.images/domains48.png){width="50%"}

## [Server](../domains.es/#server)