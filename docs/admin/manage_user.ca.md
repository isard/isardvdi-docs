# Gestió d'usuaris

Per a gestionar els usuaris s'ha d'anar al [panell d'Administració](./index.ca.md)

Es prem el botó ![](./manage_user.images/manage_user24.png){width="10%"}

![](./manage_user.images/manage_user25.png){width="80%"}

## Creació de Grups

Per a crear grups s'ha d'anar a la subsecció **"Management"** sota la secció **"User"**. S'ha de cercar la taula **"Groups"** i prémer el botó ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png){width="80%"}

I s'emplenen els camps del formulari.

![](./manage_user.images/manage_user3.png){width="60%"}

!!! Info "Notes importants respecte als camps"
    * "**Linked groups**": Tots els recursos compartits amb els seus grups enllaçats s'heretaran automàticament pel grup creat. Per exemple, si un Grup A es crea amb Grup B com a grup enllaçat, tots els recursos compartits amb el Grup B també es compartiran amb el Grup A.

    * "**Ephimeral desktops**": Establirà un temps límit d'ús dels escriptoris temporals. (Per a això també s'ha de configurar, sent Admin, en Config el "Job Scheduler")

## Edició de grups

Es poden editar els paràmetres d'un grup prement la icona ![](./manage_user.images/manage_user13.png) al costat del grup que es vol actualitzar, llavors es prem ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png){width="80%"}

Una finestra de diàleg s'obrirà amb els mateixos paràmetres que la finestra de creació.

![](./manage_user.images/manage_user29.png){width="60%"}

## Creació d'Usuaris

Per crear usuaris s'ha d'accedir a la subsecció **"Management"** sota la secció **"Usuari"**.

### Individualment

Es cerca la taula **"Users"** i es prem el botó ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png){width="80%"}

Sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user6.png){width="60%"}

I s'emplenen els camps.

!!! Info "Notes importants respecte als camps"
    **Secondary groups**: Tots els recursos que han estat compartits amb els seus grups secundaris s'heretaran automàticament per l'usuari creat. A més, l'usuari serà afegit a tots els desplegaments creats a qualsevol dels seus grups secundaris.

### Creació massiva

Es cerca la taula **"Users"** i es prem el botó ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png){width="80%"}

Sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user9.png){width="60%"}

Es pot descarregar un arxiu d'exemple prement el botó ![](./manage_user.images/manage_user10.png){width="20%"}. Sortirà un formulari a emplenar:

!!! Warning "Evitant errors"
    * Els camps "**category**" i "**group**" han d'adir-se **exactament** amb el seu nom
    * Es recomana la codificació **Unicode (UTF-8)**
    * L'arxiu csv ha d'estar separat per **tabulacions** <kbd>Tab ↹</kbd>
    * És molt recomanable fer servir el csv d'exemple

![](./manage_user.images/manage_user11.png)

Una vegada emplenat, es pot pujar en 

![](./manage_user.images/manage_user12.png)

I si s'ha pujat correctament, sortirà una taula de previsualització dels usuaris:

![](./manage_user.images/manage_user28.png){width="60%"}

Per a exportar els usuaris amb la contrasenya generada en un arxiu CSV, prem el botó ![](./manage_user.images/manage_user63.png)

!!! Failure "Errors"
    Si l'arxiu csv no s'ha pujat correctament, es mostrarà un error indicant la raó.

### Generar CSV per a crear usuaris existents

Per a generar un CSV amb els usuaris existents per a crear-los de nou, selecciona els usuaris i prem el botó ![](./manage_user.images/manage_user64.png).

![](./manage_user.images/manage_user65.png){width="80%"}

Aleshores es pot importar l'arxiu CSV per a crear els usuaris de nou. L'arxiu CSV no contindrà les contrasenyes dels usuaris.

Si el que es vol és actualitzar els usuaris, s'ha d'utilitzar el botó [**CSV for update**](#actualització-dusuaris).

## Edició d'usuaris

Per a editar usuaris s'ha d'anar a la subsecció **"Management"** sota la secció **"User"**.

### Individualment

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol actualitzar, llavors es prem ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png){width="80%"}

Una finestra de diàleg s'obrirà amb els mateixos paràmetres que la finestra de creació.

![](./manage_user.images/manage_user35.png){width="60%"}

!!! Info "Notes importants respecte als camps"
    Els camps "**username**", "**group**" i "**category**" no es poden modificar.

### Edició massiva

Es selecciona un o més usuaris i es prem el botó ![](./manage_user.images/manage_user45.png){width="10%"}

![](./manage_user.images/manage_user46.png){width="80%"}

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user47.png){width="45%"}

!!! note "Fields"
    === "Update active/inactive"
        ![](./manage_user.images/manage_user48.png){width="20%"}  
            - **Update active/inactive**: S'actualitza l'estat de l'usuari en la plataforma. Aquest usuari pot estar **actiu**, té accés a la plataforma, o **inactiu**, no té accés a la plataforma.
    === "Update secondary group"
        ![](./manage_user.images/manage_user49.png){width="50%"}        
            - **Update secondary group**: Es pot actualitzar els grups secundaris de diversos usuaris alhora. Per això, s'escriu el nom del grup i se selecciona una d'aquestes opcions:  
                - **Overwrite**: Sobreescriu el grup secundari que tenia assignat.  
                - **Add**: S'afegeix el grup secundari a l'usuari.  
                - **Delete**: Suprimeix el grup secundari que l'usuari tenia assignat.

### Actualització d'usuaris

Per actualitzar usuaris mitjançant un fitxer .csv, primer s'ha de descarregar el fitxer amb la informació dels usuaris. Per fer això, es prem el botó ![](./manage_user.images/manage_user53.png){width="15%"}

![](./manage_user.images/manage_user54.png){width="80%"}

Després, es prem el botó ![](./manage_user.images/manage_user50.png){width="15%"}

![](./manage_user.images/manage_user51.png){width="80%"}

Sortirà una finestra de diàleg amb el següent formulari on es podrà pujar el fitxer o descarregar un d'exemple:

![](./manage_user.images/manage_user52.png){width="80%"}

!!! Warning "Evitant errors"
    * Els camps "**username**", "**category**" i "**group**" han d'adir-se **exactament** amb el seu nom per tal d'actualitzar l'usuari
    * Es recomana la codificació **Unicode (UTF-8)**
    * L'arxiu csv ha d'estar separat per **tabulacions** <kbd>Tab ↹</kbd>

!!! Info "Notes importants respecte als camps"
    * Els camps "**provider**", "**category**", "**uid**", i "**group**" no es poden modificar. La resta de camps s'actualitzaràn.
    * Si es deixa una casella en **blanc**, no s'actualitzarà.
    * La columna "Active", haurà de tenir un valor **"true"** o **"false"**
    * Per afegir més d'un **grup secundari** als usuaris, s'ha de separar els grups de la columna "secondary_groups" amb **"/"**, exemple "prova/Test"

Si el que es vol és generar un CSV per a crear els usuaris de nou, s'ha d'utilitzar el botó [**CSV for create**](#generar-csv-per-a-crear-usuaris-existents).

### Habilitar/Deshabilitar usuari

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol habilitar/deshabilitar, llavors es prem ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png){width="80%"}

L'estat d'un usuari es pot veure a la taula d'usuaris.

![](./manage_user.images/manage_user38.png){width="80%"}

!!! Danger "Aneu amb compte"
    Si l'usuari es deshabilita mentre accedeix al sistema es poden produir errors de sessió.

### Canviar Contrasenya

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari el qual es vol canviar la contrasenya, llavors es prem ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png){width="80%"}

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user40.png){width="45%"}

!!! Tip "Suggeriment"
    L'usuari pot canviar la seva contrasenya mitjançant el seu [perfil](../user/profile.ca.md).

### Migrar usuari

Es pot **migrar de comptes locals cap a comptes de Google (viceversa o qualsevol altre tipus de compte)** tots els elements d'un usuari cap a un altre (sempre que tinguin el mateix rol o inferior) prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol migrar, llavors es prem ![](./manage_user.images/migrate2.png).

![](./manage_user.images/migrate1.png){width="80%"}

Una finestra de diàleg s'obrirà on es mostrarà si l'usuari té algun ítem creat (escriptori, plantilla, desplegament o mitjans)

![](./manage_user.images/migrate3.png){width="70%"}

Juntament amb una sèrie d'advertències a l'hora de fer la migració d'aquests ítems d'un usuari a un altre.

!!! Warning "Advertència"
    - Qualsevol escriptori que estigui engegat, s'aturarà.
    - La paperera de reciclatge de l'usuari es buidarà.
    - S'eliminaran les reserves de l'usuari.
    - Els escriptoris no persistents s'ELIMINARÀN.
    - Se suprimirà el llistat de copropietari en desplegaments.
    - Els escriptoris que pertanyen a altres usuaris en els desplegaments migrats romandran intactes.
    - Els recursos no permesos per al nou usuari s'eliminaran o restringiran.

Després de llegir les advertències, podem cercar l'usuari al qual volem traslladar els ítems pel nom i es prem el botó "Migrate user".

![](./manage_user.images/migrate4.png){width="80%"}


### Suplantar

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol suplantar, llavors es prem ![](./manage_user.images/manage_user44.png).

![](./manage_user.images/manage_user41.png){width="80%"}

!!! Danger "Avís d'exempció de responsabilitat"
    Suplantar un usuari **proporciona l'accés a totes les seves dades i escriptoris i comporta riscos inherents**. Abans de continuar, és important **considerar la sensibilitat de la informació a la qual s'accedirà**.

## Clau d'autoregistre

A la taula de "Grups" cal cercar el grup del qual es vol obtenir el codi. Es prem el botó ![](./manage_user.images/manage_user13.png) per veure els detalls del grup

![](./manage_user.images/manage_user14.png){width="80%"}

El grup es desplega amb unes opcions. Es prem el botó ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png){width="80%"}

Una finestra de diàleg s'obrirà on es poden generar claus d'inscripció fent clic a les diferents caselles de selecció. 

![](./manage_user.images/manage_user32.png){width="45%"}
![](./manage_user.images/manage_user17.png){width="45%"}

Un cop generada la clau d'inscripció, es pot copiar i compartir amb els usuaris.

!!! Danger "Aneu amb compte"
    * En registrar-se, **als usuaris se'ls donarà el rol de la clau d'inscripció compartida**. Per exemple, als professors se'ls donarà el codi "Advanced" i als estudiants el codi "Users".
    * Una quantitat il·limitada d'usuaris poden registrar-se utilitzant la clau d'inscripció proporcionada. Per tant, es recomana desactivar-les un cop fetes servir.

## Creació de categories

!!! warning "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

En l'apartat **"Users"**, es busca la secció **"Categories"** i es prem el botó ![](./users.images/users3.png)

![](./users.images/users4.png){width="80%"}

Apareixerà una finestra de diàleg on s'emplenen els camps del formulari.

![](./users.images/users5.png){width="80%"}

Hi ha tres opcions que es poden aplicar en crear una categoria:

* **Automatic recycle bin delete**: S'aplicarà la funcionalitat de la paperera de reciclatge als usuaris. Es pot seleccionar si es vol que els escriptoris s'esborren després d'una hora o immediatament. Aquest temps es pot modificar [aquí](./admin/recycle_bin.ca.md/#modificacio-del-temps-desborrat-automatic).

![](./users.images/users7.png){width="40%"}

* **Frontend dropdown show**: En la pàgina d'inici de sessió, apareixerà la categoria creada en el menú desplegable

![](./users.ca.images/users3.png){width="40%"}

!!! info
    Si la categoria està oculta, es pot iniciar sessió en aquesta categoria usant l'URL `/login/<category_id>`

* **Ephemeral desktops**: Per a poder posar un temps limitat d'ús a un escriptori temporal. (Per a això també s'ha de configurar en la secció de "Config" el "Job Scheduler")

![](./users.images/users8.png){width="40%"}


## Accés mitjançant OAuth

!!! warning "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

Per a poder accedir mitjançant Google des d'un compte extern, s'ha d'anar a l'apartat **"Users"** i a la secció de **"Categories"**, es prem la icona ![](./users.images/users9.png) de la categoria la qual es vol donar accés.

![](./users.images/users10.png){width="80%"}

Es prem el botó ![](./users.images/users11.png)

![](./users.images/users12.png){width="80%"}

Apareix una finestra de diàleg amb un formulari. S'emplena el camp de "Allowed email domain for foreign account like Google" amb el domini del correu.

![](./users.images/users13.png){width="45%"}