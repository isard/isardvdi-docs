# Introducció

En aquesta secció es poden veure tots els recursos que té la instal·lació, aquests sortiran en crear un escriptori/plantilla.

![](./domains.images/domains10.png)


## Vídeos

En aquesta secció es poden afegir diferents formats de vídeo.

Per a afegir un nou format, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

I s'emplena el formulari

![](./domains.images/domains15.png)



## Interfícies

En aquesta secció es poden afegir xarxes privades als escriptoris.

Per a afegir una xarxa, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

I s'emplena el formulari

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: Enllaça amb un pont cap a una xarxa del servidor. En el servidor es pot tenir interfícies que enllacin per exemple amb vlans cap a un troncal de la teva xarxa, i es pot mapear aquestes interfícies en isard i connectar-les als escriptoris.

    Per a poder mapear la interfície dins del hipervisor, en l'arxiu de isardvdi.conf s'ha de modificar aquesta línia:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - És més eficient utilitzar virtio (que és una interfície paravirtualizada), mentre que les e1000 o rtl són simulacions de targetes, i va més lent encara que és més compatible amb sistemes operatius antics i no es necessita instal·lar drivers en el cas de windows.

    **Si es té un sistema operatiu modern, amb el virtio funciona, sinó amb els altres que són interfícies. Windows antics: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


## Mètodes d'arrencada

En aquesta secció s'indiquen les diferents maneres d'arrencada d'un escriptori.

**Si no es té permisos no es pot seleccionar una iso d'arrencada i no es deixa per defecte que els usuaris es puguin mapear isos.**

Per a donar permisos es prem en la icona ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


## QoS de Xarxa

En aquesta secció s'indiquen les limitacions que es poden posar a les xarxes.

Per a afegir una limitació, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

I s'emplena el formulari

![](./domains.images/domains20.png)


## QoS de disc

En aquesta secció s'indiquen les limitacions que es poden posar als discos.

Per a afegir un límit de disc, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

I s'emplena el formulari

![](./domains.images/domains22.png)


## VPNs remotes

En aquesta secció es poden afegir xarxes remotes als escriptoris.

Per a afegir una xarxa remota, es prem el botó ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

I s'emplena el formulari

![](./domains.images/domains24.png)