# vGPUs

!!! info "Role access"
    Only **administrators** have access to this feature.

IsardVDI supports the use of virtual GPUs (vGPUs) for desktops and deployments. More information about how this technology works can be found ![here](../../deploy/gpus/gpus/).

## Delete vGPU

To delete a vGPU:

1. Navigate to **Administration > Infrastructure > Hypervisors > GPUs**.
2. In the GPUs table, click the delete button in the "Action" column.

![GPUs table and a notification above that displays "Delete GPU. Do you really want to delete this gpu?"](./vgpus.images/vgpus1.png)

As a GPU is a ![bookable resource](../../user/bookings/#bookings) that can be assigned to desktops. Deleting a GPU will unassign it from all users desktops and delete all related bookings.

The system will display a warning detailing the items to be deleted/unassigned and the users affected:

!["Delete GPU" modal warning users about modification of their plans, bookings, desktops and deployments.](./vgpus.images/vgpus2.png)

Disabling an individual GPU profile by unchecking their checkbox will result in a similar warning.

!!! info
    If a profile of the GPU to be deleted is also found in another active GPU, its bookings and links to desktops **will remain intact**. To check the profiles of each GPU, click the ![Detail](./domains.images/domains5.png) button in the row.

!!! info
    If a desktop has an ongoing booking at the time of deletion, the GPU cannot be deleted. You must either stop the desktop and delete the booking, or wait until the booking period ends.

### Notifying users

If the **Notify users that their bookings will be deleted** checkbox is checked, users will be informed via email about the deletion of their GPU assignments and bookings.

The default email notification looks like this:

![Email body warning user of GPU deletion](./vgpus.images/vgpus3.png)

The body of the email can be modified in **Administration > Config > Notifications > Notification templates** listed as "Deleted GPU Profile".

!!! warning
    Users without a valid email address linked to their profile will **not** be notified.
