# Configuración

!!! info "Roles con acceso"

    Solo los **administradores** tienen acceso a esta característica.

Para ir a la sección de "Config", se pulsa el botón ![](./users.es.images/users1.png)

![](./users.es.images/users2.png)

Y se pulsa el botón ![](./config.images/config1.png)

![](./config.images/config2.png)


## Programador de tareas

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Modo mantenimiento

El modo de mantenimiento desactiva las interfaces web para los usuarios con roles de manager, advanced y de user.

El administrador del sistema puede crear un archivo llamado `/opt/isard/config/maintenance` para cambiar al modo de mantenimiento al inicio. El archivo se elimina una vez que el sistema cambia al modo de mantenimiento.

El modo de mantenimiento se puede habilitar y deshabilitar a través de la sección de [configuración](./config.es.md) del panel de administración por parte de un usuario con función de administrador.

Para habilitar y deshabilitar esta función, se marca la casilla

![](./config.images/config6.png)

Una vez marcada la casilla, cuando los usuarios intenten iniciar sesión a su cuenta, les aparecerá un mensaje de mantenimiento

![](./config.images/config7.png)

![](./config.images/config8-es.png)

## Bastión

El bastión de IsardVDI es una funcionalidad que permite a los usuarios acceder a los escritorios virtuales a través del servidor web de IsardVDI, sin necesidad de VPN. Esto facilita hacer accesibles públicamente las máquinas virtuales dentro de IsardVDI, lo que es útil para hacer servidores o recursos públicos.

Para que funcione, debe estar configurado en el archivo `isardvdi.cfg`

```
BASTION_ENABLED = true
```

La configuración del bastión se encuentra en la sección: ![Bastion](./config.images/config22.png)

Aquí se muestra si el bastión está activado en el archivo `isardvdi.cfg` y, de ser así, qué puerto SSH se ha configurado por defecto.

Sin activar en el archivo de configuración:

![Bastion disabled in cfg](./config.images/config24.png)

Activado en el archivo de configuración:

![](./config.images/config23.png)


### Edit bastion alloweds

Permite definir qué usuarios tienen permiso para usar la funcionalidad del bastión. Por defecto, todos los usuarios tienen acceso.

Si un usuario ya no tiene permiso para utilizar el bastión, la opción no aparecerá en el formulario de edición de un escritorio.

!!! warning
    Eliminar el acceso de un usuario borrará todas las configuraciones de conexión a través del bastión para **todos** sus escritorios. Esta acción es irreversible.