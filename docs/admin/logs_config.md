# Logs config 

!!! warning "Roles with access"
    Only **admins** have access to this feature.

This page features configuration options for both **desktop** and **user** logs.

## Old logs

![Old logs config](./logs_config.images/logs_config1.png)

In the this section, you can configure the automatic deletion of logs that are older than a certain time. 
This process will occur daily at 00:30 UTC.

!!! tip "Tip"
    It is recommended to keep desktop logs for a longer period of time than the max **Desktop Timeout** configured to avoid the deletion of logs for active desktops.
