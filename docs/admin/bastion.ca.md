# Bastió

El bastió d'IsardVDI és una funcionalitat que permet accedir als escriptoris virtuals a través del mateix servidor on s'executa al web d'IsardVDI, en comptes de dependre exclusivament d'una connexió VPN (Wireguard).

És especialment útil en entorns on es necessiten servidors públics, ja que permet que aquests escriptoris siguin accessibles sense necessitat d'una VPN.

Quan s'activa el bastió, el servidor d'IsardVDI fa de porta d'enllaç per a les connexions externes:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

Els ports per defecte que són accessibles dels escriptoris són:

- **2222/TCP:** Servei SSH
- **80/TCP:** Web
- **443/TCP:** Web TLS

## Activació

- IsardVDI versió: El bastionat està disponible a partir de la versió [v14.30.0](https://gitlab.com/isard/isardvdi/-/releases/v14.30.0). Cal realitzar el procediment d'actualització d'IsardVDI a l'última versió si estem en una anterior.
- Afegeix al teu `isardvdi.cfg` actual les noves variables de bastionat (o refés el cfg des del nou cfg.example):

```
# ------ Bastion -------------------------------------------------------------
BASTION_ENABLED=true
BASTION_SSH_PORT=2222
```

- refés el `yml` i inicia el sistema actualitzat (`bash build.sh; docker compose up -d`)

Ara trobaràs a la part d'administració a `domains`-`resources` un nou bloc on activar el bastió segons els roles/categories/groups/users que vulguem permetre.

![alt text](bastion.images/bastion-allowed.png)
