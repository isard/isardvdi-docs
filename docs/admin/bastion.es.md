# Bastión

La funcionalidad _Bastión_ de IsardVDI permite acceder a los escritorios virtuales a través del mismo servidor donde se ejecuta la interfaz web de IsardVDI, en lugar de depender exclusivamente de una conexión VPN (Wireguard).

Es especialmente útil en entornos donde se requieren servidores públicos, ya que permite que estos escritorios sean accesibles sin necesidad de una VPN.

Cuando se activa el bastión, el servidor de IsardVDI actúa como puerta de enlace para las conexiones externas:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

Los puertos accesibles para los escritorios son:

- **2222/TCP:** Servicio SSH
- **80/TCP:** Web
- **443/TCP:** Web TLS


## Activación

- IsardVDI version: Bastión está disponible a partir de la versión [v14.30.0](https://gitlab.com/isard/isardvdi/-/releases/v14.30.0). Será necesaria una actualización si estamos en una versión anterior.
- Añada en su `isardvdi.cfg` las nuevas variables de bastión (o rehaga por completo el cfg a partir del nuevo cfg.example):

```
# ------ Bastion -------------------------------------------------------------
BASTION_ENABLED=true
BASTION_SSH_PORT=2222
```

- rehaga su `yml` e inicie el sistema actualizado (`bash build.sh; docker compose up -d`)

Ahora encontrará en `domains`-`resources` un nuevo bloque en dónde activar el bastión basado en los roles/categories/groups/users que se desean activar.

![alt text](bastion.images/bastion-allowed.png)
