# Introduction

!!! info "Roles with access"
    Only **administrators** have access to all categories within the platform. The **manager** is limited to his own category.

In this section you can see all the templates created by users within one or more categories.

![](./domains.images/domains7.png){width="90%"}

If you press the ![](./domains.images/domains5.png) icon, you can see more information about the template.

![](./domains.images/domains8.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info** |Detailed information about the current status of the desktop. In the event of a desktop failure, this section will display a specific error message that may provide details to identify and resolve the problem that has arisen.|
|**Hardware**|Physical (virtual) components and technological resources specifically assigned to the particular desktop.|
|**Storage**|Indicates the unique identifier (UUID) of the disk linked to the desktop, as well as information about its capacity and current usage.|
|**Allows**|Indicates the specific users or groups with whom the template has been shared, determining who has access and can use it.|
|**Media**|Indicates the presence of media files specifically associated with the desktop in question, if they exist.|


## Enable

Enabling a template allows all users with whom the template is shared to view and use it for creating desktops.

To enable or disable a template, simply press on the checkbox in the "Enabled" column of the table and confirm your selection in the notification.

![Enabled checkboxes](./domains.images/domains53.png)

If you press on the ![Hide Disabled](./domains.images/domains49.png) button located in the top right corner, the templates that are not enabled (visible to the users it's shared with) will not be shown in the table. When pressing on the button ![View Disabled](./domains.images/domains50.png) again they will be displayed.

## [Forced hyp](../domains/#forced-hyp)

## [Favourite hyp](../domains/#favourite-hyp)

## Duplicate

This option allows you to duplicate in database the template while keeping the original template disk.

Template duplication can be performed multiple times, and is useful for various scenarios, such as:

- You want to share template within other categories. You can duplicate the template and assign it to another user in another category with separate sharing permissions for the new copy while maintaining the original version

!!! info "Information" 
    Deleting a duplicated template will not affect the original template.

When duplicating a template, you can customize the new copy:

![](./domains.images/domains46.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Allows you to add a name and description to the desktop|
|**New user Owner**|User designated as the new owner of the template. This user assumes the responsibility and administration privileges associated with the template.|
|**Options - Enable**|Enable or disable the visibility and availability of the template for users. Checking this box enables the template to be active and visible, otherwise it will remain hidden and unavailable to users|
|**Allows**|Allows you to select the specific users or groups with whom you want to share the template, determining who will have access and be able to use it.|  

## [Edit](../domains/#edit)

## [XML](../domains/#xml)

## [Change owner](../domains/#change-owner)

## Delete

!!! danger "Non-reversible action"
    Currently deleting a template and its derived desktops and templates can be recovered for a predefined time in the [recycle bin](./recycle_bin.ca.md)

When deleting a template, a modal window will appear displaying all desktops created from it and any templates duplicated. Deleting the template will result in the deletion of all associated domains and their disks.

![](./domains.images/domains47.png)

!!! Warning "Dependencies"
    Deleting the template will result in the deletion of all associated domains and their disks.

![](./domains.images/domains48.png)

However, if the template to delete is duplicated from another template, there is no need to delete them all unless the origin template is deleted as well.

![](./domains.images/domains48.png){width="50%"}

## [Server](../domains/#server)