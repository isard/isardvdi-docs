# Introduction

!!! Important "Important"
    The view displayed may vary depending on the assigned role, whether **manager** or **administrator**, since each one grants **different levels of access and rights**.

To go to the **"Domains"** section, go to the [administration panel](./index.md) and depending on whether you have the **manager** or **administrator** role, the view changes:

<center>

|Manager role|Administrator role|
|:---------:|:----------------------------------:|
|![](domains.images/domains58.png){width="70%"}|![](domains.images/domains59.png){width="70%"}|  

</center>
