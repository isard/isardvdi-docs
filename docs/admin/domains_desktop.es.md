# Introducción

!!! info "Roles con acceso"
    Solo los **administradores** tienen acceso a todas las categorías dentro de la plataforma. El **gestor** está limitado a su propia categoría.

Esta sección muestra todos los escritorios creados por los usuarios dentro de una o varias categorías.

![](./domains.images/domains4.png){width="80%"}

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información sobre el escritorio.

![](./domains.images/domains6.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info**  |Información detallada sobre el estado actual del escritorio. En caso de un fallo en el escritorio, esta sección desplegará un mensaje de error específico que puede proporcionar detalles para identificar y resolver el problema que haya surgido.| 
|**Description**|Descripción detallada o información adicional asociada al escritorio en cuestión.| 
|**Template tree**|Representación gráfica que exhibe la estructura jerárquica de los escritorios y su relación con la plantilla base de la que se derivan, incluyendo sus dependencias. Esta representación visual resulta útil para comprender de manera clara y rápida el origen y las conexiones de un escritorio específico.| 
|**Hardware**|Componentes físicos (virtuales) y recursos tecnológicos asignados específicamente al escritorio en particular.| 
|**Storage**|Indica el identificador único (UUID) del disco vinculado al escritorio, así como a la información sobre su capacidad y su uso actual.| 
|**Media**|Indica la presencia de archivos multimedia asociados específicamente al escritorio en cuestión, en caso de que existan.| 

## Acciones individuales

### Viewer

Se utiliza para compartir un escritorio mediante un enlace directo a éste, sin necesidad de que el usuario esté registrado a la plataforma. (También se puede hacer desde la página inicial, ver [aquí](/docs/advanced/direct_viewer.es.md))

Para compartir el enlace, se pulsa el botón ![](./domains.images/domains62.png) y aparecerá una ventana de diálogo. En esta ventana se selecciona la casilla para que aparezca el enlace y poder copiarlo.

![](./domains.images/domains63.png){width="45%"}

### Template it

Para convertir el escritorio en plantilla, se pulsa el botón ![](./domains.images/domains64.png) y aparecerá una ventana de diálogo para rellenar.

![](./domains.images/domains65.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Permite añadir nombre y descripción al escritorio| 
|**Options**|Activa o desactiva la visibilidad y disponibilidad de la plantilla para los usuarios. Al marcar esta casilla, se habilita la plantilla para que esté activa y sea visible, de lo contrario, permanecerá oculta y no estará disponible para los usuarios| 
|**Allows**|Permite seleccionar los usuarios o grupos específicos con los que se desea compartir la plantilla, determinando quiénes tendrán acceso y podrán utilizarla.| 

### Forced hyp

El término *"forced hypervisor"* describe una función que, al ser activada, permite especificar de manera forzada el hipervisor en el cual se iniciará el escritorio virtual. Esta acción anula cualquier configuración predeterminada y asegura que el escritorio arranque exclusivamente en el hipervisor seleccionado, ignorando otras configuraciones o preferencias.

!!! important "Importante"
    Al activar esta configuración, el escritorio **solo iniciará en el hipervisor seleccionado y no lo hará en ningún otro.**

Para forzar el hipervisor, se pulsa el botón ![](./domains.images/domains66.png) y aparecerá una ventana de diálogo. En esta ventana se selecciona la casilla para que aparezca un desplegable y poder escoger el hipervisor disponible.

![](./domains.images/domains67.png){width="45%"}

### Favourite hyp

El término *"favourite hypervisor"* describe una función que permite designar un hipervisor preferido para el inicio de un escritorio. Al seleccionar esta opción, se indica el hipervisor específico que se prefiere utilizar para el arranque del escritorio virtual. Esta configuración asegura que, cuando esté disponible, el escritorio arranque automáticamente en el hipervisor marcado como favorito, optimizando la experiencia del usuario al iniciar el entorno virtual deseado.

!!! important "Importante"
    Cuando se activa esta configuración, el escritorio **iniciará en el hipervisor seleccionado. En caso de que este no esté disponible, el escritorio arrancará en otro hipervisor disponible.**

Para seleccionar un hipervisor como favorito, se pulsa el botón ![](./domains.images/domains68.png) y aparecerá una ventana de diálogo. En esta ventana se selecciona la casilla para que aparezca un desplegable y poder escoger el hipervisor disponible.

![](./domains.images/domains69.png){width="45%"}

### Edit

Para editar el escritorio, se pulsa el botón ![Edit](./domains.images/domains51.png) y aparecerá una ventana. En esta ventana se pueden editar los parámetros del escritorio.

![](./domains.images/domains60.png){width="60%"}

|||
|:---------|----------------------------------|
|**Domain name and description**|Permite la modificación del nombre y la descripción asociados al escritorio| 
|**Domain viewers**|Especifica los visores que se desean mostrar al iniciar el escritorio. Además, se puede configurar el inicio automático del escritorio en modo de pantalla completa| 
|**Hardware**|Componentes físicos (virtuales) y recursos tecnológicos asignados específicamente al escritorio en particular.| 
|**Reservable resources**|Asignación de recursos específicos, como una tarjeta gráfica, a un escritorio si están disponibles. Esta asignación implica una reserva de recursos para el funcionamiento del escritorio en cuestión.| 
|**Media**|indica la presencia de archivos multimedia asociados específicamente al escritorio en cuestión, en caso de que existan.| 

#### Virtualización anidada

La *virtualización anidada* o *nested virtualization* es una tecnología que permite ejecutar un hipervisor (software que gestiona máquinas virtuales) dentro de una máquina virtual. En términos simples, te permite crear una capa adicional de virtualización dentro de una máquina virtual existente.

Esto es útil en entornos de desarrollo, pruebas y educativos, donde **se pueden crear y ejecutar máquinas virtuales dentro de otras máquinas virtuales.** Por ejemplo, puedes crear entornos de pruebas aislados dentro de una máquina virtual principal o ejecutar hipervisores adicionales para simular configuraciones complejas sin necesidad de hardware físico adicional.

![](./domains.images/domains61.png){width="60%"}

### Change owner

La función *'change owner'* permite modificar el propietario asociado a un escritorio. Al seleccionar esta opción, se habilita la capacidad de transferir la propiedad del escritorio a otro usuario, brindando la flexibilidad de asignar nuevos responsables o administradores para dicho entorno virtual.

Para cambiar de propietario, se pulsa el botón ![](./domains.images/domains73.png) y aparecerá un formulario. Se escribe el nombre del usuario al que se le quiere cambiar de propiedad el escritorio.

![](./domains.images/domains74.png){width="45%"}
### Server

El término *'server'* hace referencia a una opción que, al ser seleccionada, permite que el escritorio virtual permanezca encendido de manera continua, funcionando como un servidor. Esto asegura que el escritorio esté siempre activo y disponible para su acceso, similar al comportamiento de un servidor, incluso cuando no está siendo utilizado activamente por un usuario.

Para hacer el escritorio servidor, se pulsa el botón ![](./domains.images/domains71.png) y aparecerá una ventana de diálogo. En esta ventana se selecciona la casilla.

![](./domains.images/domains72.png){width="45%"}

### XML

!!! danger "Importante"
    Actualmente solo el rol **administrador** puede editar el XML de los escritorios.

Si se pulsa el botón ![XML](./domains.images/domains52.png) aparecerá una ventana donde se puede modificar el fichero de configuración de la máquina virtual [KVM/QEMU XML](https://libvirt.org/formatdomain.html).

### Delete

Para eliminar el escritorio, se pulsa el botón ![](./domains.images/domains70.png). Aparecerá una ventana de confirmación que preguntará si se desea proceder con la eliminación. 

## Operaciones de disco

Cuando se está realizando una operación en el disco de almacenamiento de un escritorio, este mostrará el estado "Maintenance". Los escritorios en este estado no pueden iniciarse hasta que finalice el proceso de operación de disco.

![(Escritorio con estado "Maintenance" y un botón con la acción "Cancel Task")](./domains.images/domains75.png){width="65%"}

En algunos casos, el proceso de mantenimiento puede tardar más de lo esperado, posiblemente debido a un proceso atascado. Si esto ocurre, los usuarios pueden hacer clic en el botón "Cancel operation". Esta acción cancela la tarea en curso y debería devolver el escritorio a un estado en el que se pueda iniciar, cambiando su estado de "Failed" a "StartedPaused" a, si no ha habido ningún problema, "Stopped".

![(Tooltip que dice "Operations are being performed over the desktop disk: virt_win_reg")](./domains.images/domains76.png){width="65%"}

!!! Tip "Consejo"
    Pasar el cursor sobre el icono de información mostrará qué operación se está realizando en el disco del escritorio.

!!! Warning "Advertencia"
    Cancelar la operación del disco conlleva un pequeño riesgo de que el disco de almacenamiento se corrompa, y, por lo tanto, que el escritorio sea inutilizable.


## Global actions

En la parte superior de la tabla hay un desplegable con varias acciones. Estas acciones se aplicarán a todos los escritorios seleccionados.

![Desplegable de acciones globales](./domains.images/domains54.png)

Para seleccionar un escritorio, simplemente se tiene que pulsar en la fila de la tabla. Los escritorios seleccionados aparecerán como marcados en la casilla de "selected" al final de la fila. Aunque se navegue a la página siguiente, los escritorios seleccionados permanecerán marcados.

![Checkboxes de selección](./domains.images/domains55.png)

Alternativamente, si no se selecciona ningún escritorio, se pueden utilizar los filtros de arriba o debajo de la tabla para filtrarlos.

!!! Warning "Advertencia"
    Si no se selecciona ningún escritorio o se aplica ningún filtro, **todos los escritorios se seleccionarán por defecto.**

Una vez se hayan seleccionado los escritorios, se elige una acción del desplegable para realizarla.

|Acciones|Descripción|
|:---------|----------------------------------|
|**Soft toggle started/shutting-down state**|Revierte el estado actual del escritorio: si está apagado, enviará una señal para encenderlo; si está encendido, enviará una señal para apagarlo. Es importante destacar que **esta función no fuerza el estado del escritorio**, sino que simplemente ejecuta la acción opuesta al estado actual sin forzar cambios bruscos.| 
|**Toggle started/stopped state**|Altera el estado actual del escritorio, emitiendo una señal para encenderlo si está apagado o apagarlo si está encendido. Es importante resaltar que **esta función fuerza el cambio de estado del escritorio**, ejecutando la acción opuesta al estado actual sin considerar otras circunstancias.| 
|**Soft shut down**|Envía una señal de apagado al escritorio de manera suave y controlada, sin forzar cambios abruptos en su estado actual.| 
|**Shut down**|Envía una señal de apagado al escritorio, forzando cambios abruptos en su estado actual| 
|**Force Failed state**|En caso de que un escritorio quede en un estado intermedio debido a un error durante su creación o modificación, lo fuerza a cambiar a un estado de 'Failed'. Esto ayuda a identificar y registrar explícitamente la situación problemática del escritorio para su posterior diagnóstico y resolución.| 
|**Delete**|Borra los escritorios que han sido seleccionados. En caso de no haber ninguno seleccionado, procede a eliminar todos los escritorios existentes.| 
|**Start Paused (check status)**|Inicia el escritorio virtual pero lo detiene inmediatamente después, permitiendo verificar su estado actual.| 
|**Remove forced hypervisor**|Elimina cualquier selección previa que haya sido configurada para arrancar el escritorio con un hipervisor específico. En resumen, revierte la configuración de arranque forzado con un hipervisor determinado, si se había establecido previamente.| 
|**Remove favourite hypervisor**|Elimina cualquier selección previa que se haya configurado como el hipervisor favorito para el arranque. Deshace la preferencia previamente establecida para el inicio con un hipervisor específico, si se había designado como favorito.| 

## Bulk Edit Desktops

Se pueden seleccionar varios escritorios haciendo pulsando en las filas de la tabla. De manera alternativa, se pueden filtrar los escritorios sin pulsar en las filas y todos los escritorios que se muestran en la tabla contarán como seleccionados, igual que se hace con Global Actions.

Al pulsar en el botón ![Bulk Edit Desktops](./domains.images/domains40.png) aparecerá una ventana:

![(Ventana de actualizar múltiples escritorios a la vez)](./domains.images/domains41.png){width="50%"}

|Acciones|Descripción|
|:---------|----------------------------------|
|**Desktops to edit**|Indica todos los escritorios que se actualizarán con los datos del formulario. En caso de haber muchos escritorios, se puede ir desplazando por la lista para seleccionarlos.| 
|**Desktops viewers**|Si está marcada, aparecerá una sección nueva. Los visores de los escritorios se actualizarán según esté seleccionado.  ![Update viewers section](./domains.images/domains42.png){width="60%"}| 
|**Update RDP credentials**|Si está marcada, se cambiará el nombre de usuario y la contraseña utilizados para las sesiones RDP.  ![(Update RDP credentials)](./domains.images/domains43.png){width="60%"}| 
|**Hardware**|Los datos solo se cambiarán en los campos donde el valor seleccionado es diferente a "--"| 
|**Update network**|Si está marcada, aparecerá una sección nueva. Las interfaces de los escritorios seleccionadas se actualizarán, usando la tecla CTRL mientras se pulsa las opciones para seleccionar más de una interfaz a la vez.  ![(Update network)](./domains.images/domains44.png){width="60%"}| 
|**Bookable resources**|Los reservables se actualizarán cuando el valor seleccionado sea diferente a "--"| 

Para aplicar todos los cambios y actualizar los escritorios seleccionados, se pulsa el botón ![(Modify desktops)](./domains.images/domains45.png){width="10%"}

## Bulk Add Desktops

Al pulsar el botón ![Bulk Add Desktops](./domains.images/domains56.png) aparecerá una ventana

![(Modal de Bulk Add)](./domains.images/domains57.png){width="45%"}

Este formulario permite crear varios escritorios simultáneamente. Para crearlos, simplemente se tiene que rellenar el formulario especificando un nombre para los escritorios nuevos y seleccionando la plantilla sobre la que se basarán.

A la sección **"Allowed"**, se pueden seleccionar los usuarios para los cuales se quieren crear los escritorios eligiendo entre roles, categorías, grupos o nombres de usuario. Se creará un escritorio separado para cada usuario seleccionado.