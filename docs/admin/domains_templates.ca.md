# Introducció

!!! info "Rols amb accés"
    Només els **administradors** tenen accés a totes les categories dins de la plataforma. El **gestor** està limitat a la seva categoria.

En aquesta secció es poden veure totes les plantilles creades pels usuaris dins d'una o més categories.

![](./domains.images/domains7.png){width="90%"}

Si es prem la icona ![](./domains.images/domains5.png), es pot veure més informació sobre la plantilla.

![](./domains.images/domains8.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info** |Informació detallada sobre l‟estat actual de l‟escriptori. En cas d'error en l'escriptori, aquesta secció desplegarà un missatge d'error específic que pot proporcionar detalls per identificar i resoldre el problema que hagi sorgit.|
|**Hardware**|Components físics (virtuals) i recursos tecnològics assignats específicament a l'escriptori en particular.|
|**Storage**|Indica l'identificador únic (UUID) del disc vinculat a l'escriptori, així com a la informació sobre la capacitat i l'ús actual.|
|**Allows**|Indica els usuaris o grups específics amb què s'ha compartit la plantilla, determinant qui hi té accés i la poden utilitzar.|
|**Media**|Indica la presència d'arxius multimèdia associats específicament a l'escriptori en qüestió, en cas que n'hi hagi.|

## Enable

En habilitar una plantilla (enable), es permet a tots els usuaris amb els quals es comparteixi veure-la i utilitzar-la per crear escriptoris.

Per habilitar o deshabilitar una plantilla, simplement s'ha de prémer a la casella de selecció de la columna "Enabled" de la taula i confirmar la selecció a la notificació emergent

![Checkboxes de les plantilles  habilitades](./domains.images/domains53.png)

En prémer el botó ![Oculta desactivat](./domains.images/domains49.png) situat a dalt a la dreta, les plantilles que no estan habilitades (visibles pels usuaris amb els quals estan compartides) no es mostraran a la taula. En prémer el botó ![Visualitza Desactivat](./domains.images/domains50.png) es tornaran a mostrar.

## [Forced hyp](../domains.ca/#forced-hyp)

## [Favourite hyp](../domains.ca/#favourite-hyp)

## Duplicat

Aquesta opció permet duplicar la plantilla a la base de dades, mentre que es manté el disc original.

La duplicació de plantilles es pot realitzar diverses vegades i és útil a alguns casos, com ara:

- Es vol compartir la plantilla en altres categories. Es podria duplicar la plantilla i assignar-la a un altre usuari en una categoria diferent amb permisos de compartició separats per la còpia nova i mantenir la versió original

!!! info "Informació" 
    L'eliminació d'una plantilla duplicada no eliminarà la plantilla original.

En duplicar una plantilla, es pot personalitzar la còpia nova:

![](./domains.images/domains46.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Permet afegir nom i descripció a l'escriptori| 
|**New user Owner**|Usuari designat com el nou propietari de la plantilla. Aquest usuari assumeix la responsabilitat i els privilegis dadministració associats amb la plantilla.| 
|**Options - Enable**|Activa o desactiva la visibilitat i disponibilitat de la plantilla per als usuaris. En marcar aquesta casella, s'habilita la plantilla perquè estigui activa i sigui visible; en cas contrari, romandrà oculta i no estarà disponible per als usuaris| 
|**Allows**|Permet seleccionar els usuaris o grups específics amb què es vol compartir la plantilla, determinant qui tindran accés i podran utilitzar-la.| 

## [Edit](../domains.ca/#edit)

## [XML](../domains.ca/#xml)

## [Change owner](../domains.ca/#change-owner)

## Delete

!!! danger "Acció no reversible"
    Actualment l'esborrat d'una plantilla i els seus escriptoris i plantilles derivats es poden recuperar durant un temps predefinit a la [paperera de reciclatge](./recycle_bin.ca.md)

En eliminar una plantilla, apareixerà un modal que mostrarà tots els escriptoris creats a partir d'ella i qualsevol plantilla duplicada. Eliminar la plantilla comportarà l'eliminació de tots els dominis associats i els seus discs.

![](./domains.images/domains47.png)

!!! Warning "Dependències"
    L'eliminació de la plantilla comportarà l'eliminació de tots els dominis associats i els vostres discos.

Tot i això, si la plantilla a eliminar és una duplicada d'una altra plantilla, no cal eliminar-les totes. Només s'haurien d'eliminar si també s'elimina la plantilla d'origen.

![](./domains.images/domains48.png){width="50%"}


## [Server](../domains.ca/#server)