# Introduction

The following section provides a detailed view of the disk information. Here, users can find crucial data such as the used and total size of the disks, as well as their current status, including whether they have been deleted or experienced failures for any reason.

With the following buttons:

- **Check disk info** ![](./storage.images/check.png){width="3%"}: This button checks that the storage status is correct.
- **Show last task info** ![](./storage.images/show.png){width="3%"}: A modal opens where it gives information about the last task performed by that storage.

![](./storage.images/task_info.png){width="60%"}

## Storage files

![](./storage.images/storage_files.png){width="80%"}

## Other status storage files

![](./storage.images/other_status.png){width="80%"}

## Deleted storage files log

![](./storage.images/deleted_storage.png){width="80%"}