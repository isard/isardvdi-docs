# Gestionar

Els desplegaments creats pels usuaris es poden gestionar aquí. La informació sobre els escriptoris i la visibilitat es mostren a la taula i en els detalls dels paràmetres de l'escriptori i es poden veure els usuaris i grups desplegats.

![](deployments.images/deployment1.png){width="80%"}

## Esborrar

!!! danger "Acció no reversible"

    Actualment l'esborrat d'un desplegament i els seus escriptoris no es pot recuperar.
    Amb l'entrada de la MR de [paperera de reciclatge](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    es podran recuperar durant un temps predefinit.

La icona de supressió ![](./deployments.images/delete.png){width="2%"} a la dreta de cada fila permet **suprimir** el desplegament, per tant els escriptoris que hi pertanyen.