# vGPUs

!!! info "Rols amb accés"
    Només els **administradors** tenen accés a totes aquestes característiques.

IsardVDI admet l'ús de GPUs virtuals (vGPUs) per a escriptoris i desplegaments. Més informació sobre com funciona aquesta tecnologia es pot trobar [aquí](../../deploy/gpus/gpus.ca/).

## Eliminar vGPU

Per eliminar una vGPU:

1. Navegar a **Administration > Infrastructure > Hypervisors > GPUs**.
2. A la taula de GPUs, fer clic al botó d'eliminar a la columna "Action".

![Taula de GPUs i una notificació que mostra "Delete GPU. Do you really want to delete this gpu?"](./vgpus.images/vgpus1.png)

Una GPU és un [recurs reservable](../../user/bookings.ca/#bookings) que es pot assignar a escriptoris. Eliminar una GPU implicarà desassignar-la de tots els escriptoris d'usuaris i eliminar totes les reserves relacionades.

El sistema mostrarà una advertència detallant els elements que s'eliminaran/desassignaran i els usuaris afectats:

![Modal d'eliminar GPU informant als usuaris sobre canvis als seus plans, reserves, escriptoris i desplegaments](./vgpus.images/vgpus2.png)

Deshabilitar un perfil de GPU individual desmarcant la seva casella resultarà en una advertència similar.

!!! info
    Si el perfil de la GPU a eliminar es troba també en una altra GPU activa, les seves reserves i assignacions a escriptoris **romandran intactes**. Per comprovar quins són els perfils de cada GPU, es pot fer clic al botó ![Detail](./domains.images/domains5.png) a la fila corresponent.

!!! info
    Si un escriptori té una reserva en curs en el moment de l'eliminació, no es podrà eliminar la GPU. Cal parar l'escriptori i eliminar la reserva o esperar que finalitzi el temps de reserva.

### Notificar als usuaris

Si la casella **Notify users that their bookings will be deleted** està marcada, els usuaris seran informats per correu electrònic sobre l'eliminació de les seves assignacions a escriptoris i reserves de GPU.

La notificació per correu electrònic predeterminada es veu així:

![Contingut del correu electrònic advertint l'usuari de l'eliminació de la GPU](./vgpus.ca.images/vgpus1.png)

El contingut del correu electrònic es pot modificar a **Administration > Config > Notifications > Notification templates**, llistat com "Deleted GPU Profile".

!!! warning "Advertència"
    Els usuaris que no tinguin una adreça de correu electrònic vàlida vinculada al seu perfil **no** seran notificats.
