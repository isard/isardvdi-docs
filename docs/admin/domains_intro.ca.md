# Introducció

!!! Important "Important"
    La vista que es desplega pot variar segons el rol assignat, ja sigui **gestor** o **administrador**, atès que cadascú atorga **diferents nivells d'accés i drets**.

Per anar a la secció de **"Domains"**, se'n va al [panell d'administració](./index.ca.md) i depenent de si es té el rol **gestor** o **administrador** la vista canvia:

<center>

|Rol gestor|Rol administrador|
|:---------:|:----------------------------------:|
|![](domains.images/domains58.png){width="70%"}|![](domains.images/domains59.png){width="70%"}|  

</center>