# Bookables

## Priority

En esta sección se pueden crear prioridades dependiendo de lo que se necesite en ese momento. Aquí se pueden crear reglas con un determinado tiempo máximo de reserva, número de escritorios, etc.

![](./domains.images/domains26.png)

Para añadir una prioridad nueva, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

I apareixerà un quadre de diàleg on es pot emplenar els camps, a més permet compartir amb altres rols, categories, grups o usuaris:

* **Name**: El nom que se li vol donar a la prioritat

* **Description**: Una pequeña descripción para poder saber un poco más de información sobre esa prioridad

* **Rule**: Es la regla que le damos a esa prioridad, esto se aplica en el apartado de "Resources". Es importante tener en cuenta que si se quiere aplicar más de una regla a una prioridad, que se llamen iguales, **"default"**.

* **Priority**: Es la prioridad que se le da a la regla; cuánto mayor sea el número, más prioridad tendrá y cuánto menor sea el número, menos tendrá.

* **Forbid time**: Es el tiempo de antelación que no se pueden hacer reservas.

* **Max time**: Es el tiempo máximo de una reserva.

* **Max items**: Son el máximo de escritorios que se permite tener con dicha regla.

![](./domains.images/domains28.png)


### Compartir 

Si se quiere compartir una prioridad con un rol, categoría, grupo o usuario se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

Y se rellena el formulario

![](./domains.images/domains31.png)


### Editar

Para editar una prioridad, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains34.png)


## Resources

En esta sección se puede encontrar todos los perfiles de GPUs que se hayan seleccionado en el apartado de "Hypervisores"

Aquí se le pueden aplicar las prioridades/reglas que se hayan creado anteriormente.

![](./domains.images/domains35.png)


### Compartir

Para compartir un recurso, se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

Y se rellena el formulario

![](./domains.images/domains37.png)


### Editar

Para editar un recurso, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains39.png)


## Events 

En esta sección se pueden ver los eventos que se generan derivados de las acciones de las gpus.