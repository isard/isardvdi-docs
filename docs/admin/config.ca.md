# Configuració

!!! info "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

Per a anar a la secció de "Config", es prem el botó ![](./users.ca.images/users1.png)

![](./users.ca.images/users2.png)

I es prem el botó ![](./config.images/config1.png)

![](./config.images/config2.png)


## Programador de tasques

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Mode manteniment

El mode de manteniment desactiva les interfícies web per als usuaris amb rols de managers, avançats i d'usuari.

L'administrador del sistema pot crear un arxiu anomenat `/opt/isard/config/maintenance` per a canviar a el mode de manteniment a l'inici. L'arxiu s'elimina una vegada que el sistema canvia a el mode de manteniment.

El mode de manteniment es pot habilitar i deshabilitar a través de la secció de [configuració](./config.ca.md) del panell d'administració per part d'un usuari amb funció d'administrador.

Per a habilitar i deshabilitar aquesta funció, es marca la casella

![](./config.images/config6.png)

Una vegada marcada la casella, quan els usuaris intentin iniciar sessió al seu compte, els apareixerà un missatge de manteniment

![](./config.images/config7.png)

![](./config.images/config8-ca.png)

## Bastió

El bastió a IsardVDI és una funcionalitat que permet als usuaris accedir als escriptoris virtuals a través del servidor web d'IsardVDI, sense necessitat de VPN. Això facilita fer accessibles públicament les màquines virtuals dins d'IsardVDI, útil per fer servidors o recursos públics.

Per activar aquesta funcionalitat, cal configurar el fitxer `isardvdi.cfg`

```
BASTION_ENABLED = true
```

La configuració del bastió es troba a la secció: ![Bastion](./config.images/config22.png)

Es mostra si el bastió està activat en el fitxer `isardvdi.cfg` i, en cas afirmatiu, quin port d'SSH s'ha configurat per defecte.

Sense activar al fitxer de configuració:

![Bastion disabled in cfg](./config.images/config24.png)

Activat al fitxer de configuració:

![](./config.images/config23.png)

### Edit bastion alloweds

Permet definir quins usuaris tenen permís per utilitzar la funcionalitat del bastió. Per defecte, tots els usuaris hi tenen accés.

Si un usuari ja no té permís de fer servir el bastió, no li apareixerà l'opció al formulari d'editar un escriptori.

!!! warning
    Eliminar l'accés d'un usuari esborrarà totes les configuracions de connexió a través del bastió per a **tots** els seus escriptoris. Aquesta acció és irreversible.
