# Config

!!! info "Role access"

    Only **administrators** have access to this feature.

To go to the "Config" section, press the button ![](./users.images/users1.png)

![](./users.images/users2.png)

And press the button ![](./config.images/config1.png)

![](./config.images/config2.png)


## Job Scheduler

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Maintenance mode

Maintenance mode disables web interfaces for users with manager, advanced, and user roles.

System administrator can create a file named `/opt/isard/config/maintenance` to switch to maintenance mode at start up. The file is removed once system is switched to maintenance mode.

Maintenance mode can be enabled and disabled via [configuration](./config.md) section of administration interface by an user with administrator role.

To enable and disable this function, check the box

![](./config.images/config6.png)

Once the box is checked, when users try to log in to their account, they will see a maintenance message

![](./config.images/config7.png)

![](./config.images/config8-es.png)

## Bastion

The Bastion feature in IsardVDI allows users to access virtual desktops through the IsardVDI web server, without the need for a VPN. This makes it easier to publicly access virtual machines within IsardVDI, which is useful to make public servers or resources.

For it to work, it must be configured in the `isardvdi.cfg` file.

```
BASTION_ENABLED = true
```

The bastion configuration is located in the following section: ![Bastion](./config.images/config22.png)

Here, it displays whether the bastion is enabled in the `isardvdi.cfg` file and, if so, which SSH port has been configured by default.

Disabled in the configuration file:

![Bastion disabled in cfg](./config.images/config24.png)

Enabled in the configuration file:

![](./config.images/config23.png)


### Edit bastion alloweds

Allows you to define which users have permission to use the bastion feature. By default, all users have access.

If a user no longer has permission to use the bastion, the option will not appear in the desktop edit form.

!!! warning
    Removing a user's access will delete all bastion connection configurations for *all* their desktops. This action is irreversible.
