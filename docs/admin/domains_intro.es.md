# Introducción

!!! Important "Importante"
    La vista que se despliega puede variar según el rol asignado, ya sea **gestor** o **administrador**, dado que cada uno otorga **diferentes niveles de acceso y derechos**.  

Para ir a la sección de **"Domains"**, se va al [panel de administración](./index.es.md) y dependiendo de si se tiene el rol **gestor** o **administrador** la vista cambia:

<center>

|Rol gestor|Rol administrador|
|:---------:|:----------------------------------:|
|![](domains.images/domains58.png){width="70%"}|![](domains.images/domains59.png){width="70%"}|  

</center>