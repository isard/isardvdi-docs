# Introduction

In this section you can see all the resources that the installation has, these will appear when creating a desktop/template.

![](./domains.images/domains10.png)


## Videos 

In this section you can add different video formats.

To add a new format, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

And the fill out the form

![](./domains.images/domains15.png)


## Interfaces

In this section you can add private networks to desktops.

To add a network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

And the fill out the form

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: It links with a bridge to a server network. On the server you can have interfaces that link, for example, with vlans to a backbone of your network, and you can map these interfaces in isard and connect them to the desktops.

    In order to map the interface within the hypervisor, this line must be modified in the isardvdi.conf file:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - It is more efficient to use virtio (which is a paravirtualized interface), while the e1000 or rtl are card simulations, and it is slower, although it is more compatible with older operating systems and it is not necessary to install drivers in the case of windows.

    **If you have a modern operating system, it works with virtio, but with the others that are interfaces. Old windows: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


## Boots

This section lists the different boot modes for a desktop.

**If you do not have permissions, you cannot select a bootable iso and it is not allowed by default that users can map isos.**

To give permissions, press on the icon ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


## Network QoS

This section lists the limitations that can be placed on networks.

To add a limitation, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

And the fill out the form

![](./domains.images/domains20.png)


## Disk QoS

This section lists the limitations that can be placed on disks.

To add a disk limit, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

And the fill out the form

![](./domains.images/domains22.png)


## Remote VPNs

In this section you can add remote networks to desktops.

To add a remote network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

And the fill out the form

![](./domains.images/domains24.png)