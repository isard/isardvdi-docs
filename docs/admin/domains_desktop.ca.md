# Introducció

!!! info "Rols amb accés"
    Només els **administradors** tenen accés a totes les categories dins de la plataforma. El **gestor** està limitat a la seva categoria.

Aquesta secció mostra tots els escriptoris creats pels usuaris dins d'una o més categories.

![](./domains.images/domains4.png){width="80%"}

Si es prem la icona ![](./domains.images/domains5.png), es podrà veure més informació sobre l'escriptori.

![](./domains.images/domains6.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info**|Informació detallada sobre l'estat actual de l'escriptori. En cas d'error en l'escriptori, aquesta secció desplegarà un missatge d'error específic que pot proporcionar detalls per identificar i resoldre el problema que hagi sorgit.|
|**Description**|Descripció detallada o informació addicional associada a l'escriptori en qüestió.|
|**Template tree**|Representació gràfica que exhibeix l'estructura jeràrquica dels escriptoris i la seva relació amb la plantilla base de què es deriven, incloent-hi les dependències. Aquesta representació visual és útil per comprendre de manera clara i ràpida l'origen i les connexions d'un escriptori específic.|
|**Hardware**|Components físics (virtuals) i recursos tecnològics assignats específicament a l'escriptori en particular.|
|**Storage**|Indica l'identificador únic (UUID) del disc vinculat a l'escriptori, així com a la informació sobre la capacitat i l'ús actual.|
|**Media**|Indica la presència d'arxius multimèdia associats específicament a l'escriptori en qüestió, en cas que n'hi hagi.|

## Accions individuals

### Viewer

S'utilitza per compartir un escriptori mitjançant un enllaç directe a aquest, sense que l'usuari estigui registrat a la plataforma. (També es pot fer des de la pàgina inicial, veure [aquí](/docs/advanced/direct_viewer.ca.md))

Per compartir l'enllaç, es prem el botó ![](./domains.images/domains62.png) i apareixerà una finestra de diàleg. En aquesta finestra se selecciona la casella perquè aparegui l'enllaç i poder-lo copiar.

![](./domains.images/domains63.png){width="45%"}

### Template it

Per convertir l'escriptori en plantilla, es prem el botó ![](./domains.images/domains64.png) i apareixerà una finestra de diàleg per omplir.

![](./domains.images/domains65.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Permet afegir nom i descripció a l'escriptori.|
|**Options**|Activa o desactiva la visibilitat i disponibilitat de la plantilla per als usuaris. En marcar aquesta casella, s'habilita la plantilla perquè estigui activa i sigui visible; altrament, romandrà oculta i no estarà disponible per als usuaris.|
|**Allows**|Permet seleccionar els usuaris o grups específics amb què es vol compartir la plantilla, determinant qui tindran accés i podran utilitzar-la.|

### Forced hyp

El terme "forced hypervisor" descriu una funció que, en ser activada, permet especificar de manera forçada l'hipervisor en què s'iniciarà l'escriptori virtual. Aquesta acció anul·la qualsevol configuració predeterminada i assegura que l'escriptori arrenqui exclusivament a l'hipervisor seleccionat, ignorant altres configuracions o preferències.

!!! important "Important"
    En activar aquesta configuració, l'escriptori **només iniciarà a l'hipervisor seleccionat i no ho farà en cap altre.**

Per forçar l'hipervisor, es prem el botó ![](./domains.images/domains66.png) i apareixerà una finestra de diàleg. En aquesta finestra se selecciona la casella perquè aparegui un desplegable i escollir l'hipervisor disponible.

![](./domains.images/domains67.png){width="45%"}

### Favourite hyp

El terme *"favourite hypervisor"* descriu una funció que permet designar un hipervisor preferit per iniciar un escriptori. Quan seleccioneu aquesta opció, s'indica l'hipervisor específic que preferiu utilitzar per a l'arrencada de l'escriptori virtual. Aquesta configuració assegura que, quan estigui disponible, l'escriptori arrenqui automàticament a l'hipervisor marcat com a favorit, optimitzant l'experiència de l'usuari en iniciar l'entorn virtual desitjat.

!!! important "Important"
    Quan s'activi aquesta configuració, l'escriptori **iniciarà a l'hipervisor seleccionat. En cas que no estigui disponible, l'escriptori arrencarà en un altre hipervisor disponible.**

Per seleccionar un hipervisor com a preferit, es prem el botó ![](./domains.images/domains68.png) i apareixerà una finestra de diàleg. En aquesta finestra se selecciona la casella perquè aparegui un desplegable i escollir l'hipervisor disponible.

![](./domains.images/domains69.png){width="45%"}

### Edit

Per editar l'escriptori, es prem el botó ![Edit](./domains.images/domains51.png) i apareixerà una finestra. En aquesta finestra es poden editar els paràmetres de l'escriptori.

![](./domains.images/domains60.png){width="60%"}

|||
|:---------|----------------------------------|
|**Domain name and description**|Permet modificar el nom i la descripció associats a l'escriptori|
|**Domain viewers**|Especifica els visors que es volen mostrar en iniciar l'escriptori. A més, podeu configurar l'inici automàtic de l'escriptori en mode de pantalla completa|
|**Hardware**|Components físics (virtuals) i recursos tecnològics assignats específicament a l'escriptori en particular.|
|**Reservable resources**|Assignació de recursos específics, com ara una targeta gràfica, a un escriptori si estan disponibles. Aquesta assignació implica una reserva de recursos per al funcionament de lescriptori en qüestió.|
|**Media**|indica la presència d'arxius multimèdia associats específicament a l'escriptori en qüestió, en cas que n'hi hagi.|

#### Virtualització anidada

La *virtualització anidada* o *nested virtualization* és una tecnologia que permet executar un hipervisor (programari que gestiona màquines virtuals) dins d'una màquina virtual. En termes simples, permet crear una capa addicional de virtualització dins d'una màquina virtual existent.

Això és útil en entorns de desenvolupament, proves i educatius, on **es poden crear i executar màquines virtuals dins d'altres màquines virtuals.** Per exemple, es pot crear entorns de proves aïllats dins d'una màquina virtual principal o executar hipervisors addicionals per simular configuracions complexes sense necessitat de maquinari físic addicional.

![](./domains.images/domains61.png){width="60%"}

### Change owner

La funció *'change owner'* permet modificar el propietari associat a un escriptori. En seleccionar aquesta opció, s'habilita la capacitat de transferir la propietat de l'escriptori a un altre usuari, brindant la flexibilitat d'assignar responsables o administradors nous per a aquest entorn virtual.

Per canviar de propietari, es prem el botó ![](./domains.images/domains73.png) i apareixerà un formulari. S'escriu el nom de l'usuari al qual es vol canviar de propietat l'escriptori.

![](./domains.images/domains74.png){width="45%"}
### Server

El terme *'server'* fa referència a una opció que, en ser seleccionada, permet que l'escriptori virtual romangui encès de manera contínua, funcionant com a servidor. Això assegura que l'escriptori estigui sempre actiu i disponible per accedir-hi, similar al comportament d'un servidor, fins i tot quan no és utilitzat activament per un usuari.

Per fer l'escriptori servidor, es prem el botó ![](./domains.images/domains71.png) i apareixerà una finestra de diàleg. En aquesta finestra se selecciona la casella.

![](./domains.images/domains72.png){width="45%"}

### XML

!!! danger "Important"
    Actualment només el rol **administrador** pot editar l'XML dels escriptoris.

Si es prem el botó ![XML](./domains.images/domains52.png) apareixerà una finestra on es pot modificar el fitxer de configuració de la màquina virtual [KVM/QEMU XML](https://libvirt.org/formatdomain.html).

### Delete

Per eliminar l'escriptori, es prem el botó ![](./domains.images/domains70.png). Apareixerà una finestra de confirmació que preguntarà si voleu procedir amb l'eliminació.


## Operacions de disc

Quan s'està efectuant una operació en el disc d'emmagatzematge d'un escriptori, aquest mostrarà l'estat "Maintenance". Els escriptoris en aquest estat no es poden iniciar fins que finalitzi el procés d'operació de disc.

![(Escriptori en estat "Maintenance" i un botó amb l'acció "Cancel Task")](./domains.images/domains75.png){width="65%"}

En alguns casos, el procés de manteniment pot trigar més del que s'esperava, possiblement a causa d'un procés bloquejat. Si això passa, es pot fer clic al botó "Cancel operation". Aquesta acció cancel·la la tasca en curs i hauria de tornar l'escriptori a un estat en el qual es pugui iniciar, canviant el seu estat de "Failed" a "StartedPaused" a, si no hi ha hagut cap problema, "Stopped".

![(Tooltip que mostra "Operations are being performed over the desktop disk: virt_win_reg")](./domains.images/domains76.png){width="65%"}

!!! Tip "Consell"
    Passar el cursor sobre la icona d'informació mostrarà quina operació específica s'està realitzant al disc de l'escriptori.

!!! Warning "Advertència"
    Cancel·lar l'operació del disc comporta un petit risc que el disc d'emmagatzematge es corrompi, i per tant que l'escriptori sigui inutilitzable.



## Global actions

A la part superior de la taula tenim un desplegable amb diverses accions. Aquestes accions s'aplicaran a tots els escriptoris seleccionats.

![Desplegable d'accions globals](./domains.images/domains54.png)

Per a seleccionar un escriptori, simplement s'ha de prémer a la fila de la taula. Els escriptoris seleccionats apareixeran com a marcats a la casella de "selected" al final de la fila. Encara que es navegui a la pàgina següent, els escriptoris seleccionats romandran marcats.

![Checkboxes de selecció](./domains.images/domains55.png)

Alternativament, si no se selecciona cap escriptori, es poden fer servir els filtres de dalt o de sota de la taula per a filtrar-los.

!!! Warning Avís
    Si no se selecciona cap escriptori o apliqueu cap filtre, **tots els escriptoris se seleccionaran per defecte.**

|Accions|Descripció|
|:---------|----------------------------------|
|**Soft toggle started/shutting-down state**|Reverteix l'estat actual de l'escriptori: si està apagat, enviarà un senyal per engegar-lo; si està encès, enviarà un senyal per apagar-lo. És important destacar que aquesta funció no força l'estat de l'escriptori, sinó que simplement executa l'acció oposada a l'estat actual sense forçar canvis bruscos.
|**Toggle started/stopped state**|Altera l'estat actual de l'escriptori, emet un senyal per encendre'l si està apagat o apagar-lo si està encès. És important ressaltar que aquesta funció força el canvi d'estat de l'escriptori, executant l'acció oposada a l'estat actual sense considerar altres circumstàncies.
|**Soft shut down**|Envia un senyal d'apagat a l'escriptori de manera suau i controlada, sense forçar canvis abruptes al seu estat actual.|
|**Shut down**|Envia un senyal d'apagat a l'escriptori, forçant canvis abruptes al seu estat actual|
|**Force Failed state**|En cas que un escriptori quedi en un estat intermedi a causa d'un error durant la seva creació o modificació, el força a canviar a un estat de 'Failed'. Això ajuda a identificar i registrar explícitament la situació problemàtica de l'escriptori per al seu diagnòstic i resolució posteriors.|
|**Delete**|Esborra els escriptoris que han estat seleccionats. En cas de no haver-ne cap de seleccionat, procedeix a eliminar tots els escriptoris existents.|
|**Start Paused (check status)**|Inicia l'escriptori virtual però l'atura immediatament després, permetent verificar-ne l'estat actual.|
|**Remove forced hypervisor**|Elimina qualsevol selecció prèvia que hagi estat configurada per arrencar l'escriptori amb un hipervisor específic. En resum, reverteix la configuració d'arrencada forçada amb un hipervisor determinat, si s'havia establert prèviament.|
|**Remove favourite hypervisor**|Elimina qualsevol selecció prèvia que s'hagi configurat com a hipervisor preferit per a l'arrencada. Desfà la preferència prèviament establerta per a l'inici amb un hipervisor específic, si s'havia designat com a favorit.

## Bulk Edit Desktops

Es poden seleccionar diversos escriptoris fent clic a les files de la taula. De manera alternativa, es poden filtrar els escriptoris sense fer clic a les files i tots els escriptoris que es mostren a la taula comptaran com a seleccionats, igual que es fa amb Global Actions.

Es prem el botó ![Bulk Edit Desktops](./domains.images/domains40.png) i apareixerà una finestra:

![(Finestra d'actualitzar múltiples escriptoris alhora)](./domains.images/domains41.png){width="50%"}

|Accions|Descripció|
|:---------|----------------------------------|
|**Desktops to edit**|Indica tots els escriptoris que s'actualitzaran amb les dades del formulari. En cas que hi hagi molts escriptoris, es pot anar desplaçant per la llista per seleccionar-los.|
|**Desktops viewers**|Si està marcada, apareixerà una secció nova. Els visualitzadors dels escriptoris s'actualitzaran segons estigui seleccionat. ![Update viewers section](./domains.images/domains42.png){width="60%"}|
|**Update RDP credentials**|Si està marcada, es canviarà el nom d'usuari i la contrasenya utilitzats per a les sessions RDP. ![(Update RDP credentials)](./domains.images/domains43.png){width="60%"}|
|**Hardware**|Les dades només es canviaran als camps on el valor seleccionat és diferent de "--"|
|**Update network**|Si està marcada, apareixerà una secció nova. Les interfícies dels escriptoris seleccionades s'actualitzaran utilitzant la tecla CTRL mentre premeu les opcions per seleccionar més d'una interfície alhora. ![(Update network)](./domains.images/domains44.png){width="60%"}|
|**Bookable resources**|Els reservables s'actualitzaran quan el valor seleccionat sigui diferent de "--"|

Per aplicar tots els canvis i actualitzar els escriptoris seleccionats, s'ha de prémer el botó ![(Modify desktops)](./domains.images/domains45.png){width="10%"}

## Bulk Add Desktops

En prémer a ![Bulk Add Desktops](./domains.images/domains56.png) apareixerà una finestra

![(Modal de Bulk Add)](./domains.images/domains57.png){width="45%"}

Aquest formulari permet crear diversos escriptoris simultàniament. Per crear-los, simplement s'ha d'omplir el formulari especificant un nom pels escriptoris nous i seleccionant la plantilla en què es basaran.

A la secció **"Allowed"**, es poden seleccionar els usuaris pels quals es volen crear els escriptoris triant entre rols, categories, grups o noms d'usuari. Es crearà un escriptori separat per cada usuari seleccionat.