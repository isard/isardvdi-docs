# Introduction

!!! warning "Role access"

    Only **managers** and **administrators** have access to this feature.
    Managers are restricted to their own category and do not have permissions to all the indicated functionalities.

The purpose of this section, reserved for **managers and administrators**, is to facilitate greater control, supervision and organization of the various sections and categories within the platform, allowing for more efficient and structured management of the available resources.

To go to the "Administration" panel and see the functionalities, press the button ![](./users.images/users1.png){width="12%"}

![](./users.images/users2.png){width="80%"}
