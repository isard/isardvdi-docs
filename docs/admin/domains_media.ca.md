# Introducció

!!! info "Rols amb accés"
    Només els **administradors** tenen accés a totes les categories dins de la plataforma. El **gestor** està limitat a la seva categoria.

Aquesta secció mostra tots els mitjans creats pels usuaris dins d'una o més categories.

![](./domains.images/media1.png){width="80%"}

1. Media: En aquest apartat es mostren tots els mitjans pujats a la plataforma.

2. Media in other status: Mitjans que tenen un altre tipus d'estat, com poden ser els esborrats, els que s'han descarregat malament o s'han quedat en manteniment.


## Media

* **Show last task info** ![](./domains.images/media2.png) : Verifica si s'està executant la tasca de "Check media".

* **Check media status** ![](./domains.images/media3.png) : Verifica si el mitjà existeix i si l'ISO està físicament en el disc. Si no existeix, s'eliminarà automàticament.

* **Create desktop from media** ![](./domains.images/media4.png) : Crea un escriptori virtual a partir d'aquest mitjà.

* **Changed allowed users** ![](./domains.images/media5.png) : Canvia els permisos de compartició del mitjà.

* **Change owners** ![](./domains.images/media6.png) : Canvia de propietari el mitjà.

* **Delete media** ![](./domains.images/media7.png) : Esborra el mitjà.


## Media in other status

* **Deleted**: Mitjans que ja han estat eliminats de la plataforma.
* **Download Aborting**: Mitjans la descàrrega dels quals ha estat avortada, per exemple, quan es cancel·la una descàrrega en procés.
* **Download Failed**: Mitjans amb descàrregues fallides.
* **Download Failed Invalid Format**: Mitjans la descàrrega dels quals ha fallat a causa d'un format invàlid de la ISO.
* **Reset Downloading**: Mitjans en els quals la descàrrega s'ha reiniciat.