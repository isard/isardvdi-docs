# Introducció

La següent secció proporciona una vista detallada de la informació dels discos. Aquí, els usuaris poden trobar dades crucials com la grandària utilitzada i total dels discos, així com el seu estat actual, incloent-hi si han estat eliminats o si han experimentat algun error per qualsevol motiu.

Amb els següents botons:

- **Check disk info** ![](./storage.images/check.png){width="3%"}: Aquest botó el que fa és revisar que l'estat de l'emmagatzematge sigui el correcte.
- **Show last task info** ![](./storage.images/show.png){width="3%"}: S'obre un modal on dona informació de l'última tasca que ha realitzat aquell emmagatzematge.

![](./storage.images/task_info.png){width="60%"}


## Storage files

![](./storage.images/storage_files.png){width="80%"}

## Other status storage files

![](./storage.images/other_status.png){width="80%"}

## Deleted storage files log

![](./storage.images/deleted_storage.png){width="80%"}