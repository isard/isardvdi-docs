# Introducción

!!! warning "Roles con acceso"

    Solo los **gestores** y los **administradores** tienen acceso a esta característica.
    Los gestores están restringidos a su propia categoría y no tienen permisos a todas las funcionalidades indicadas.

La finalidad de esta sección, reservada para **gestores y administradores**, es facilitar un mayor control, supervisión y organización de las diversas secciones y categorías dentro de la plataforma, permitiendo una gestión más eficiente y estructurada de los recursos disponibles.

Para ir al panel de "Administración" y ver las funcionalidades, se pulsa el botón ![](./users.es.images/users1.png){width="12%"}

![](./users.es.images/users2.png){width="80%"}
