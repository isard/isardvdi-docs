# Introducció

!!! warning "Rols amb accés"

    Només els **gestors** i els **administradors** tenen accés a aquesta característica.
    Els gestors estan restringits a la seva pròpia categoria i no tenen permisos a totes les funcionalitats indicades.

La finalitat d'aquesta secció, reservada per a **gestors i administradors**, és facilitar un major control, supervisió i organització de les diverses seccions i categories dins de la plataforma, permetent una gestió més eficient i estructurada dels recursos disponibles.

Per a anar al panell d'"Administració" i veure les funcionalitats, es prem el botó ![](./users.ca.images/users1.png){width="12%"}

![](./users.ca.images/users2.png){width="80%"}

