# Introducción

En esta sección se pueden ver todos los recursos que tiene la instalación, éstos saldrán al crear un escritorio/plantilla.

![](./domains.images/domains10.png){width="80%"}

## Videos

En esta sección se pueden añadir diferentes formatos de vídeo.

Para añadir un nuevo formato, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

Y se rellena el formulario

![](./domains.images/domains15.png)

## Interfaces

En esta sección se pueden añadir redes privadas a los escritorios.

Para añadir una red, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

Y se rellena el formulario

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: Enlaza con un puente hacia una red del servidor. En el servidor se puede tener interfaces que enlacen por ejemplo con vlans hacia un troncal de tu red, y se puede mapear estas interfaces en isard y conectarlas a los escritorios.

    Para poder mapear la interfaz dentro del hipervisor, en el archivo de isardvdi.conf se tiene que modificar esta línea:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - Es más eficiente utilizar virtio (que es una interface paravirtualizada), mientras que las e1000 o rtl son simulaciones de tarjetas, y va más lento aunque es más compatible con sistemas operativos antiguos y no se necesita instalar drivers en el caso de windows.

    **Si se tiene un sistema operativo moderno, con el virtio funciona, sino con los otros que son interfaces. Windows antiguos: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


## Boots

En esta sección se indican los diferentes modos de arranque de un escritorio.

**Si no se tiene permisos no se puede seleccionar una iso de arranque y no se deja por defecto que los usuarios se puedan mapear isos.**

Para dar permisos se pulsa en el icono ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


## Network QoS

En esta sección se indican las limitaciones que se pueden poner a las redes.

Para añadir una limitación, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

Y se rellena el formulario

![](./domains.images/domains20.png)


## Disk QoS

En esta sección se indican las limitaciones que se pueden poner a los discos.

Para añadir un límite de disco, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

Y se rellena el formulario

![](./domains.images/domains22.png)


## Remote VPNs

En esta sección se pueden añadir redes remotas a los escritorios.

Para añadir una red remota, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

Y se rellena el formulario

![](./domains.images/domains24.png)

