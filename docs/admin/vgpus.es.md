# vGPUs

!!! info "Roles con acceso"
    Solo los **administradores** tienen acceso a esta característica.

IsardVDI admite el uso de GPUs virtuales (vGPUs) para escritorios y despliegues. Más información sobre cómo funciona esta tecnología se puede encontrar [aquí](../../deploy/gpus/gpus.es/).

## Eliminar vGPU

Para eliminar una vGPU:

1. Navegar a **Administration > Infrastructure > Hypervisors > GPUs**.
2. En la tabla de GPUs, hacer clic en el botón de eliminar en la columna "Action".

![Tabla de GPUs y una notificación que muestra "Delete GPU. Do you really want to delete this gpu?"](./vgpus.images/vgpus1.png)

Una GPU es un [recurso reservable](../../user/bookings.es/#bookings) que puede asignarse a escritorios. Eliminar una GPU implicará retirarla de todos los escritorios de usuarios y eliminar todas las reservas relacionadas.

El sistema mostrará una advertencia detallando los elementos que se eliminarán/retirarán y los usuarios afectados:

![Modal de eliminar GPU que informa al usuario sobre modificaciones en sus planes, reservas, escritorios y despliegues](./vgpus.images/vgpus2.png)

Deshabilitar un perfil de GPU individual desmarcando su casilla resultará en una advertencia similar.

!!! info
    Si el perfil de la GPU a eliminar se encuentra también en otra GPU activa, sus reservas y asignaciones a escritorios **permanecerán intactos**. Para comprobar cuáles son los perfiles de cada GPU, se puede hacer clic en el botón ![Detail](./domains.images/domains5.png) en la fila correspondiente.

!!! info
    Si un escritorio tiene una reserva en curso en el momento de la eliminación, no se podrá eliminar la GPU. Se debe parar el escritorio y eliminar la reserva o esperar a que finalice el tiempo de reserva.

### Notificar a los usuarios

Si la casilla **Notify users that their bookings will be deleted** está marcada, los usuarios serán informados por correo electrónico sobre la eliminación de sus asignaciones a escritorios y reservas de GPU.

La notificación por correo electrónico predeterminada se ve así:

![Contenido del correo electrónico advirtiendo al usuario de la eliminación de la GPU](./vgpus.es.images/vgpus1.png)

El contenido del correo electrónico se puede modificar en **Administration > Config > Notifications > Notification templates** , listado como "Deleted GPU Profile".

!!! warning "Advertencia"
    Los usuarios que no tengan una dirección de correo electrónico válida vinculada a su perfil **no** serán notificados.
