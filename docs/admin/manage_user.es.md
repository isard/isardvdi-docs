# Gestión de usuarios

Para gestionar los usuarios se tiene que ir al [panel de Administración](./index.es.md)

Se pulsa el botón ![](./manage_user.images/manage_user24.png){width="10%"}

![](./manage_user.images/manage_user25.png){width="80%"}

## Creación de Grupos

Para crear grupos se debe acceder a la subsección **"Management"** bajo la sección **"User"**. Seguidamente se busca la tabla **"Groups"** y se pulsa el botón ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png){width="80%"}

Y se rellenan los campos.

![](./manage_user.images/manage_user3.png){width="60%"}

!!! Info "Notas importantes sobre los campos del formulario"
    * **Linked groups**: Todos los recursos compartidos con sus grupos enlazados se heredarán automáticamente por el grupo creado. Por ejemplo, si un Grupo A se crea con Grupo B como grupo enlazado, todos los recursos compartidos con Grupo B también se compartirán con Grupo A.

    * **Ephimeral desktops**: Establecerá un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar, siendo Admin, en Config el "Job Scheduler")

## Edición de Grupos

Se pueden editar los parámetros de un grupo pulsando el icono ![](./manage_user.images/manage_user13.png) junto al grupo que se quiera actualizar, y se pulsa ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png){width="80%"}

Una ventana de diálogo aparecerá con los mismos parámetros que la ventana de creación.

![](./manage_user.images/manage_user29.png){width="60%"}

## Creación de Usuarios

Para crear usuarios se tiene que acceder a la subsección **"Management"** bajo la sección **"User"**.

### Individualmente

Se busca la tabla **"Users"** y se pulsa el botón ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png){width="80%"}

Saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user6.png){width="60%"}

Y se rellenan los campos.

!!! Info "Notas importantes sobre los campos del formulario"
    **Secondary groups**: Todos los recursos compartidos con sus grupos secundarios serán heredados automáticamente por el usuario creado. Además, el usuario será añadido a todos los despliegues que sean creados en cualquiera de sus grupos secundarios.

### Creación masiva

Se busca la tabla **"Users"** y se pulsa el botón ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png){width="80%"}

Saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user9.png){width="60%"}

Se puede descargar un archivo de ejemplo pulsando el botón ![](./manage_user.images/manage_user10.png){width="20%"}. Saldrá un formulario a rellenar:

!!! Warning "Evitando errores"
    * Los campos "**category**" y "**group**" deben ser **exactamente** iguales a su nombre
    * La codificación recomendada es **Unicode (UTF-8)**
    * Los csv deben de estar separados por **tabulaciones** <kbd>Tab ↹</kbd>
    * Es altamente recomendado utilizar el csv de ejemplo

![](./manage_user.images/manage_user11.png)

Una vez rellenado, se puede subir en 

![](./manage_user.images/manage_user12.png)

Y si se ha subido correctamente, saldrá una tabla de previsualización de los usuarios:

![](./manage_user.images/manage_user28.png){width="60%"}

Para exportar los usuarios con la contraseña generada en un CSV, pulsa el botón ![](./manage_user.images/manage_user63.png)

!!! Failure "Errores"
    Si el csv no se ha subido correctamente se mostrará un error indicando el motivo.

### Generar CSV para crear usuarios existentes

Para generar un CSV con los usuarios existentes para crearlos de nuevo, selecciona los usuarios y pulsa el botón ![](./manage_user.images/manage_user64.png).

![](./manage_user.images/manage_user65.png){width="80%"}

Podrás importar el archivo CSV para crear los usuarios de nuevo. El archivo CSV no contendrá las contraseñas de los usuarios.

Si lo que se quiere es actualizar los usuarios, se tiene que utilizar él botón [**CSV for update**](#actualización-de-usuarios).

## Edición de Usuarios

Para editar usuarios se debe acceder a la subsección **"Management"** bajo la sección **"User"**.

### Individualmente

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere actualizar, entonces se pulsa ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png){width="80%"}

Una ventana de diálogo aparecerá con los mismos parámetros que la ventana de creación.

![](./manage_user.images/manage_user35.png){width="60%"}

!!! Info "Notas importantes sobre los campos del formulario"
    Los campos "**username**", "**category**" y "**group**" no se pueden modificar.

### Edición masiva

Se selecciona uno o más usuarios y se pulsa el botón ![](./manage_user.images/manage_user45.png){width="10%"}

![](./manage_user.images/manage_user46.png){width="80%"}

Saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user47.png){width="45%"}

!!! note "Fields"
    === "Update active/inactive"
        ![](./manage_user.images/manage_user48.png){width="20%"}  
            - **Update active/inactive**: Se actualiza el estado del usuario en la plataforma. Este usuario puede estar **activo**, tiene acceso a la plataforma, o **inactivo**, no tiene acceso a la plataforma.
    === "Update secondary group"
        ![](./manage_user.images/manage_user49.png){width="50%"}        
            - **Update secondary group**: Se pueden actualizar los grupos secundarios de diversos usuarios al mismo tiempo. Para ello, se escribe el nombre del grupo y se selecciona una de estas opciones:  
                - **Overwrite**: Sobrescribe el grupo secundario que tenía asignado.  
                - **Add**: Se añade el grupo secundario al usuario.  
                - **Delete**: Suprime el grupo secundario que el usuario tenía asignado.

### Actualización de usuarios

Para actualizar usuarios mediante un fichero .csv, primero se tiene que descargar el fichero con la información de los usuarios. Para hacer eso, se pulsa el botón ![](./manage_user.images/manage_user53.png){width="15%"}

![](./manage_user.images/manage_user54.png){width="80%"}

Después, se pulsa el botón ![](./manage_user.images/manage_user50.png){width="15%"}

![](./manage_user.images/manage_user51.png){width="80%"}

Saldrá una ventana de diálogo con el siguiente formulario donde se podrá subir el fichero o descargar uno de ejemplo:

![](./manage_user.images/manage_user52.png){width="80%"}

!!! Warning "Evitando errores"
    * Los campos "**username**", "**category**" y "**group**" tienen que avenirse **exactamente** con su nombre para actualizar el usuario
    * Se recomienda la codificación **Unicode (UTF-8)**
    * El archivo csv tiene que estar separado por **tabulaciones** <kbd>Tab ↹</kbd>

!!! Info "Notas importantes respecto a los campos"
    * Los campos "**provider**", "**category**", "**uid**", y "**group**" no se pueden modificar. El resto de campos se actualizarán.
    * Si se dejan un casilla en **blanco**, no se actualizará.
    * La columna "Active", tendrá que tener un valor **"true"** o **"false"**
    * Para añadir más de un **grupo secundario** a los usuarios, se tiene que separar los grupos de la columna "secondary_groups" con **"/"**, ejemplo "prova/Test"

Si lo que se quiere es generar un CSV para crear los usuarios de nuevo, se tiene que utilizar él botón [**CSV for create**](#generar-csv-para-crear-usuarios-existentes).

### Habilitar/Deshabilitar

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere habilitar/deshabilitar, entonces se pulsa el botón ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png){width="80%"}

El estado de un usuario se puede ver en la tabla de usuarios.

![](./manage_user.images/manage_user38.png){width="80%"}

!!! Danger "Vayan con cuidado"
    Si el usuario se deshabilita mientras accede al sistema pueden producirse problemas de sesión.

### Cambiar contraseña

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere cambiar la contraseña, entonces se pulsa ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png){width="80%"}

Se abrirá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user40.png){width="45%"}

!!! Tip "Sugerencia"
    El usuario puede cambiar su contraseña mediante su [perfil](../user/profile.es.md).

### Migrar usuario

Se pueden **migrar de cuentas locales hacia cuentas de Google (viceversa o cualquier otro tipo de cuenta)** todos los elementos de un usuario hacia otro (siempre y cuando tengan el mismo rol o inferior) pulsando el icono ![](./manage_user.images/manage_user13.png) al costado del usuario que se quiera migrar, entonces se pulsa ![](./manage_user.images/migrate2.png).

![](./manage_user.images/migrate1.png){width="80%"}

Una ventana de diálogo se abrirá donde mostrará si el usuario tiene algún ítem creado (escritorio, plantilla, despliegue o medios)

![](./manage_user.images/migrate3.png){width="70%"}

Juntamente con una serie de advertencias a la hora de hacer la migración de estos ítems de un usuario a otro.

!!! Warning "Advertencia"
    - Cualquier escritorio que esté encendido, se apagará.
    - La papelera de reciclaje del usuario se vaciará. 
    - Se eliminarán las reservas del usuario.
    - Los escritorios no persistentes se ELIMINARÁN.
    - Se borrará el listado de copropietario en despliegues.
    - Los escritorios que pertenezcan a otros usuarios en los despliegues migrados seguirán intactos.
    - Los recursos no permitidos para el nuevo usuario se eliminarán o restringirán.

Después de leer las advertencias, podemos buscar el usuario, el cual queremos trasladar los ítems por nombre y pulsando el botón "Migrate user"

![](./manage_user.images/migrate4.png){width="80%"}


### Suplantar 

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere suplantar, entonces se pulsa ![](./manage_user.images/manage_user44.png).

![](./manage_user.images/manage_user41.png){width="80%"}

!!! Danger "Aviso de exención de responsabilidad"
    Suplantar un usuario **proporciona el acceso a todos sus datos y escritorios y comporta riesgos inherentes**. Antes de continuar, es importante **considerar la sensibilidad de la información a la cual se accederá**.

## Clave de autoregistro

En la tabla **"Grupos"** se busca el nombre del grupo del que se quiera obtener el codigo. Se pulsa el botón ![](./manage_user.images/manage_user13.png)

![](./manage_user.images/manage_user14.png)

El grupo se despliega con unas opciones. Se pulsa el botón ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

Se abrirá una ventana de diálogo donde se pueden generar códigos pulsando las diferentes casillas de selección.

![](./manage_user.images/manage_user32.png)

Una vez generado el código de autoregistro este puede ser copiado y compartido con los usuarios.

![](./manage_user.images/manage_user17.png)

!!! Danger "Vayan con cuidado"
    * Al registrarse, **los usuarios tendrán el rol del código de autoregistro compartido**. Por ejemplo, a los profesores, se les dará el código "Advanced"y a los alumnos el código de "Users".
    * La cantidad de usuarios que pueden registrarse con un código es ilimitado. Por este motivo, es recomendable desactivarlos una vez utilizados.

## Creación de categorías

!!! warning "Roles con acceso"

    Solo los **administradores** tienen acceso a esta característica.

En el apartado **"Users"**, se busca la sección **"Categories"** y se pulsa el botón ![](./users.images/users3.png)

![](./users.images/users4.png){width="80%"}

Aparecerá una ventan de diálogo donde se rellenan los campos del formulario.

![](./users.images/users5.png){width="80%"}

Hay tres opciones que se pueden aplicar al crear una categoría:

* **Automatic recycle bin delete**: Se aplicará la funcionalidad de la papelera de reciclaje a los usuarios. Se puede seleccionar si se quiere que los escritorios se borren después de una hora o inmediatamente. Este tiempo se puede modificar aquí.

![](./users.images/users7.png){width="40%"}

* **Frontend dropdown show**: En la página de inicio de sesión, aparecerá la categoría creada en el menú desplegable.

![](./users.ca.images/users3.png){width="40%"}

!!! info
    Si la categoría está oculta, se puede iniciar sesión en esa categoría usando la URL `/login/<category_id>`

* **Ephemeral desktops**: Para poder poner un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar en la sección de "Config" el "Job Scheduler")

![](./users.images/users8.png){width="40%"}

## Acceso mediante OAuth

!!! warning "Roles con acceso"

    Solo los **administradores** tienen acceso a esta característica.

Para poder acceder mediante Google desde una cuenta externa, se tiene que ir al apartado **"Users"** y a la sección de **"Categories"**, se pulsa el icono ![](./users.images/users9.png) de la categoria la cual se quiere dar acceso.

![](./users.images/users10.png){width="80%"}

Se pulsa el botón ![](./users.images/users11.png)

![](./users.images/users12.png){width="80%"}

Aparece una ventana de diálogo con un formulario. Se rellena el campo de "Allowed email domain for foreign account like Google" con el dominio del correo.

![](./users.images/users13.png){width="45%"}