# Configuración de logs

!!! warning "Roles con acceso"
    Sólo los **administradores (admins)** tienen acceso a esta característica.

Esta página ofrece opciones de configuración para los logs de **escritorios** y **usuarios**.

## Logs antiguos

![Configuración de logs antiguos](./logs_config.images/logs_config1.png)

En esta sección, se puede configurar la eliminación automática de logs que superen una cierta antigüedad.
Este proceso se realizará diariamente a las 00:30 UTC.

!!! tip "Consejo"
    Se recomienda mantener los logs de escritorio durante un período de tiempo más largo que el tiempo máximo de **Desktop Timeout** configurado para evitar la eliminación de logs de escritorios activos.
