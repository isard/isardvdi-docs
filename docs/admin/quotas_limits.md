# Quotas and Limits

To manage quotas and limits you have to go to the [administration panel](./index.md) and press the button ![](./quotas_limits.images/quotas_limits3.png){width="10%"}

![](./quotas_limits.images/quotas_limits4.png){width="80%"}

## Quotas

Quotas define the amount of resources that can be used **individually** (by each user), based on the category and group to which the user belongs to.

Quotas restrictions are divided as follows:

![](./quotas_limits.images/quotas_limits8.png){width="80%"}

**Creation Quota**

* **Persistent**: Amount of persistent desktops that the user is allowed to create.
* **Temporal**: Amount of temporal desktops that the user is allowed to create.
* **Templates**: Amount of templates that the user is allowed to create.
* **Media**: Amount of media that the user is allowed to create.
* **Deployments**: Amount of deployments that the user is allowed to create.
* **Deploy desktops**: Amount of desktops that the user is allowed to create in each deployment.

!!! Warning 
    * **Shared resources** with the user are **not computed in the user quota**.
    * Desktops created **through deployments** are **not computed in the user quota**.

**Running desktops Quota**

* **Concurrent**: Total amount of desktops that can be started at the same time.
* **vCPUs**: Total amount of vCPUS of started desktops that can be used at the same time.
* **Memory (GB)**: Total amount of vRAM memory of started desktops that can be used at the same time.
* **Deployments**: Total amount of desktops from a deployment that can be started as the owner of a deployment.

!!! Warning
    When a **desktop from a deployment** is started by an **owner** or **co-owner** of the deployment its computed in the **Deployments** quota of the user that started it. 
    If the user starts their desktop from the deployment, the **Concurrent** quota of the user will be applied.
    If an admin or manager starts a desktop from a deployment they don't own, no quota will be applied.

**Size Quota**

* **Disk size**: Maximum size allowed when creating a desktop from a media.
* **Total size**: Total size that can be used considering desktops, templates and media.
* **Soft size**: Once reached this size the user will be warned.

Quotas can be set by user, group or category. They're applied by levels, starting from the bottom to the top (the lower level quota will be applied). For instance, considering the following structure:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Category --> Group2[Group 2]
    Category --> Group3[Group 3]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
    Group2 --> User4[User 4]
    Group2 --> User5[User 5]
    Group2 --> User6[User 6]
    Group3 --> User7[User 7]
    Group3 --> User8[User 8]
    Group3 --> User9[User 9]
```

* If the users have a **user quota set** it will be applied. Its **group and category quota** will be **ignored**.
* If the users haven't got a user quota set and a **group quota is set**, its group quota will be applied. Its **category quota** will be **ignored**.
* If **neither** of the **users or groups quotas** are **set**, its **category quota** will be **applied**. The category quota can only set by an [administrator role](../admin/index.md).
* If **no quota is applied** all users will have access to create **unlimited** desktops, templates, and isos with unlimited vCPU, memory vRAM and disk size.

!!! Tip

    The applied restriction (user, group or category quota) and the resources usage can be consulted through the users [profile page](../user/profile.md).

    === "User Quota Applied"
        ![](./quotas_limits.images/quotas_limits5.png)

    === "Group Quota Applied"
        ![](./quotas_limits.images/quotas_limits6.png)

    === "Category Quota Applied"
        ![](./quotas_limits.images/quotas_limits7.png)

### User Quota

Users quota parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to update, and then pressing ![](./quotas_limits.images/quotas_limits9.png).

The following dialog box will pop up:

![](./quotas_limits.images/quotas_limits10.png){width="70%"}

!!! Info "Important notes about the form fields"
    * If the "Apply group quota" checkbox is selected the form values will be ignored and the user will be applied its group quota.
    * When unchecking the "Apply group quota" checkbox the form input boxes will be enabled and the quota will be applied to the user **individually**. Refer to [quotas](#quotas) to view each fields purpose.

### Group Quota

Group quota parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the group we want to update, and then pressing ![](./quotas_limits.images/quotas_limits9.png).

The following dialog box will pop up:

![](./quotas_limits.images/quotas_limits11.png){width="70%"}

!!! Info "Important notes about the form fields"
    * If the "Apply category quota" checkbox is selected the **form** values **will be ignored** and **the group will be applied its category quota**.
    * When unchecking the "Apply category quota" checkbox the form **input boxes will be enabled**. Refer to [quotas](#quotas) to view each fields purpose.
    * Override group users current quota checkbox will apply the form quota to each user **individually**. Hence, each of the users quota will be overriden. Note that [admins](../admin/index.md) can set higher quotas to users than managers, thus, when overriding the users quota these exceptions would be also overriden.
    * Apply current changes will apply the form quota **only to the users with the selected role**.

## Limits

Limits define the amount of resources that can be used globally in a category or a group. 

For instance, considering a Group 1 with a limit of 10 desktops without any quota applied:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
```

The total amount of desktops that can be created is 10 and since there's no quota applied, when an user creates a desktop, the system checks if the stablished limit is surpassed. Hence, there's no control over the amount of desktops each user has (that would be controlled through quotas) and greater flexibility is offered, since many combinations could be given:

* User 1 could have 6 desktops, User 2 could have 3 desktops and User 3 could have 1 desktop.
* User 1 could have 9 desktops, User 2 could have 1 desktop.
* User 1 could have 2 desktops, User 2 could have 7 desktops and User 3 could have 1 desktop.
* User 1 could have 10 desktops, therefore no more desktops could be created.
* Etc

### Group Limits

Group limits parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the group we want to update, and then pressing ![](./quotas_limits.images/quotas_limits12.png).

The following dialog box will pop up:

![](./quotas_limits.images/quotas_limits13.png){width="70%"}

!!! Info "Important notes about the form fields"
    * If the "Apply category limits" checkbox is selected the **form** values **will be ignored** and **the group will be applied its category limits**.
    * The **Concurrent** limit takes into account all started desktops, regardless of the user that started them.
    * Refer to [limits](#limits) for further information about the limits functionality.