# Introducción

La siguiente sección proporciona una vista detallada de la información de los discos. Aquí, los usuarios pueden encontrar datos cruciales como el tamaño utilizado y total de los discos, así como su estado actual, incluyendo si han sido eliminados o si han experimentado fallos por cualquier motivo.

Con los siguientes botones:

- **Check disk info** ![](./storage.images/check.png){width="3%"}: Este botón lo que hace es revisar que el estado del almacenamiento sea el correcto.
- **Show last task info** ![](./storage.images/show.png){width="3%"}: Se abre un modal donde proporciona información de la última tarea que ha realizado aquel almacenamiento.

![](./storage.images/task_info.png){width="60%"}

## Storage files

![](./storage.images/storage_files.png){width="80%"}

## Other status storage files

![](./storage.images/other_status.png){width="80%"}

## Deleted storage files log

![](./storage.images/deleted_storage.png){width="80%"}