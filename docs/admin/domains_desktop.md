# Introduction

!!! info "Roles with access"
    Only **administrators** have access to all categories within the platform. The **manager** is limited to his own category.

This section shows all desktops created by users within one or more categories.

![](./domains.images/domains4.png){width="80%"}

If you press the ![](./domains.images/domains5.png) icon, you can see more information about the desktop.

![](./domains.images/domains6.png){width="80%"}

|||
|:---------|----------------------------------|
|**Status detailed info** |Detailed information about the current status of the desktop. In the event of a desktop failure, this section will display a specific error message that may provide details to identify and resolve the problem that has arisen.|
|**Description**|Detailed description or additional information associated with the desktop in question.|
|**Template tree**|Graphic representation that displays the hierarchical structure of the desktops and their relationship with the base template from which they are derived, including their dependencies. This visual representation is useful for quickly and clearly understanding the origin and connections of a specific desktop.|
|**Hardware**|Physical (virtual) components and technological resources specifically assigned to the particular desktop.|
|**Storage**|Indicates the unique identifier (UUID) of the disk linked to the desktop, as well as information about its capacity and current usage.|
|**Media**|Indicates the presence of media files specifically associated with the desktop in question, if they exist.|  

## Individual actions

### Viewer

It is used to share a desktop through a direct link to it, without the user needing to be registered on the platform. (It can also be done from the home page, see [here](/docs/advanced/direct_viewer.md))

To share the link, press the ![](./domains.images/domains62.png) button and a dialog window will appear. In this window, select the box so that the link appears and you can copy it.

![](./domains.images/domains63.png){width="45%"}

### Template it

To convert the desktop into a template, press the ![](./domains.images/domains64.png) button and a dialog box will appear to fill out.

![](./domains.images/domains65.png){width="50%"}

|||
|:---------|----------------------------------|
|**Template name and description**|Allows you to add a name and description to the desktop|
|**Options**|Enables or disables the template's visibility and availability to users. Checking this box enables the template to be active and visible, otherwise it will remain hidden and unavailable to users|
|**Allows**|Allows you to select the specific users or groups with whom you want to share the template, determining who will have access and be able to use it.|

### Forced hyp

The term *"forced hypervisor"* describes a function that, when activated, allows you to force specify the hypervisor on which the virtual desktop will start. This action overrides any default settings and ensures that the desktop boots exclusively on the selected hypervisor, ignoring other settings or preferences.

!!! important "Important"
    When you enable this setting, the desktop **will only boot on the selected hypervisor and will not boot on any others.**

To force the hypervisor, press the ![](./domains.images/domains66.png) button and a dialog window will appear. In this window, select the box so that a drop-down menu appears and you can choose the available hypervisor.

![](./domains.images/domains67.png){width="45%"}

### Favourite hyp

The term *"favorite hypervisor"* describes a feature that allows you to designate a preferred hypervisor for the startup of a desktop. Selecting this option indicates the specific hypervisor that you prefer to use to boot the virtual desktop. This configuration ensures that, when available, the desktop automatically boots into the favorited hypervisor, optimizing the user experience when launching the desired virtual environment.

!!! important "Important"
    When this setting is enabled, the desktop **will boot to the selected hypervisor. If this is not available, the desktop will boot into another available hypervisor.**

To select a hypervisor as a favorite, press the ![](./domains.images/domains68.png) button and a dialog window will appear. In this window, select the box so that a drop-down menu appears and you can choose the available hypervisor.

![](./domains.images/domains69.png){width="45%"}

### Edit

To edit the desktop, press the ![Edit](./domains.images/domains51.png) button and a window will appear. In this window you can edit the desktop parameters.

![](./domains.images/domains60.png){width="60%"}

|||
|:---------|----------------------------------|
|**Domain name and description**|Allows modification of the name and description associated with the desktop|
|**Domain viewers**|Specifies the viewers to display when the desktop starts. Additionally, you can configure the desktop to automatically start in full screen mode |
|**Hardware**|Physical (virtual) components and technological resources specifically assigned to the particular desktop.|
|**Reservable resources**|Assigning specific resources, such as a graphics card, to a desktop if they are available. This assignment implies a reservation of resources for the operation of the desktop in question.|
|**Media**|indicates the presence of media files specifically associated with the desktop in question, if they exist.|

#### Nested virtualization

Nested virtualization is a technology that allows a hypervisor (software that manages virtual machines) to run inside a virtual machine. In simple terms, it allows you to create an additional layer of virtualization within an existing virtual machine.

This is useful in development, testing, and educational environments, where **virtual machines can be created and run within other virtual machines.** For example, you can create isolated test environments within a main virtual machine or run additional hypervisors to simulate complex configurations without the need for additional physical hardware.

![](./domains.images/domains61.png){width="60%"}

### Change owner

The *'change owner'* function allows you to modify the owner associated with a desktop. Selecting this option enables the ability to transfer ownership of the desktop to another user, providing the flexibility to assign new managers or administrators for that virtual environment.

To change the owner, press the ![](./domains.images/domains73.png) button and a form will appear. Enter the name of the user whose ownership of the desktop you want to change.

![](./domains.images/domains74.png){width="45%"}

### Server

The term *'server'* refers to an option that, when selected, allows the virtual desktop to remain on continuously, functioning as a server. This ensures that the desktop is always active and available for access, similar to the behavior of a server, even when it is not actively being used by a user.

To make the server desktop, press the ![](./domains.images/domains71.png) button and a dialog window will appear. In this window the box is selected.

![](./domains.images/domains72.png){width="45%"}

### XML

!!! danger "Important"
    Currently only the **administrator** role can edit the desktop XML.

If you press the ![XML](./domains.images/domains52.png) button a modal will appear where you can modify the [KVM/QEMU XML](https://libvirt.org/formatdomain.html) configuration file of the virtual machine.

### Delete

To delete a desktop, press the ![](./domains.images/domains70.png) button. A confirmation window will appear asking if you wish to proceed with the deletion.

## Storage operations

When a storage disk operation is being performed on a desktop, it will show the desktop status "Maintenance".Desktops in this status cannot be started until the storage operation process is finished.

![(Desktop row displaying the status "Maintenance" and a button with action "Cancel Task")](./domains.images/domains75.png){width="65%"}

In some cases, the maintenance process may take longer than expected, possibly due to a stalled process. If this occurs, users can click the "Cancel operation" button. This action cancels the ongoing task and should return the desktop to an startable state, changing its status from "Failed" to "StartedPaused" to "Stopped" if there have not been any issues.

![(Tooltip that reads "Operations are being performed over the desktop disk: virt_win_reg")](./domains.images/domains76.png){width="65%"}

!!! Tip Tip
    Hovering over the info icon will display the specific storage operation being performed on the desktop disk.

!!! Warning Warning
    Cancelling the disk operation carries a small risk of rendering the storage disk corrupted, and therefore the desktop, unusable. Please be cautious


## Global actions

On top of the table we have a dropdown with several actions. These actions will be applied to all selected desktops.

![Global actions dropdown](./domains.images/domains54.png)

To select a desktop, simply press on the row in the table. The selected desktops will appear as checked in the checkbox at the end of the row. Even if you navigate to the next page, the selected desktops will remain checked.

![Selected checkboxes](./domains.images/domains55.png)

Alternatively, if you don't select any desktops, you can use the filters above or below the table to filter them.

!!! Warning
    If you don't select any desktop or apply any filters, **ALL desktops will be selected by default**.

Once you have selected the desktops, choose an action from the dropdown to perform it.

|Actions|Description|
|:---------|----------------------------------|
|**Soft toggle started/shutting-down state**|Reverts the current state of the desktop: if it is off, it will send a signal to turn it on; If it is on, it will send a signal to turn it off. It is important to note that **this function does not force the desktop state**, but simply executes the opposite action to the current state without forcing abrupt changes.|
|**Toggle started/stopped state**|Alters the current state of the desktop, issuing a signal to turn it on if it is off or shut it down if it is on. It is important to note that **this function forces the desktop state to change**, executing the opposite action to the current state without considering other circumstances.|
|**Soft shut down**|Sends a shutdown signal to the desktop in a soft and controlled manner, without forcing abrupt changes in its current state.|
|**Shut down**|Sends a shutdown signal to the desktop, forcing abrupt changes to its current state|
|**Force Failed state**|In case a desktop is left in an intermediate state due to an error during its creation or modification, it is forced to change to a 'Failed' state. This helps to explicitly identify and record the problematic desktop situation for later diagnosis and resolution.|
|**Delete**|Deletes the desktops that have been selected. If none are selected, proceed to delete all existing desktops.|
|**Start Paused (check status)**|Starts the virtual desktop but stops it immediately afterwards, allowing you to check its current status.|
|**Remove forced hypervisor**|Removes any previous selection that has been configured to boot the desktop with a specific hypervisor. In short, it reverts the force boot configuration with a given hypervisor, if it was previously set.|
|**Remove favorite hypervisor**|Removes any previous selection that was set as the favorite hypervisor for boot. Undoes the previously set preference for booting with a specific hypervisor, if it had been designated as a favorite.|  


## Bulk edit desktops

You can select multiple desktops by pressing on the rows on the table. Alternatively you can filter the desktops without pressing on the rows and all the desktops seen on the table will count as selected.

If you press the ![Bulk Edit Desktops](./domains.images/domains40.png) button a modal will appear:

![(Edit Multiple Desktops modal)](./domains.images/domains41.png){width="50%"}

|Actions|Description|
|:---------|----------------------------------|
|**Desktops to edit**|Indicates all desktops that will be updated with the form data. If there are many desktops, you can scroll through the list to select them.|
|**Desktops viewers**|If checked, a new section will appear. The desktop viewers will update as selected. ![Update viewers section](./domains.images/domains42.png){width="60%"}|
|**Update RDP credentials**|If checked, the username and password used for RDP sessions will be changed. ![(Update RDP credentials)](./domains.images/domains43.png){width="60%"}|
|**Hardware**|Data will only be changed in fields where the selected value is different than "--"|
|**Update network**|If checked, a new section will appear. The selected desktop interfaces will be updated, using the CTRL key while pressing options to select more than one interface at a time.  ![(Update network)](./domains.images/domains44.png){width="60%"}|
|**Bookable resources**|Bookable resources will be updated when the selected value is different than "--"|

To apply all changes and update the selected desktops, press the ![(Modify desktops)](./domains.images/domains45.png){width="10%"} button

## Bulk Add Desktops

If you press on ![Bulk Add Desktops](./domains.images/domains56.png) a modal will appear

![(Add Multiple Desktops modal)](./domains.images/domains57.png)

This form allows you to create multiple desktops simultaneously. To create them, simply fill out the form by specifying a name for the new desktops and selecting the template that they will be based on.

In the **"Allowed"** section, you can select the users you wish to create the desktops for by choosing from roles, categories, groups or usernames. A separate desktop will be created for each user selected.


