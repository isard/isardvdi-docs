# Introducción

!!! info "Roles con acceso"
    Solo los **administradores** tienen acceso a todas las categorías dentro de la plataforma. El **gestor** está limitado a su categoría.

Esta sección muestra todos los medios creados por los usuarios dentro de una o más categorías.

![](./domains.images/media1.png){width="80%"}

1. Media: En este apartado se muestran todos los medios subidos a la plataforma.

2. Media in other status: Medios que tienen otro tipo de estado, como pueden ser los borrados, los que se han decargado mal o se han quedado en mantenimiento.


## Media

* **Show last task info** ![](./domains.images/media2.png) : Verifica si se está ejecutando la tarea de "Check media".

* **Check media status** ![](./domains.images/media3.png) : Verifica si el medio existe y si la ISO está físicamente en el disco. Si no existe, se eliminará automáticamente.

* **Create desktop from media** ![](./domains.images/media4.png) : Crea un escritorio virtual a partir de este medio.

* **Changed allowed users** ![](./domains.images/media5.png) : Cambia los permisos de compartición del medio.

* **Change owners** ![](./domains.images/media6.png) : Cambia de propietario el medio.

* **Delete media** ![](./domains.images/media7.png) : Borra el medio.


## Media in other status

* **Deleted**: Medios que ya han sido eliminados de la plataforma.
* **Download Aborting**: Medios cuya descarga ha sido abortada, por ejemplo, cuando se cancela una descarga en proceso.
* **Download Failed**: Medios con descargas fallidas.
* **Download Failed Invalid Format**: Medios cuya descarga ha fallado debido a un formato inválido de la ISO.
* **Reset Downloading**: Medios en los que la descarga se ha reiniciado.

