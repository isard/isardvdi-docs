# Bookables

## Priority

In this section you can create priorities depending on what is needed at that time. Here you can create rules with a certain maximum reservation time, number of desks, etc.

![](./domains.images/domains26.png)

To add a new priority, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

And a dialog box will appear where you can fill in the fields, also allows you to share with other roles, categories, groups or users:

* **Name**: The name that you want to give to the priority

* **Description**: A short description to know a little more information about that priority

* **Rule**: It is the rule that we assign to that priority, this is applied in the "Resources" section. It is important to note that if you want to apply more than one rule to a priority, you need to name them the same, **"default"**.

* **Priority**: It is the priority given to the rule; the higher the number, the more priority it will have and the lower the number, the less it will have.

* **Forbid time**: It is the time in advance that reservations cannot be made.

* **Max time**: It is the maximum time of a reservation.

* **Max items**: These are the maximum number of desks allowed to have this rule.

![](./domains.images/domains28.png)


### Share

If you want to share a priority with a role, category, group or user, click on the icon ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

And fill out the form

![](./domains.images/domains31.png)


### Edit

To edit a priority, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains34.png)


## Resources

In this section you can find all the GPU profiles that have been selected in the "Hypervisors" section

Here you can apply the priorities/rules that have been previously created.

![](./domains.images/domains35.png)


### Share

To share a resource, click the icon![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

And fill out the form

![](./domains.images/domains37.png)


### Edit

To edit a resource, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains39.png)


## Events 

In this section you can see the events that are generated derived from the actions of the gpus.
