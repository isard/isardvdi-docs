# Manage users

To manage users you have to go to the [Administration panel](./index.md)

Press the button ![](./manage_user.images/manage_user24.png){width="10%"}

![](./manage_user.images/manage_user25.png){width="80%"}

## Group Creation

To create groups you have to go to the **"Management"** subsection under the **"User"** section. Then look for the **"Groups"** table and press the button ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png){width="80%"}

And fill the fields in the form.

![](./manage_user.images/manage_user3.png){width="60%"}

!!! Info "Important notes about the form fields"
    * **Linked groups**: All the resources that have been shared with its linked groups will be automatically inherited by the created group. For instance, if a Group A is created with Group B as a linked group, all the resources shared with Group B will also be shared with Group A.

    * **Ephimeral desktops**: Will set a limited time of use to a temporary desktop. (For this you also have to configure, being Admin, in Config the "Job Scheduler")

## Group Edition

A group's parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the group we want to update, and then pressing ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png){width="80%"}

A dialog box will pop up with the same parameters as the creation form.

![](./manage_user.images/manage_user29.png){width="60%"}

## User Creation

To create users you have to go to the **"Management"** subsection under the **"User"** section.

### Individually

Look for the **"Users"** table and press the button ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png){width="80%"}

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user6.png){width="60%"}

And fill the fields in the form

!!! Info "Important notes about the form fields"
    **Secondary groups**: All the resources that have been shared with its secondary groups will be automatically inherited by the created user. In addition, the user will be added to the deployments created in any of its secondary groups.

### Bulk Creation

Look for the **"Users"** table and press the button ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png){width="80%"}

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user9.png){width="60%"}

An example file can be downloaded by clicking the button ![](./manage_user.images/manage_user10.png){width="20%"}. A form will appear to fill out:

!!! Warning "Avoiding errors"
    * The **category** and **group** fields must **exactly** match its name 
    * The recommended encoding is **Unicode (UTF-8)**
    * The csv must be separated by **tabulations** <kbd>Tab ↹</kbd>
    * It's highly recommended to use the given sample csv  

![](./manage_user.images/manage_user11.png)

Once filled out, it can be uploaded on

![](./manage_user.images/manage_user12.png)

And if it has been uploaded correctly, a preview table of the users will appear:

![](./manage_user.images/manage_user28.png){width="60%"}

To export the users with the generated password into a CSV, press the button ![](./manage_user.images/manage_user63.png)

!!! Failure "Errors"
    If the csv has not been uploaded correctly, an error will be shown indicating the reason.

### Generate CSV to create existing users

To generate a CSV with existing users to create them again, select the users and press the button ![](./manage_user.images/manage_user64.png). 

![](./manage_user.images/manage_user65.png){width="80%"}

You can then import the CSV file to create the users again. The CSV file will not contain the passwords of the users.

If you want to update the users, you should use the [**CSV for update**](#user-update) button.

## User Edition

To edit users you have to go to the **"Management"** subsection under the **"User"** section.

### Individually

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to update, and then pressing ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png){width="80%"}

A dialog box will pop up with the same parameters as the creation one.

![](./manage_user.images/manage_user35.png){width="60%"}

!!! Info "Important notes about the form fields"
    The users **username**, **group** and **category** can't be modified.

### Bulk Edition

Select one or more users and press the button ![](./manage_user.images/manage_user45.png){width="10%"}

[](./manage_user.images/manage_user46.png){width="80%"}

A dialog box will appear with the following form: 

![](./manage_user.images/manage_user47.png){width="45%"}

!!! note "Fields"
    === "Update active/inactive"
        ![](./manage_user.images/manage_user48.png){width="20%"}  
            - **Update active/inactive**: The user's status on the platform is updated. This user can be **active**, has access to the platform, or **inactive**, does not have access to the platform.  
    === "Update secondary group"
        ![](./manage_user.images/manage_user49.png){width="50%"}        
            - **Update secondary group**: Secondary groups of multiple users can be updated at the same time. To do this, write the name of the group and select one of these options:    
                - **Overwrite**: Overwrites the secondary group that was assigned to it.  
                - **Add**: The secondary group written is added to the user.  
                - **Delete**: Deletes the secondary group that the user was assigned to.

### User update

To update users using a .csv file, you must first download the file with the user information. To do this, press the button ![](./manage_user.images/manage_user53.png){width="15%"}

![](./manage_user.images/manage_user54.png){width="80%"}

Then press the button ![](./manage_user.images/manage_user50.png){width="15%"}

![](./manage_user.images/manage_user51.png){width="80%"}

A dialog window will appear with the following form where you can upload the file or download an example:

![](./manage_user.images/manage_user52.png){width="80%"}

!!! Warning "Avoiding mistakes"
    * The "**username**", "**category**" and "**group**" fields have to match **exactly** with their name to update the user
    * **Unicode (UTF-8)** encoding is recommended
    * The csv file must be separated by **tabulations** <kbd>Tab ↹</kbd>

!!! Info "Important notes regarding fields"
    * The fields "**provider**", "**category**", "**uid**", and "**group**" cannot be modified. The rest of the fields will be updated.
    * If you leave a box **blank**, it will not be updated.
    * The "Active" column will have to have a value **"true"** or **"false"**
    * To add more than one **secondary group** to users, you have to separate the groups in the "secondary_groups" column with **"/"**, example "prova/Test"

If you want to generate a CSV to create the users again, you should use the [**CSV for create**](#generate-csv-to-create-existing-users) button.

### Enable/Disable

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to disable/enable, then press ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png){width="80%"}

The users status can be seen in the users table.

![](./manage_user.images/manage_user38.png){width="80%"}

!!! Danger "Be careful"
    If the user is disabled while logged in it could provoke session errors.

### Change Password

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to change its password, then press ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png){width="80%"}

A dialog box will pop up with the following form:

![](./manage_user.images/manage_user40.png){width="45%"}

!!! Tip
    Users can change its passwords through their [profile](../user/profile.md).

### Migrate user

All items can be **migrated from local accounts to Google accounts (vice versa o any other type of account)**  one user to another (as long as they have the same role or lower) by clicking the icon ![](./manage_user.images/manage_user13.png) next to the user you want to migrate, then click ![](./manage_user.images/migrate2.png).

![](./manage_user.images/migrate1.png){width="80%"}

A dialog box will open showing if the user has any items created (desktop, template, deployment or media)

![](./manage_user.images/migrate3.png){width="70%"}

Along with a series of warnings when migrating these items from one user to another.

!!! Warning "Warning"
    - Any desktops that are started will be shut off.
    - The user's recycle bin will be emptied.
    - The user's reservations will be removed.
    - Non-persistent desktops will be DELETED.
    - The co-owner listing in deployments will be cleared.
    - Desktops owned by other users in migrated deployments will remain intact.
    - Resources not allowed for the new user will be removed or restricted.

After reading the warnings, we can search for the user, whose items we want to migrate by name and clicking the "Migrate user" button.

![](./manage_user.images/migrate4.png){width="80%"}

### Impersonate User

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to impersonate,and press ![](./manage_user.images/manage_user44.png).

![](./manage_user.images/manage_user41.png){width="80%"}

!!! Danger "Disclaimer"
    Impersonating a user **provides access to all its data and desktops and it comes with inherent risks**. Before proceeding, it is important to **consider the sensitivity of the information that will be accessed**.

## Enrollment keys

In the **"Groups"** table search for the group that you want to obtain the code. Press the button ![](./manage_user.images/manage_user13.png) to view the group details

![](./manage_user.images/manage_user14.png)

The group is displayed with some options. Press the button ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

A dialog box will appear where you can generate codes by clicking the different checkboxes.

![](./manage_user.images/manage_user32.png)

Once the enrollment key code is generated, it can be copied and shared with users.

![](./manage_user.images/manage_user17.png)

!!! Danger "Be careful"
    * When signing up, **the users will be given the role of the shared enrollment key code**. For instance, Teachers, will be given the "Advanced" code and students the "Users" code.
    * An unlimited amount of users can sign up using the given enrollment key code. Thus, it's recommended to deactivate them once used.

## Create categories

!!! warning "Roles with access"

    Only **administrators** have access to this feature.

In the **"Users"** section, look where it says **"Categories"** and press the button ![](./users.images/users3.png)

![](./users.images/users4.png){width="80%"}

A dialog box will appear where the form fields can be filled.

![](./users.images/users5.png){width="80%"}

There are three options that can be applied when creating a category:

* **Automatic recycle bin delete**: Recycle Bin functionality will be applied to users. You can select whether you want desktops to be deleted after one hour or immediately. This time can be modified here.

![](./users.images/users7.png){width="40%"}

* **Frontend dropdown show**: En la página de inicio de sesión, aparecerá la categoría creada en el menú desplegable.

![](./users.images/users6.png){width="40%"}

!!! info
    If the category is hidden it is still possible to login in that category using the url `/login/<category_id>`

* **Ephemeral desktops**: Para poder poner un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar en la sección de "Config" el "Job Scheduler")

![](./users.images/users8.png){width="40%"}


## Access via OAuth

!!! warning "Roles with access"

    Only **administrators** have access to this feature.

To be able to access through Google from an external account, you have to go to the **"Users"** section and the **"Categories"** section, press the icon ![](./users.images/users9.png) of the category to which you want to give access.

![](./users.images/users10.png){width="80%"}

Press the button ![](./users.images/users11.png)

![](./users.images/users12.png){width="80%"}

And a dialog window with a form appears. Fill in the "Allowed email domain for foreign account like Google" field with the email domain.

![](./users.images/users13.png){width="45%"}



