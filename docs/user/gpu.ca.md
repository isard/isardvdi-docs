# vGPU

IsardVDI permet treballar amb la tecnologia de **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** que permet disposar de recursos d'una targeta gràfica associats a un escriptori virtual. Aquesta tecnologia ens permet executar programari que requereix **recursos de GPU dedicats**, com ara programes de disseny 3D, animació, edició de vídeo, CAD o disseny industrial.

Cada targeta gràfica es pot dividir en diferents perfils amb una quantitat de memòria reservada. En funció del perfil seleccionat, es disposa de més o menys targetes virtuals. En la següent imatge es mostra un servidor amb una sèrie de targetes GPU instal·lades. Cada targeta es pot dividir en vGPUs (virtual GPUs) que es poden mapejar a cada màquina virtual.

![](./gpu.images/gpu1.png){width=60%}

Veurem amb un exemple el funcionament. Es disposa d'una targeta **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** i es vol utilitzar un programa com el SolidWorks per dissenyar peces 3D. S'utilitza un perfil amb 2 GB de memòria gràfica dedicada. Aquesta targeta gràfica disposa de 48 GB de memòria, que es pot dividir en perfils "2Q": el primer caràcter "2" fa referència a la quantitat de memòria reservada (2 GB) i el segon caràcter "Q" fa referència a la llicència vWS, el mode en què la targeta virtual es configura per a treballs creatius i tècnics que utilitzen la tecnologia **[Quadro de Nvidia](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)**. 

Els diferents perfils i modes de funcionament disponibles per a aquesta targeta estan accessibles al [manual de Nvidia amb els perfils per a l'A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). En el cas de 2Q tenim:

- Memòria dedicada i reservada: 2 GB
- Nombre màxim de vGPUs per GPU: 24

Diagrames de funcionament de la tecnologia Nvidia vGPU:

![](./gpu.images/gpu2.png){width=50%}

![](./gpu.images/gpu3.png){width=60%}

Per a més informació sobre l'ús de la tecnologia GPU sobre escriptoris en IsardVDI, consultar l'[apartat de reserves i perfils](../../user/bookings.ca.md).

## Tipus de perfils

La quantitat de GPU físiques que té una placa depèn de la placa. Cada GPU física pot admetre diversos tipus diferents de GPU virtual (vGPU). Els tipus de vGPU tenen una quantitat fixa de memòria intermèdia de fotogrames, una quantitat de capçals de pantalla compatibles i resolucions màximes. S'agrupen en diferents perfils segons les diferents classes de càrrega de treball per a les que estan optimitzats. Cada perfil s'identifica amb la darrera lletra del nom del tipus de vGPU. Per a [més informació](https://docs.nvidia.com/vgpu/10.0/grid-vgpu-user-guide/index.html).

|Perfils||Càrrega de treball òptima|
|----||----|
|Q||Per a professionals creatius i tècnics que requereixen el rendiment i les característiques de la tecnologia Quadro.|
|C||Càrregues de treball de servidors amb ús intensiu de computació, com ara intel·ligència artificial (IA), aprenentatge profund o computació d'alt rendiment|
|B||Per a professionals de negocis i treballadors del coneixement|
|A||Streaming d'aplicacions o solucions basades en sessions per a usuaris d'aplicacions virtuals|

## Com preparar un escriptori/plantilla

### Instal·lació de controladors

En aquesta guia s'explica pas a pas el procés d'instal·lació dels controladors NVIDIA com a client (guest) per a un escriptori virtual a la plataforma IsardVDI connectat a un servidor amb targetes GPU NVIDIA.

#### Requisits en aquesta guia

- Servidor IsardVDI amb els **controladors de NVIDIA d'amfitrió** prèviament instal·lats (host) 
- Escriptori virtual amb sistema operatiu **Windows 10/11**
- Arxiu executable instal·lador dels controladors de NVIDIA
- Memòria USB

#### Passos previs    

Cal esbrinar la **versió del controlador NVIDIA amfitrió del servidor** per poder instal·lar la **mateix versió** a l'escriptori virtual perquè hi hagi una compatibilitat entre ambdues màquines. 

!!! note "L'administrador del servidor d'IsardVDI ha de proporcionar la informació de la versió del controlador de NVIDIA"

El servidor té com a sistema operatiu una distribució Linux, així que a la terminal s'escriu l'ordre ```nvidia-smi -q|grep -i "Driver Version"``` i es recull la primera fila de la resposta de l'ordre on es reflecteix la versió dels controladors:

```
root@servidor:/home/isard# nvidia-smi -q|grep -i "Driver Version"
Wed Jan 4 08:51:17 2023
+------------------------------------------------- ----------------------------+
| NVIDIA-SMI 510.47.03 Driver Version: 510.47.03 CUDA Version: N/A |
|-------------------------------+----------------- -----+----------------------+
```

L'equivalent en sistema operatiu **Windows** a la versió de controladors **510.467.03** és la **511.65**, com mostra aquesta taula de comparacions del web oficial de NVIDIA:

![](./gpu.images/gpu4.png){width=80%}

> Si el servidor on apunta l'escriptori virtual a configurar té alguna d'aquestes targetes gràfiques, els controladors amfitrió hauran d'estar actualitzats a aquestes versions mínim. Sinó, poden existir problemes de compatibilitat i/o en el procés d'instal·lació dels controladors client a l'escriptori:

> ![](./gpu.images/gpu5.png)

#### Escriptori

##### Paràmetres bàsics

Tenir un Windows o un Linux com a sistema operatiu, sobre una plantilla base amb aquest sistema preinstal·lat i preconfigurat, sense cap controlador de pantalla ni de targeta gràfica instal·lats. **Si hi ha controladors instal·lats, s'han de desinstal·lar per deixar preparat el sistema i poder realitzar els passos següents, sinó, no funcionarà.**

![](./gpu.images/gpu40.png){width=80%}

Es **edita l'escriptori** amb els paràmetres de la imatge següent, que es modificaran posteriorment (no tenen rellevància els paràmetres vCPUs ni Memory (GB)):

![](./gpu.images/gpu52.png)

##### Instal·lació 

Com s'explica anteriorment, per a aquesta pràctica cal tenir el fitxer instal·lador de controladors client corresponent al sistema operatiu de l'escriptori virtual.

Es **descarreguen** a l'equip local, mitjançant el llistat de descàrrega de programari disponible al [panell web de NVIDIA Enterprise](https://nvid.nvidia.com/affwebservices/public/saml2sso?SPID=https:/ /api.licensing.nvidia.com), o mitjançant aquesta [una altra font de Google](https://cloud.google.com/compute/docs/gpus/grid-drivers-table#windows_drivers) on es pot recórrer a la descarrega només el fitxer *.exe* o *.run* per a Windows o Linux. 
    
Després s'introdueix a la **memòria USB** per així connectar-los a l'escriptori virtual mitjançant el **visor SPICE**.

![](./gpu.images/gpu9.png)

Com que és un escriptori amb un **perfil GPU assignat** però sense reservar (es pot fer però no és necessari) s'accedeix a l'escriptori mitjançant el **panell d'Administració** d'Isard:

![](./gpu.images/gpu10.png){width=50%}
![](./gpu.images/gpu11.png){width=40%}
![](./gpu.images/gpu12.png){width=50%}

I es **copia el fitxer** a la carpeta **admin - instal·ladors** del **Windows** de l'escriptori:

![](./gpu.images/gpu13.png)

Es **executa** el fitxer ia la nova finestra d'instal·lació es marquen els passos següents:

![](./gpu.images/gpu14.png){width=45%}
![](./gpu.images/gpu15.png){width=45%}
![](./gpu.images/gpu16.png){width=45%}
![](./gpu.images/gpu17.png){width=45%}
![](./gpu.images/gpu18.png){width=45%}
![](./gpu.images/gpu19.png){width=45%}

!!! warning "**NO PULSAR REINICIAR ARA.** Es marca **Reiniciar més tard** i, molt important, es **apaga manualment** l'escriptori."

##### Paràmetres inicials

Sense tocar l'escriptori i sense tornar a arrencar-lo, es edita des d'IsardVDI per canviar els paràmetres i que quedi com a continuació a la imatge. Molt important desactivar els visors anteriors i deixar només **RDP (Client) i RDP (Browser)**; el paràmetre **Video** ha de tenir l'opció **Only GPU** seleccionada:

![](./gpu.images/gpu51.png)

##### Prova definitiva

A continuació s'**inicia de nou** l'escriptori i s'hi accedeix mitjançant qualsevol **visor RDP**. 

Seguidament cal comprovar que el **Panel de Control de NVIDIA** apareix a la **barra d'eines** de Windows a la cantonada inferior dreta, cosa que certificarà la correcta instal·lació dels controladors i la compatibilitat amb l'equip, **això pot portar uns minuts fins que Windows arrenqui tots els serveis**:

![](./gpu.images/gpu22.png){width=35%}
![](./gpu.images/gpu23.png){width=35%}
![](./gpu.images/gpu24.png){width=60%}

#### Errors d'instal·lació

??? question "No s'obre el tauler de control de NVIDIA / no es mostra la icona que estigui NVIDIA iniciat"

    De **no mostrar-se** i/o **no poder obrir-se** el panell, cal **desinstal·lar els 3 programes** mitjançant **Panel de Control - Programes - Desinstal·la un programa**:

    ![](./gpu.images/gpu25.png){width=45%}
    ![](./gpu.images/gpu26.png){width=45%}
    ![](./gpu.images/gpu27.png){width=45%}

    L'equip **s'apaga manualment**, s'inicia i es torna a realitzar la [instal·lació dels controladors](./gpu.ca.md/#installació-de-controladors). 

    **No s'edita l'escriptori** per posar-vos els [paràmetres inicials](./gpu.ca.md/#paràmetres-inicials); després de desinstal·lar els paquets i apagar l'escriptori, es torna a accedir-hi mitjançant **visor RDP** i amb el paràmetre **Only GPU** per a **Video**.

    ![](./gpu.images/gpu22.png){width=35%}
    ![](./gpu.images/gpu23.png){width=35%}
    ![](./gpu.images/gpu24.png){width=60%}

    Repetir la desinstal·lació i instal·lació dels paquets fins que aparegui la icona del **Panel de control de NVIDIA** i es pugui obrir.

??? question "No detecta el controlador de NVIDIA"

    Assegurar-se que el controlador del servidor i que s'instal·la a l'escriptori són compatibles, han d'anar alhora perquè sinó no funciona.

    Ex: si el servidor té la versió 15.2 a l'escriptori s'ha d'instal·lar qualsevol versió 15.X (preferiblement 15.2)

### Gestió del token

Per afegir el token de llicència de NVIDIA, primer necessitem descarregar-lo des del [portal de NVIDIA](https://nvid.nvidia.com/login).

Un cop tinguem el token, l'hem d'introduir a l'escriptori virtual. Això es pot fer descarregant el fitxer des d'un servei d'emmagatzematge al núvol, des del portal de NVIDIA, o mitjançant un dispositiu USB (ja sigui a través del [visor Spice o visor RDP]((../user/viewers/viewers.ca.md)))

- **Windows**

El token s'ha de col·locar a la carpeta ubicada a la següent ruta:

```C:\Program Files\NVIDIA Corporation\vGPU Licensing\ClientConfigToken```

Per verificar que el token funcioni correctament, s'obre un ```cmd``` i s'escriu ```nvidia-smi -q```

On s'ha de desplaçar fins a trobar el següent apartat. Si el **License Status** mostra **Licensed** amb una data de venciment, això confirmarà que la llicència del client del Windows s'ha aplicat correctament.

Si s'ha llicenciat bé, sortirà això:

![](./gpu.images/gpu28.png){width=60%}

Si no s'ha llicenciat bé, surt això:

![](./gpu.images/gpu29.png){width=60%}

- **Linux**

S'ha de moure el token al directori ``/etc/nvidia/ClientConfigToken``, i es canvien els permisos:

```
sudo mv client_configuration_token_*.tok /etc/nvidia/ClientConfigToken
sudo chmod 744 /etc/nvidia/ClientConfigToken/client_configuration_token_*.tok
```

Si no existeix el fitxer **gridd.conf** es crea a partir del fitxer **gridd.conf.template** (no s'ha de moficiar res del fitxer):

```
sudo cp gridd.conf.template gridd.conf
```

S'inicia ia la terminal s'escriu l'ordre

```
nvidia-smi -q
```

On s'ha de desplaçar fins a trobar el següent apartat. Si el **License Status** mostra **Licensed** amb una data de venciment, això confirmarà que la llicència del client de Linux s'ha aplicat correctament.

![](./gpu.images/gpu30.png){width=60%}

#### Errors de token

??? question "No llegeix el token de llicència"

    Assegureu-vos que no falli la sincronització de la data/hora de l'escriptori.
    Si l'hora no està ben sincronitzada, doncs falla. Cal reiniciar el servidor o reiniciar un cop ha canviat l'hora.

    ```
    cff15b928d2e   registry.gitlab.com/isard/isardvdi/hypervisor:v12-18-14   "/src/start.sh"          3 weeks ago
    ```

    L'error cal mirar-lo al log de NVIDIA. Quan arrenca el Windows agafa l'hora UTC, s'actualitza i ja l'enganxa bé.

    '''
    Windows Licensing Event Logs

    Al Windows, licensing events s'allotja al plain-text file %SystemDrive%\Users\Public\Documents\NvidiaLogging\Log.NVDisplay.Container.exe.log.

    The log file is rotated when its size reaches 16 MB. A new log file is created and the old log file is renamed to Log.NVDisplay.Container.exe.log1. Each time the log file is rotated, the number in the file name of each existing old log file is increased by 1. The oldest log file is deleted when the number of log files exceeds 16.
    '''

    Aquest és el [manual d'usuari de llicència grid](https://docs.nvidia.com/grid/17.0/grid-licensing-user-guide/index.html#licensing-event-logs) on explica els logs.

### Crear/editar escriptori/desplegament

!!! warning "Per poder reservar i iniciar un escriptori amb un perfil de gpu, l'administrador ho ha d'haver planificat prèviament"

[Crear](create_desktop.es.md)/[editar escriptori](edit_desktop.es.md) o [crear/editar un desplieuge](../advanced/deployments.es.md) amb els punts anteriors fets, desplaçar-se a l'apartat de **Visors** i assegurar-se de marcar les caselles amb els visors **RDP**. 

![](./gpu.images/gpu32.png)

!!! info "És important saber que només funciona amb els visors RDP"

    **IMPORTANT!**: Si hi ha un problema amb el sistema operatiu i mai arribem a tenir adreça IP, no podrem accedir per RDP. Sempre cal evitar això, però, amb més raó en aquest apartat, forçar l'apagat de l'escriptori des de la interfície d'IsardVDI pot provocar una corrupció del disc (ja que és l'equivalent a treure el cable de corrent d'un ordinador encès) i que aquest no aconsegueixi iniciar el sistema operatiu i obtenir adreça IP, de manera que ens podrem quedar sense accés a l'escriptori.

També cal canviar algunes configuracions a l'apartat **Hardware**.

És important que la configuració **Videos** tingui l'opció **Only GPU** marcada. **Als escriptoris amb S.O. Windows amb els controladors de NVIDIA instal·lats, no és compatible tenir alhora una targeta de vídeo QXL i una targeta vGPU.** 

Al següent apartat **Reservables** més avall, s'assigna el **perfil** de GPU disponible.

!!! example ""
    Els **Windows** només podran utilitzar els **visors RDP**, però els **Linux** permeten utilitzar els **visors Spice/RDP** i el **vídeo** a **Default**.

![](./gpu.images/gpu51.png)

### Start now o reserva

Hi ha dues maneres d'iniciar l'escriptori amb GPU: la primera és utilitzar la funció "start now" o "arrencada al moment", que arrenca l'escriptori immediatament si hi ha perfils de GPU disponibles. La segona opció és mitjançant una reserva, que requereix reservar l'escriptori amb una antelació definida per l'administrador d'IsardVDI.

#### Start now

Per iniciar l'escriptori al moment, podeu veure que ens indica que "L'escriptori amb vGPU no està reservat". Per crear la reserva de l'escriptori, premeu el botó "iniciar". Sortirà una finestra de diàleg amb opcions d'hores:

![](./gpu.images/gpu45.png){width=45%}
![](./gpu.images/gpu46.png){width=60%}

##### Targeta gràfica disponible

Si la targeta gràfica està disponible, l'escriptori s'iniciarà sense problemes i mostrarà la data i l'hora de finalització de la reserva. Un cop arribada aquesta data/hora, l'escriptori s'apagarà i la reserva s'eliminarà automàticament.

![](./gpu.images/gpu42.png){width=50%}


##### Targeta gràfica no disponible

Si la targeta gràfica no està disponible, apareixerà un modal amb dues opcions: canviar la targeta gràfica per una que estigui disponible o reservar l'escriptori amb la targeta gràfica actual per a quan estigui disponible.

![](./gpu.images/gpu38.png)

#### Reserva

Per reservar un escriptori o desplegament, consulta aquesta secció del manual: [Reserves](../user/bookings.ca.md)

