# VPN 

## ¿Para qué sirve la conexión VPN en IsardVDI?

Se establece el **túnel VPN** de usuario en IsardVDI para que el usuario pueda **acceder a sus escritorios mediante el [visor RDP VPN](../viewers/viewers.es/#como-usar-los-visores-rdp-y-rdp-vpn)** o simplemente para poder **establecer comunicación entre la máquina anfitriona y los escritorios del usuario** (ej. acceder a ellos mediante una conexión SSH). 


## Cómo establecer el túnel VPN de usuario

En primer lugar, la interfaz de red **Wireguard VPN** debe estar activa en todos los escritorios que se quieran visibles en el túnel VPN, ya sea [editando su "Hardware"](../edit_desktop.es/#editar-escritorio) o [creándolos directamente con esa interfaz de red](../create_desktop.es/#vista-principal).

![](vpn.es.images/1.png){width="35%"}

En segundo lugar, se accede al menú desplegable del usuario y se selecciona la opción **VPN**, que descargará un fichero ***.conf*** en el equipo anfitrión.

![](vpn.es.images/2.png){width="70%"}

Para establecer la conexión se hará uso del programa **WireGuard**. Dependiendo del sistema operativo del equipo anfitrión, se descarga, instala y utiliza el programa, como se explica a continuación.

### Con anfitrión Linux

Después de descargar el fichero ***.conf*** de VPN, mediante la terminal, se ejecutan los siguientes comandos:

```
$ sudo apt install wireguard
$ sudo mv /ruta/de/descarga/isard-vpn.conf /etc/wireguard
$ wg-quick up isard-vpn
```

El túnel VPN del usuario ya está establecido correctamente. Para probar que funciona, se puede **arrancar los escritorios** del usuario y hacer **ping** desde el anfitrión hacia la dirección IP de éstos:

![](vpn.es.images/3.png){width="30%"}

```
root@debian:~# ping 10.2.91.193
PING 10.2.91.193 (10.2.91.193) 56(84) bytes of data.
64 bytes from 10.2.91.193: icmp_seq=1 ttl=63 time=13.6 ms
64 bytes from 10.2.91.193: icmp_seq=2 ttl=63 time=6.15 ms
64 bytes from 10.2.91.193: icmp_seq=3 ttl=63 time=6.46 ms
64 bytes from 10.2.91.193: icmp_seq=4 ttl=63 time=6.89 ms
^C
--- 10.2.91.193 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 6.146/8.275/13.601/3.085 ms
```

### Con anfitrión Windows

Se descarga **[WireGuard para Windows mediante este enlace](https://download.wireguard.com/windows-client/wireguard-installer.exe)**, se abre el programa y se importa el archivo ***.conf*** de VPN recién descargado.

![](vpn.es.images/4.png){width="60%"}

![](vpn.es.images/5.png){width="60%"}

![](vpn.es.images/6.png){width="60%"}

El túnel VPN del usuario ya está establecido correctamente. Para probar que funciona, se puede **arrancar los escritorios** del usuario y hacer **ping** desde el anfitrión hacia la dirección IP de éstos:

![](vpn.es.images/3.png){width="30%"}

```
C:\Users\windows>ping 10.2.91.193

Haciendo ping a 10.2.91.193 con 32 bytes de datos:
Respuesta desde 10.2.91.193: bytes=32 tiempo=13ms TTL=116
Respuesta desde 10.2.91.193: bytes=32 tiempo=14ms TTL=116
Respuesta desde 10.2.91.193: bytes=32 tiempo=14ms TTL=116
Respuesta desde 10.2.91.193: bytes=32 tiempo=13ms TTL=116

Estadísticas de ping para 10.2.91.193:
    Paquetes: enviados = 4, recibidos = 4, perdidos = 0
    (0% perdidos),
Tiempos aproximados de ida y vuelta en milisegundos:
    Mínimo = 13ms, Máximo = 14ms, Media = 13ms

```

### Con anfitrión Mac OS

Se descarga el programa [Wireguard](https://www.wireguard.com/install/) desde la AppStore y luego el archivo isard-vpn.conf desde la plataforma. Se abre el archivo isard-vpn.conf con el editor de textos.

![](./vpn.es.images/vpn_mac1.png){width="70%"}

Se tiene que eliminar la línea ```PostUp = : ``` del archivo y guardar. Se va a la pestaña del Wireguard y se selecciona "Importar túnel desde archivo

![](./vpn.es.images/vpn_mac2.png){width="30%"}

Seleccionamos el archivo modificado y lo subimos

![](./vpn.es.images/vpn_mac3.png){width="70%"}

