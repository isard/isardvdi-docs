# vGPU

IsardVDI allows working with **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** technology, which allows you to have resources from a graphics card associated with a virtual desktop. This technology enables us to run software that requires **dedicated GPU resources**, such as 3D design programs, animation, video editing, CAD, or industrial design.

Each graphics card can be divided into different profiles with a certain amount of reserved memory. Depending on the selected profile, you will have more or fewer virtual cards. The following image shows a server with a series of GPU cards installed. Each card can be divided into vGPUs (virtual GPUs) that can be mapped to each virtual machine.

![](./gpu.images/gpu1.png){width=60%}

Let's see how it works with an example. You have an **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** card and want to use a program like SolidWorks to design 3D pieces. You will use a profile with 2GB of dedicated graphics memory. This graphics card has 48GB of memory, which can be divided into "2Q" profiles: the first character "2" refers to the amount of reserved memory (2GB), and the second character "Q" refers to the vWS licensing mode, the way in which the virtual card is configured for creative and technical work that uses **[Nvidia Quadro](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)** technology. The different profiles and operating modes available for this card are accessible in the [Nvidia manual with profiles for the A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). In the case of 2Q, you have:

- Dedicated and reserved memory: 2GB
- Maximum number of vGPUs per GPU: 24

Functioning diagrams of Nvidia vGPU technology:

![](./gpu.images/gpu2.png){width=50%}

![](./gpu.images/gpu3.png){width=60%}

For more information on using GPU technology on desktops in IsardVDI, please consult the [section on bookings and profiles](../user/bookings.md).

## Type of profiles

The number of physical GPUs a board has depends on the board. Each physical GPU can support several different types of virtual GPU (vGPU). vGPU types have a fixed number of frame buffers, a number of supported display heads, and maximum resolutions. They are grouped into different profiles based on the different workload classes they are optimized for. Each profile is identified by the last letter of the vGPU type name. For [more information](https://docs.nvidia.com/vgpu/10.0/grid-vgpu-user-guide/index.html).

|Profiles||Optimal Workload|
|----||----|
|Q||For creative and technical professionals who require the performance and features of Quadro technology.|
|C||Compute-intensive server workloads, such as artificial intelligence (AI), deep learning, or high-performance computing|
|B||For business professionals and knowledge workers|
|A||Application streaming or session-based solutions for virtual application users|

## How to prepare a desktop/template

### Driver Installation

This guide explains step by step the process of installing **NVIDIA drivers as a client** for a virtual desktop on the IsardVDI platform** connected to a **server with NVIDIA GPU cards**.

#### Requirements in this guide

- IsardVDI server with **host NVIDIA drivers** pre-installed (host) 
- Virtual desktop with **Windows 10/11** operating system
- NVIDIA drivers installer executable file
- USB memory

#### Previous steps    

The **version of the server's host NVIDIA driver** must be found out in order to install the **same version** on the virtual desktop so that there is compatibility between both machines. 

!!! note "The IsardVDI server administrator needs to provide the NVIDIA driver version information"

The server has a Linux distribution as its operating system, so in the terminal the command ```nvidia-smi -q|grep -i "Driver Version"``` is typed and the first row of the command response is collected where The version of the drivers is reflected:

```
root@server:/home/isard# nvidia-smi -q|grep -i "Driver Version"
Wed Jan 4 08:51:17 2023
+---------------------------------------------------------------- ----------------------------+
| NVIDIA-SMI 510.47.03 Driver Version: 510.47.03 CUDA Version: N/A |
|----------------------------------+----------------- -++----------------------+
```

The **Windows** operating system equivalent to driver version **510.467.03** is **511.65**, as shown in this comparison table from the official NVIDIA website:

![](./gpu.images/gpu4.png){width=80%}

> If the server pointed where the virtual desktop to be configured has any of these graphics cards, the host drivers must be updated to these minimum versions. Otherwise, there may be compatibility problems and/or in the installation process of the client drivers on the desktop:

> ![](./gpu.images/gpu5.png)

#### Desktop

##### Basic parameters

Have a Windows or Linux operating system, on a base template with said system pre-installed and pre-configured, **without** any screen or graphics card drivers installed. **If there are drivers installed, they must be uninstalled to prepare the system and be able to carry out the following steps, otherwise it will not work.**

![](./gpu.images/gpu41.png){width=80%}

Edit the desktop with the parameters of the following image, which will be modified later (the vCPUs and Memory (GB) parameters are not relevant):

![](./gpu.images/gpu54.png)

##### Installation 

As explained above, for this practice you need to have the **installer file** of client drivers corresponding to the virtual desktop operating system.

They are **downloaded** to the local computer, using the software download list available on the [NVIDIA Enterprise web panel](https://nvid.nvidia.com/affwebservices/public/saml2sso?SPID=https:/ /api.licensing.nvidia.com), or through this [other Google source](https://cloud.google.com/compute/docs/gpus/grid-drivers-table#windows_drivers) where you can use the Download only the *.exe* or *.run* file for Windows or Linux. 
    
Then it is inserted into the **USB memory** in order to connect them to the virtual desktop using the **SPICE viewer**.

![](./gpu.images/gpu9.png)

As it is a desktop with a **GPU profile assigned** but without reservation (it can be done but is not necessary), the desktop is accessed through the Isard **Administration panel**:

![](./gpu.images/gpu10.png){width=50%}
![](./gpu.images/gpu11.png){width=40%}
![](./gpu.images/gpu12.png){width=50%}

And **copy the file** to the **admin - installers** folder of the **Windows** desktop:

![](./gpu.images/gpu13.png)

The file is **executed** and in the new installation window the following steps are marked:

![](./gpu.images/gpu14.png){width=45%}
![](./gpu.images/gpu15.png){width=45%}
![](./gpu.images/gpu16.png){width=45%}
![](./gpu.images/gpu17.png){width=45%}
![](./gpu.images/gpu18.png){width=45%}
![](./gpu.images/gpu19.png){width=45%}

!!! warning "**DO NOT PRESS RESTART NOW.** **Restart later** is checked and, very important, the desktop is **manually shut down**."

##### Initial parameters

Without messing with the desktop and **without starting it**, **edit** it from IsardVDI to **change** the parameters so that it looks like the image below. It is very important to disable the previous viewers and leave only **RDP (Client) and RDP (Browser)**; The **Video** parameter must have the **Only GPU** option selected:

![](./gpu.images/gpu50.png)

##### Definitive test

The desktop is then **started again** and accessed using any **RDP viewer**. 

Next, you must check that the **NVIDIA Control Panel** appears in the Windows **toolbar** in the lower right corner, which will certify the correct installation of the drivers and compatibility with the equipment, **this may take a few minutes for Windows to start all services**:

![](./gpu.images/gpu22.png){width=35%}
![](./gpu.images/gpu23.png){width=35%}
![](./gpu.images/gpu24.png){width=60%}

#### Installation errors

??? question "NVIDIA control panel does not open / NVIDIA started icon is not displayed"

    If the panel is **not shown** and/or **cannot be opened**, you must **uninstall the 3 programs** using **Control Panel - Programs - Uninstall a program**:

    ![](./gpu.images/gpu25.png){width=45%}
    ![](./gpu.images/gpu26.png){width=45%}
    ![](./gpu.images/gpu27.png){width=45%}

    Turn the computer **manually off**, start it again and follow the [driver installation guide](./gpu.md/#driver-installation) again. 

    **The desktop is not edited** to set the [initial parameters](./gpu.md/#initial-parameters); after uninstalling the packages and shutting down the desktop, it is again accessed using **RDP viewer** and with the **Only GPU** parameter for **Video**.

    ![](./gpu.images/gpu22.png){width=35%}
    ![](./gpu.images/gpu23.png){width=35%}
    ![](./gpu.images/gpu24.png){width=60%}

    Repeat uninstalling and installing the packages until the **NVIDIA Control Panel** icon appears and can be opened.

??? question "Not detecting the NVIDIA driver"

    Make sure that the server driver and what is installed on the desktop are compatible, they have to go hand in hand because otherwise it will not work.

    Ex: if the server has version 15.2, any version 15.X must be installed on the desktop (preferably 15.2)

### Token management

To add the NVIDIA license token, you first need to download it from the [NVIDIA portal](https://nvid.nvidia.com/login).

Once we have the token, we must enter it on the virtual desktop. This can be done by downloading the file from a cloud storage service, from the NVIDIA portal, or via a USB device (either through the [Spice viewer or RDP viewer](../user/viewers/viewers.md))

- **Windows**

The token should be placed in the folder located at the following path:

```C:\Program Files\NVIDIA Corporation\vGPU Licensing\ClientConfigToken```

To verify that the token is working correctly, open a ```cmd``` and type ```nvidia-smi -q```

Where you have to scroll until you find the next section. If the **License Status** shows **Licensed** with an expiration date, this will confirm that the Windows client license has been successfully applied.

If you have graduated correctly, this will come out:

![](./gpu.images/gpu28.png){width=60%}

If you have not graduated correctly, this comes out:

![](./gpu.images/gpu29.png){width=60%}

- **Linux**

You have to move the token to the ```/etc/nvidia/ClientConfigToken``` directory, and change the permissions:

```
sudo mv client_configuration_token_*.tok /etc/nvidia/ClientConfigToken
sudo chmod 744 /etc/nvidia/ClientConfigToken/client_configuration_token_*.tok
```

If the **gridd.conf** file does not exist, it is created from the **gridd.conf.template** file (nothing in the file has to be changed):

```
sudo cp gridd.conf.template gridd.conf
```

It starts and the command is written to the terminal

```
nvidia-smi -q
```

Where you have to scroll until you find the next section. If the **License Status** shows **Licensed** with an expiration date, this will confirm that the Linux client license has been successfully applied.

![](./gpu.images/gpu30.png){width=60%}

#### Token errors

??? question "Does not read license token"

    Ensure that desktop date/time synchronization does not fail.
    If the time is not well synchronized, it will fail. You have to restart the server or reboot once the time has changed.

    ```
    cff15b928d2e   registry.gitlab.com/isard/isardvdi/hypervisor:v12-18-14   "/src/start.sh"          3 weeks ago
    ```

    You have to look at the error in the nvidia log. When Windows starts up it gets the UTC time, it updates and it gets it right.

    ```
    Windows Licensing Event Logs

    On Windows, licensing events are logged in the plain-text file %SystemDrive%\Users\Public\Documents\NvidiaLogging\Log.NVDisplay.Container.exe.log.

    The log file is rotated when its size reaches 16 MB. A new log file is created and the old log file is renamed to Log.NVDisplay.Container.exe.log1. Each time the log file is rotated, the number in the file name of each existing old log file is increased by 1. The oldest log file is deleted when the number of log files exceeds 16. 
    ```

    This is the [grid license user manual](https://docs.nvidia.com/grid/17.0/grid-licensing-user-guide/index.html#licensing-event-logs) where it explains the logs.

### Create/edit desktop/deployment

!!! warning "In order to book and start a desktop with a gpu profile, the administrator must have previously scheduled it"

[Create](create_desktop.md)/[edit desktop](edit_desktop.md) or [create/edit a deployment](../advanced/deployments.md) with the previous points on this manual done, move to **Viewers** section and make sure to check the boxes with **RDP** viewers. 

![](./gpu.images/gpu33.png)

!!! info "It is important to know that it only works with RDP viewers"

    **IMPORTANT!**: If there is a problem with the operating system and we never have an IP address, we will not be able to access via RDP. This should always be avoided, but, even more so in this section, forcing the desktop to shut down from the IsardVDI interface can cause disk corruption (since it is the equivalent of removing the power cable from a powered-on computer) and it may not be able to start the operating system and obtain an IP address, which could leave us without access to the desktop.

You also have to change some settings in the **Hardware** section.

It is important that the **Videos** setting has the **Only GPU** option checked. On **desktops with Windows OS with NVIDIA drivers installed, it is not supported to have both a QXL video card and a vGPU card.** 

In the following **Bookings** section below, the available GPU **profile** is assigned.

!!! example ""
    **Windows** can only use **RDP viewers**, but **Linux** allows you to use both **RDP/Spice viewers** and **video** in **Default**.

![](./gpu.images/gpu50.png)

### Start now or book

There are two ways to start the desktop with GPU: the first is to use the "start now" function, which starts the desktop immediately if GPU profiles are available. The second option is through a reservation, which requires booking the desktop in advance defined by the IsardVDI administrator.

#### Start now

To start the desktop immediately, you can see that it tells us that "The desktop with vGPU is not booked." To create the desktop reservation, press the "start" button. A dialog window will appear with time options:

![](./gpu.images/gpu44.png){width=45%}
![](./gpu.images/gpu47.png){width=60%}

##### Graphics card available

If the graphics card is available, the desktop will boot without problems and display the end date and time of the booking. Once that date/time is reached, the desktop will turn off and the reservation will be automatically deleted.

![](./gpu.images/gpu44.png){width=60%}


##### Graphics card not available

If the graphics card is not available, a modal will appear with two options: change the graphics card with one that is available or reserve the desktop with the current graphics card for when it is available.

![](./gpu.images/gpu39.png)

#### Booking

To book a desktop or deployment, consult this section of the manual: [Bookings](../user/bookings.md)

