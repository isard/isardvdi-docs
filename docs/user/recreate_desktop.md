# Recreate desktop

!!! warning
    This action will reset the desktop to its initial state, removing all changes made to it.

This action is only available in desktops that are part of a deployment with the appropriate permissions.

To recreate a desktop, press the button ![Recreate desktop icon](./recreate_desktop.images/button.png)

![Desktop card](./recreate_desktop.images/card.png)

![Recreate notification](./recreate_desktop.images/notification.png)