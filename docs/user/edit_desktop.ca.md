# Editar escriptori

Es poden editar els següents apartats apart del nom de l'escriptori, la seva descripció, i una imatge descriptiva.

Per a poder editar un escriptori es prem la icona ![](edit_desktop.es.images/edit_desktop3.png)

![](edit_desktop.ca.images/edit_desktop1.png){width="90%"}


## Visors

En aquest apartat es poden seleccionar quins [visors](../user/viewers/viewers.ca.md) es volen utilitzar per **accedir** a l'escriptori. També es pot seleccionar si es vol iniciar l'escriptori en pantalla completa o no.

![](create_desktop.ca.images/visors_1.png){width="90%"}


## Login RDP

En aquest apartat s'assigna l'**usuari i contrasenya** de login per als **visors RDP** (usuari creat al S.O. de l'escriptori). Aquesta configuració només és necessària si no es vol estar identificant-se en l'escriptori per RDP tota l'estona i si el visor RDP està seleccionat.

![](create_desktop.ca.images/login_rdp_1.png){width="60%"}


## Hardware

En aquest apartat es pot editar el maquinari que es vulgui tenir a l'escriptori:

- **Videos**: per defecte **sempre es selecciona la opció Default**
- **Boot**: 
    - **Hard disk** si hi ha un sistema operatiu instal·lat a l'escriptori
    - **CD/DVD** si s'assigna un **[mitjà](../../advanced/media.ca)** i es vol iniciar des d'aquest
- **Disk bus**: per defecte **sempre es selecciona la opció Default**
- **Xarxes**: llistat d'interfícies de xarxa que es volen a l'escriptori

![](create_desktop.ca.images/hardware_1.png){width="90%"}


## Reservables

En aquest apartat es pot seleccionar un **perfil de targeta de GPU** que es vulgui associar a l'escriptori (pot ser que no hi hagi disponibles).

![](create_desktop.ca.images/hardware_2.png){width="90%"}


## Mitjans

En aquest apartat se li pot afegir un [mitjà](../../advanced/media.ca) de l'usuari o que estigui compartit amb aquest.

![](create_desktop.ca.images/hardware_3.png){width="90%"}

## Imatge

En aquesta secció es pot seleccionar la **imatge de portada** que tindrà l'escriptori a la vista principal.

![](./edit_desktop.ca.images/imatge.png){width="90%"}