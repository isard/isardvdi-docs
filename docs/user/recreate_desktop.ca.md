# Recrear escriptori

!!! warning "Avís"
    Aquesta acció restablirà l'escriptori al seu estat inicial, eliminant tots els canvis realitzats.

Aquesta acció només està disponible en escriptoris que formen part d'un desplegament amb els permisos adequats.

Per recrear un escriptori, prem el botó ![Icona recrear escriptori](./recreate_desktop.images/button.png)

![Desktop card](./recreate_desktop.ca.images/card.png)

![Recreate notification](./recreate_desktop.ca.images/notification.png)