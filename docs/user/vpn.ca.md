# VPN 

## Per a què serveix la connexió VPN a IsardVDI?

Es estableix el **túnel VPN** d'usuari a IsardVDI perquè l'usuari pugui **accedir als seus escriptoris mitjançant el [visor RDP VPN](../viewers/viewers.ca/#com-utilitzar-els-visors-rdp-i-rdp-vpn)** o simplement per poder **establir comunicació entre la màquina amfitriona i els escriptoris de l'usuari** (p. ex. accedir-hi mitjançant una connexió SSH).


## Com establir el túnel VPN d'usuari

En primer lloc, la interfície de xarxa **Wireguard VPN** ha d'estar activa a tots els escriptoris que es volen fer visibles en el túnel VPN, ja sigui [editant el seu **Hardware**](../edit_desktop.ca/#hardware) o [creant-los directament amb aquesta **interfície de xarxa**](../create_desktop.ca/#vista-principal).

![](vpn.ca.images/1.png){width="35%"}

En segon lloc, s'accedeix al menú desplegable de l'usuari i s'hi selecciona l'opció **VPN**, que descarregarà un fitxer ***.conf*** a l'equip amfitrió.

![](vpn.ca.images/2.png){width="80%"}

Per establir la connexió s'utilitzarà el programa **WireGuard**. Depenent del sistema operatiu de l'equip amfitrió, es descarrega, s'instal·la i s'utilitza el programa, com s'explica a continuació.


### Amb amfitrió Linux

Després de descarregar el fitxer ***.conf*** de VPN, mitjançant la terminal s'executen les següents comandes:

```
$ sudo apt install wireguard
$ sudo mv /ruta/de/descàrrega/isard-vpn.conf /etc/wireguard
$ wg-quick up isard-vpn
```

El túnel VPN de l'usuari ja està establert correctament. Per provar que funciona, es poden **arrencar els escriptoris** de l'usuari i fer **ping** des de l'amfitrió cap a l'adreça IP d'aquests:

![](vpn.ca.images/3.png){width="30%"}

```
root@debian:~# ping 10.2.91.193
PING 10.2.91.193 (10.2.91.193) 56(84) bytes of data.
64 bytes from 10.2.91.193: icmp_seq=1 ttl=63 time=13.6 ms
64 bytes from 10.2.91.193: icmp_seq=2 ttl=63 time=6.15 ms
64 bytes from 10.2.91.193: icmp_seq=3 ttl=63 time=6.46 ms
64 bytes from 10.2.91.193: icmp_seq=4 ttl=63 time=6.89 ms
^C
--- 10.2.91.193 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 6.146/8.275/13.601
```


### Amb amfitrió Windows

Es descarrega **[WireGuard per a Windows a través d'aquest enllaç](https://download.wireguard.com/windows-client/wireguard-installer.exe)**, s'obre el programa i s'importa l'arxiu ***.conf*** de VPN recentment descarregat.

![](vpn.ca.images/4.png){width="60%"}

![](vpn.ca.images/5.png){width="60%"}

![](vpn.ca.images/6.png){width="60%"}

El túnel VPN de l'usuari ja està establert correctament. Per provar que funciona, es poden **arrencar els escriptoris** de l'usuari i fer **ping** des de l'amfitrió cap a l'adreça IP d'aquests:

![](vpn.ca.images/3.png){width="30%"}

```
C:\Users\windows>ping 10.2.91.193

Fent ping a 10.2.91.193 amb 32 bytes de dades:
Resposta des de 10.2.91.193: bytes=32 temps=13ms TTL=116
Resposta des de 10.2.91.193: bytes=32 temps=14ms TTL=116
Resposta des de 10.2.91.193: bytes=32 temps=14ms TTL=116
Resposta des de 10.2.91.193: bytes=32 temps=13ms TTL=116

Estadístiques de ping per a 10.2.91.193:
    Paquets: enviats = 4, rebuts = 4, perduts = 0
    (0% perduts),
Temps aproximat de tornada en mil·lisegons:
    Mínim = 13ms, Màxim = 14ms, Mitjana = 13ms

```

### Amb amfitrió Mac OS

Es descarrega el programa [Wireguard](https://www.wireguard.com/install/) des de l'AppStore i després l'arxiu isard-vpn.conf des de la plataforma. S'obre l'arxiu isard-vpn.conf amb l'editor de textos.

![](./vpn.es.images/vpn_mac1.png){width="70%"}

S'ha d'eliminar la línia ```PostUp = : ``` de l'arxiu i desar. Es va a la pestanya del Wireguard i se selecciona "Importar túnel des d'arxiu"

![](./vpn.es.images/vpn_mac2.png){width="30%"}

Seleccionem l'arxiu modificat i el pugem

![](./vpn.es.images/vpn_mac3.png){width="70%"}