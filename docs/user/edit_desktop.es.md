# Editar escritorio

Se pueden editar los siguientes apartados aparte del nombre del escritorio, su descripción, y una imagen descriptiva.

Para poder editar un escritorio se pulsa el icono ![](edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.es.images/edit_desktop4.png){width="90%"}


## Visores

En este apartado se puede seleccionar qué [visores](../user/viewers/viewers.es.md) se quieren utilizar para **acceder** al escritorio. También se puede seleccionar si se quiere arrancar el escritorio en pantalla completa o no.

![](./create_desktop.es.images/creacion5.png){width="90%"}


## Login RDP

En este apartado se asigna el **usuario y contraseña** de login para el **visor de RDP** (usuario creado en el S.O. del escritorio). Esta configuración sólo es necesaria si no se quiere estar identificándose en el escritorio por RDP todo el rato y si el visor RDP está seleccionado.

![](create_desktop.es.images/login_rdp.png){width="60%"}


## Hardware

En este apartado se puede editar el hardware que se quiera tener en el escritorio.

- **Videos**: por defecto **siempre se selecciona la opción Default**
- **Boot**: 
    - **Hard disk** si hay un sistema operativo instalado en el escritorio
    - **CD/DVD** si se asigna un **[medio](../advanced/media.es.md)** y se quiere iniciar desde éste
- **Disk bus**: por defecto **siempre se selecciona la opción Default**
- **Redes**: listado de interfaces de redes que se quieren al escritorio

![](create_desktop.es.images/hardware.png){width="90%"}


## Reservables

En este apartado se puede seleccionar un **perfil de tarjeta de GPU** que se quiera asociar al escritorio (puede ser que no hayan disponibles).

![](create_desktop.ca.images/hardware_2.png){width="90%"}


## Medio

En este apartado se le puede añadir un [medio](../advanced/media.es.md) del usuario o que esté compartida con éste.

![](create_desktop.es.images/media.png){width="90%"}


## Imagen

En este apartado se puede seleccionar la **imagen de portada** que tendrá el escritorio en la vista principal.

![](./edit_desktop.es.images/edit_desktop10.png){width="90%"}