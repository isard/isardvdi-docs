# Estat

Els escriptoris poden tenir diferents estats, que reflecteixen la seva disponibilitat actual per a l'usuari.

## En manteniment

Un escriptori pot estar **En Manteniment**. Aquest estat significa que s'està efectuant una operació al disc d'emmagatzematge de l'escriptori. Fins que l'acció acabi, el sistema impedeix que l'usuari iniciï l'escriptori per evitar problemes respecte a les seves dades. A sota de l'estat es mostra l'operació de disc específica que s'està realitzant en el disc d'emmagatzematge de l'escriptori. Aquestes accions poden haver sigut iniciades pel mateix usuari o per un administrador de la plataforma.

En alguns casos, el procés de manteniment pot trigar més del que s'esperava a causa d'un procés bloquejat. Si això passa, els usuaris poden fer clic al botó "Cancel·lar operació". Aquesta acció cancel·la la tasca en curs i hauria de tornar l'escriptori a un estat en el qual es pugui iniciar.

![Targeta d'escriptori en estat "En manteniment" amb un text que diu "S'està editant el registre"](./dektop_status.images/desktop_status_ca.png){width="25%"}

!!! Warning "Advertència"
    Cancel·lar l'operació del disc comporta un petit risc que el disc d'emmagatzematge es corrompi, i per tant que l'escriptori sigui inutilitzable.