# Reservas

En este apartado veremos cómo realizar reservas para los escritorios con tecnología vGPU asociadas a recursos GPUs de tarjetas NVIDIA, cómo añadir este tipo de hardware a los escritorios virtuales y la gestión de las reservas asociada.

!!! example ""
    Para más información de qué es una vGPU, cómo se prepara un escritorio, cómo se hace la instalación de los controladores NVIDIA, etc.., consultar el [apartado de vGPU](../user/gpu.es.md).


## ¿Por qué un sistema de reservas?

IsardVDI está pensado para que los usuarios de forma autónoma puedan crear y desplegar escritorios, el límite de cuántos escritorios pueden arrancar de forma simultánea en el sistema viene condicionado por los recursos disponibles. Si los escritorios no tienen vGPUs, el límite suele venir por la cantidad de memoria RAM disponible en los hipervisores, siendo aconsejable que siempre la capacidad de memoria y vCPUs de dichos servidores sea suficiente para sostener la concurrencia de escritorios. Podemos limitar la cantidad de recursos que otorgamos a cada usuario, grupo o categoría con las cuotas y límites que IsardVDI permite establecer.

En el caso de las vGPUs, son recuros caros (está el coste de la tarjeta y el coste del licenciamiento) y para acceder a estos recursos se ha establecido un sistema de reservas, con las siguientes características:

- El administrador puede **planificar horarios con difererentes perfiles** aplicados para cada tarjeta disponible, permitiendo que en un determinado horario, se establezca un perfil con mucha memoria dedicada y pocos usuarios (p. ej., lanzar una renderización), y en otro horario, otro perfil con poca memoria y muchos usuarios (p. ej., realizar una formación)

- Existe un **sistema de permisos** granular que permite definir qué usuarios tienen permiso a usar un determinado perfil de una tarjeta. Permite que sólo un grupo reducido de usuarios puedan acceder a los perfiles con más memoria

- Un usuario puede reservar un escritorio para una determinada franja horaria con un **tiempo mínimo previo a la reserva** y una **duración máxima de la reserva**. Permite que se puedan hacer reservas con poco tiempo de antelación previo y que no puedan reservarse franjas superiores a número reducido de horas

- Un usuario avanzado puede hacer una **reserva para un despliegue**, reservando tantas unidades de perfiles de GPU como escritorios contiene el despliegue

- Existe también un sistema de **prioridades de reservas**, que permite poner reglas para que unos usuarios puedan sobreescribir y revocar reservas de otros. Esta opción es útil si queremos fomentar que la tarjeta GPU se utilice por muchos usuarios, pero queremos a la vez garantizar que para unos determinados grupos nunca les falte capacidad de reservas, como por ejemplo, si un grupo usa un programa de diseño 3D de forma constante, y puedan mediante esta función revocar reservas de otros usuarios


## Reservar un escritorio/despliegue

Es necesario realizar las **reservas** de los **escritorios** con vGPU. El administrador habrá planificado unos perfiles de GPU en las diferentes tarjetas disponibles y el usuario podrá hacer reservas siempre que tenga permisos y coincida el perfil de su vGPU con una planificación disponible.

Se accede a reservar una GPU para el escritorio desde el último icono de acciones de éste. 

Para reservar un despliegue se accede mediante el panel de despliegues, con mismo botón, que está a la derecha del despliegue.

![](bookings.images/bookables1.png){width=30%}

![](bookings.images/bookables4.png)

Nos da acceso a la vista semanal de la **disponibilidad** (podemos cambiar la vista a mensual o diaria), donde encontramos dos columnas por cada día de la semana. En la columna de la izquierda aparece la disponibilidad para el perfil de tarjeta, y en la columna de la derecha es donde aparecerán las reservas que tenemos para ese escritorio o despliegue.

En la figura se aprecia que existe **disponibilidad** durante la semana y no hay hecha **ninguna** reserva.

![](bookings.images/booking7.png)

Las reservas pueden crearse mediante el botón de la esquina superior derecha ![](bookings.images/booking10.png){width=95} o mediante el cursor, **clicando en la franja Reservas y arrastrando** para seleccionar el rango de hora deseado. En el formulario que aparecerá podremos ajustar el rango de fechas y horas de duración de la reserva que queremos realizar.

![](bookings.images/booking12.png){width=60%}

Una vez hecha la reserva, aparecerá en la columna de la derecha de cada jornada. 

![](bookings.images/booking17.png){width=20%}


## Eliminar reserva

!!! info 
    No se puede modificar una reserva que ya está en curso.

Para eliminar una reserva, se presiona en la franja gris donde está la reserva y se pulsa el botón de eliminar que aparece al editarla.

![](bookings.images/booking20.png){width=60%}


## Videotutoriales

Aquí ofrecemos dos videotutoriales que explican las configuraciones previas de un escritorio o despliegue, y cómo realizar reservas para ambas opciones, con los **dos métodos de reserva que existen en IsardVDI**, en la primera parte y segunda parte respectivamente.

Subtítulos en español disponibles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>