# Estado

Los escritorios pueden tener diferentes estados, lo que refleja su disponibilidad actual para el usuario.

## En mantenimiento

Un escritorio puede estar **En Mantenimiento**. Este estado significa que se está efectuando una operación en el disco de almacenamiento del escritorio. Hasta que la acción finalice, el sistema impide que el usuario inicie el escritorio para evitar posibles problemas en sus datos. Debajo del estado se mostrará la operación específica del disco que se está realizando en el disco de almacenamiento del escritorio. Estas acciones pueden haber sido iniciadas por el propio usuario o por un administrador de la plataforma.

En algunos casos, el proceso de mantenimiento puede tardar más de lo esperado debido a un proceso atascado. Si esto ocurre, los usuarios tienen la opción hacer clic en el botón "Cancelar operación". Esta acción cancela la tarea en curso y debería devolver el escritorio a un estado en el que se pueda iniciar.

![Tarjeta de escritorio en estado "En mantenimiento" con un texto que dice "Se está editando el registro"](./dektop_status.images/desktop_status_es.png){width="25%"}

!!! Warning "Advertencia"
    Cancelar la operación del disco conlleva un pequeño riesgo de que el disco de almacenamiento se corrompa, y. por lo tanto, que el escritorio sea inutilizable.