# Emmagatzematge

Des de la vista principal de l'usuari, a **Emmagatzematge** es pot obtenir més d'informació sobre els discs dels escriptoris que s'han creat, així com el consum d'aquests i de plantilles.

![](./storage.ca.images/storage2.png)

En aquest apartat podem veure:

1. **Escriptoris/plantilles**: El nom de l'escriptori/plantilla. Si passes el ratolí per sobre del nom, veuràs l'ID de l'emmagatzematge.
2. **Mida ocupada**: Mida que ocupa el disc de l'escriptori o plantilla en GB.

!!! Info
    La **mida ocupada** no té en compte la mida del disc de la plantilla. Comença a ocupar la mida del disc en el moment de la creació.

3. **Mida total**: Mida total del disc que es pot utilitzar.
4. **% Quota**: Indica el percentatge de disc que s'ha consumit respecte a la quota que té l'usuari.
5. **Últim accés**: Indica l'últim cop que es va accedir a l'escriptori o es va crear la plantilla.

## Incrementar l'espai del disc

![](./storage.ca.images/storage3.png)

Els usuaris amb un rol d'Avançat o superior poden augmentar la mida total de l'emmagatzematge dels seus escriptoris. Per fer-ho, feu clic al botó d'incrementar![Botó d'incrementar l'espai del disc](./storage.images/storage5.png) i apareixerà un modal on podeu introduir la nova mida que voleu assignar al escriptori.

![Modal d'incrementar l'espai del disc](./storage.ca.images/storage4.png)
