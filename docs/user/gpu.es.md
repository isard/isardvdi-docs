# vGPU

IsardVDI permite trabajar con la tecnología de **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** que permite disponer de recursos de una tarjeta gráfica asociados a un escritorio virtual. Esta tecnología nos permite poder ejecutar software que requiere **recursos de GPU dedicados**, como programas de diseño 3D, animación, edición de vídeo, CAD o diseño industrial.

Cada tarjeta gráfica puede dividirse en diferentes perfiles con una cantidad de memoria reservada. En función del perfil seleccionado se dispone de más o menos tarjetas virtuales. En el siguiente gráfico se observa un servidor con una serie de tarjetas GPU instaladas. Cada tarjeta puede dividirse en vGPUs (virtual GPUs) que pueden mapearse a cada máquina virtual.

![](./gpu.images/gpu1.png){width=60%}

Veamos con un ejemplo el funcionamiento. Se dispone de una tarjeta **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** y se quiere usar un programa como el SolidWorks para diseñar piezas 3D. Se utiliza un perfil con 2GB de memoria gráfica dedicada. 
Esta tarjeta gráfica dispone de 48GB de memoria, que puede dividirse en perfiles "2Q": el primer carácter "2" hace referencia a la cantidad de memoria reservada (2GB) y el segundo carácter "Q" hace referencia al licenciamiento vWS, el modo en que la tarjeta virtual se configura para trabajos creativos y técnicos que usan la tecnología **[Quadro de Nvidia](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)**. 

Los diferentes perfiles y modos de funcionamiento disponibles para esta tarjeta están accesibles en el [manual de nvidia con los perfiles para la A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). En el caso de 2Q tenemos:

- Memoria dedicada y reservada: 2GB
- Número máximo de vGPUs por GPU: 24

Diagramas de funcionamiento de la tecnología Nvidia vGPU:

![](./gpu.images/gpu2.png){width=50%}

![](./gpu.images/gpu3.png){width=60%}

Para más información sobre el uso de la tecnología GPU sobre escritorios en IsardVDI, consultar el [apartado de reservas y perfiles](../../user/bookings.es.md).

## Tipo de perfiles

La cantidad de GPU físicas que tiene una placa depende de la placa. Cada GPU física puede admitir varios tipos diferentes de GPU virtual (vGPU). Los tipos de vGPU tienen una cantidad fija de búfer de fotogramas, una cantidad de cabezales de pantalla compatibles y resoluciones máximas. Se agrupan en diferentes perfiles según las distintas clases de carga de trabajo para las que están optimizados. Cada perfil se identifica con la última letra del nombre del tipo de vGPU. Para [más información](https://docs.nvidia.com/vgpu/10.0/grid-vgpu-user-guide/index.html).

|Perfiles||Carga de trabajo óptima|
|----||----|
|Q||Para profesionales creativos y técnicos que requieren el rendimiento y las características de la tecnología Quadro.|
|C||Cargas de trabajo de servidores con uso intensivo de computación, como inteligencia artificial (IA), aprendizaje profundo o computación de alto rendimiento|
|B||Para profesionales de negocios y trabajadores del conocimiento|
|A||Streaming de aplicaciones o soluciones basadas en sesiones para usuarios de aplicaciones virtuales|

## Cómo preparar un escritorio/plantilla

### Instalación de controladores

En esta guía se explica paso a paso el proceso de instalación de los **controladores NVIDIA como cliente (guest)** para un escritorio virtual en la plataforma IsardVDI** conectado a un **servidor con tarjetas GPU NVIDIA**.

#### Requisitos en esta guía

- Servidor IsardVDI con los **controladores de NVIDIA de anfitrión** previamente instalados (host) 
- Escritorio virtual con sistema operativo **Windows 10/11**
- Archivo ejecutable instalador de los controladores de NVIDIA
- Memoria USB


#### Pasos previos    

Se debe averiguar la **versión del controlador NVIDIA anfitrión del servidor** para poder instalar la **misma versión** en el escritorio virtual para que exista una compatibilidad entre ambas máquinas. 

!!! note "El administrador del servidor de IsardVDI tiene que proporcionar la información de la versión del controlador de NVIDIA"

El servidor tiene como sistema operativo una distribución Linux, así que en la terminal se escribe el comando ```nvidia-smi -q|grep -i "Driver Version"``` y se recoge la primera fila de la respuesta del comando donde se refleja la versión de los controladores:

```
root@servidor:/home/isard# nvidia-smi -q|grep -i "Driver Version"
Wed Jan  4 08:51:17 2023
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 510.47.03    Driver Version: 510.47.03    CUDA Version: N/A      |
|-------------------------------+----------------------+----------------------+
```

El equivalente en sistema operativo **Windows** a la versión de controladores **510.467.03** es la **511.65**, como muestra esta tabla de comparaciones de la web oficial de NVIDIA:

![](./gpu.images/gpu4.png){width=80%}

> Si el servidor donde apunta el escritorio virtual a configurar tiene alguna de estas tarjetas gráficas, los controladores host deberán estar actualizados a estas versiones mínimo. Sino, pueden existir problemas de compatibilidad y/o en el proceso de instalación de los controladores cliente en el escritorio:

> ![](./gpu.images/gpu5.png)

#### Escritorio

##### Parámetros básicos

Tener un Windows o un Linux como sistema operativo, sobre una plantilla base con dicho sistema preinstalado y preconfigurado, **sin** ningún **controlador** de pantalla ni de targeta gráfica instalados. **Si existen controladores instalados, se deben desinstalar para dejar preparado el sistema y poder realizar los siguientes pasos, sino, no funcionará.**

![](./gpu.images/gpu48.png){width=80%}

Se **edita el escritorio** con los parámetros de la siguiente imagen, que se modificarán posteriormente (no tienen relevancia los parámetros vCPUs ni Memory (GB)):

![](./gpu.images/gpu53.png)

##### Instalación 

Como se explica anteriormente, para esta práctica se necesita tener el **archivo instalador** de controladores cliente correspondiendo al sistema operativo del escritorio virtual.

Se **descargan** en el equipo local, mediante el listado de descarga de software disponible en el [panel web de NVIDIA Enterprise](https://nvid.nvidia.com/affwebservices/public/saml2sso?SPID=https://api.licensing.nvidia.com), o mediante esta [otra fuente de Google](https://cloud.google.com/compute/docs/gpus/grid-drivers-table#windows_drivers) donde se puede recurrir a la descarga de solo el archivo *.exe* o *.run* para Windows o Linux. 
    
Luego se introduce en la **memoria USB** para así conectarlos al escritorio virtual mediante el **visor SPICE**.

![](./gpu.images/gpu9.png)

Al ser un escritorio con un **perfil GPU asignado** pero sin reservar (se puede hacer pero no es necesario) se accede al escritorio mediante el **panel de Administración** de Isard:

![](./gpu.images/gpu10.png){width=50%}
![](./gpu.images/gpu11.png){width=40%}
![](./gpu.images/gpu12.png){width=50%}

Y se **copia el archivo** a la carpeta **admin - instaladores** del **Windows** del escritorio:

![](./gpu.images/gpu13.png)

Se **ejecuta** el archivo y en la nueva ventana de instalación se marcan los siguientes pasos:

![](./gpu.images/gpu14.png){width=45%}
![](./gpu.images/gpu15.png){width=45%}
![](./gpu.images/gpu16.png){width=45%}
![](./gpu.images/gpu17.png){width=45%}
![](./gpu.images/gpu18.png){width=45%}
![](./gpu.images/gpu19.png){width=45%}

!!! warning "**NO PULSAR REINICIAR AHORA.** Se marca **Reiniciar más tarde** y, muy importante, se **apaga manualmente** el escritorio."

##### Parámetros iniciales

Sin tocar el escritorio y **sin volver a arrancarlo**, se **edita** desde IsardVDI para **cambiar** los parámetros y que quede como a continuación en la imagen. Muy importante desactivar los visores anteriores y dejar únicamente **RDP (Client) y RDP (Browser)**; el parámetro **Video** tiene que tener la opción **Only GPU** seleccionada:

![](./gpu.images/gpu49.png)

##### Prueba definitiva

A continuación se **inicia de nuevo** el escritorio y se accede a él mediante cualquier **visor RDP**. 

Seguidamente hay que comprobar que el **Panel de Control de NVIDIA** aparece en la **barra de herramientas** de Windows en la esquina inferior derecha, lo que certificará la correcta instalación de los controladores y la compatibilidad con el equipo, **esto puede llevar unos minutos hasta que Windows arranque todo los servicios**:

![](./gpu.images/gpu22.png){width=35%}
![](./gpu.images/gpu23.png){width=35%}
![](./gpu.images/gpu24.png){width=60%}

#### Errores de instalación

??? question "No se abre el panel de control de NVIDIA / no se muestra el icono de que esté NVIDIA iniciado"

    De **no mostrarse** y/o **no poder abrirse** el panel, hay que **desinstalar los 3 programas** mediante **Panel de Control - Programas - Desinstala un programa**:

    ![](./gpu.images/gpu25.png){width=45%}
    ![](./gpu.images/gpu26.png){width=45%}
    ![](./gpu.images/gpu27.png){width=45%}

    Se **apaga manualmente** el equipo, se inicia y se realiza de nuevo la [instalación de los controladores](./gpu.es.md/#instalación-de-controladores). 

    **No se edita el escritorio** para ponerle los [parámetros iniciales](./gpu.es.md/#parámetros-iniciales-1); después de desinstalar los paquetes y apagar el escritorio, se vuelve a acceder a él mediante **visor RDP** y con el parámetro **Only GPU** para **Video**.

    ![](./gpu.images/gpu22.png){width=35%}
    ![](./gpu.images/gpu23.png){width=35%}
    ![](./gpu.images/gpu24.png){width=60%}

    Repetir la desinstalación e instalación de los paquetes hasta que apareza el icono del **Panel de control de NVIDIA** y pueda abrirse.

??? question "No detecta el controlador de NVIDIA"

    Asegurarse de que el controlador del servidor y que se instala en el escritorio son compatibles, tienen que ir a la par porque sino no funciona.

    Ej: si el servidor tiene la versión 15.2 al escritorio se le tiene que instalar cualquier versión 15.X (preferiblemente 15.2)


### Gestión del token

Para añadir el token de licencia de NVIDIA, primero necesitamos descargarlo desde el [portal de NVIDIA](https://nvid.nvidia.com/login).

Una vez tengamos el token, debemos introducirlo en el escritorio virtual. Esto puede hacerse descargando el archivo desde un servicio de almacenamiento en la nube, desde el portal de NVIDIA, o mediante un dispositivo USB (ya sea a través del [visor Spice o visor RDP](../user/viewers/viewers.es.md))

- **Windows**

El token debe colocarse en la carpeta ubicada en la siguiente ruta:

```C:\Program Files\NVIDIA Corporation\vGPU Licensing\ClientConfigToken```

Para verificar que el token funcione correctamente, se abre un ```cmd``` y se escribe ```nvidia-smi -q```

Donde se tiene que desplazar hasta encontrar el siguiente apartado. Si el **License Status** muestra **Licensed** con una fecha de vencimiento, esto confirmará que la licencia del cliente de Windows se ha aplicado correctamente.

Si se ha licenciado bien saldrá esto:

![](./gpu.images/gpu28.png){width=60%}

Si no se ha licenciado bien sale esto:

![](./gpu.images/gpu29.png){width=60%}

- **Linux**

Se tiene que mover el token al directorio ```/etc/nvidia/ClientConfigToken```, y se cambian los permisos:

```
sudo mv client_configuration_token_*.tok /etc/nvidia/ClientConfigToken
sudo chmod 744 /etc/nvidia/ClientConfigToken/client_configuration_token_*.tok
```

Si no existe el archivo **gridd.conf** se crea a partir del archivo **gridd.conf.template** (no se tiene que moficiar nada del archivo):

```
sudo cp gridd.conf.template gridd.conf
```

Se incia y a la terminal se escribe el comando

```
nvidia-smi -q
```

Donde se tiene que desplazar hasta encontrar el siguiente apartado. Si el **License Status** muestra **Licensed** con una fecha de vencimiento, esto confirmará que la licencia del cliente de Linux se ha aplicado correctamente.

![](./gpu.images/gpu30.png){width=60%}

#### Erorres de token

??? question "No lee el token de licencia"

    Asegurarse de que no falle la sincronización de la fecha/hora del escritorio.
    Si la hora no está bien sincronizada pues falla. Hay que reiniciar el servidor o reiniciar una vez ha cambiado la hora.

    ```
    cff15b928d2e   registry.gitlab.com/isard/isardvdi/hypervisor:v12-18-14   "/src/start.sh"          3 weeks ago
    ```

    El error hay que mirarlo en el log de nvidia. Cuando arranca el windows pilla la hora UTC, se actualiza y ya lo pilla bien.

    ```
    Windows Licensing Event Logs

    On Windows, licensing events are logged in the plain-text file %SystemDrive%\Users\Public\Documents\NvidiaLogging\Log.NVDisplay.Container.exe.log.

    The log file is rotated when its size reaches 16 MB. A new log file is created and the old log file is renamed to Log.NVDisplay.Container.exe.log1. Each time the log file is rotated, the number in the file name of each existing old log file is increased by 1. The oldest log file is deleted when the number of log files exceeds 16. 
    ```

    Este es el [manual de usuario de licencia grid](https://docs.nvidia.com/grid/17.0/grid-licensing-user-guide/index.html#licensing-event-logs) donde explica los logs.

### Crear/editar escritorio/despliegue

!!! warning "Para poder reservar e iniciar un escritorio con un perfil de gpu, el administrador debe haberlo planificado previamente"

[Crear](create_desktop.es.md)/[editar escritorio](edit_desktop.es.md) o [crear/editar un desplieuge](../advanced/deployments.es.md) con los puntos anteriores hechos, desplazarse al apartado de **Visores** y asegurarse de marcar las casillas con los visores **RDP**. 

![](./gpu.images/gpu31.png)

!!! info "Es importante saber que solamente funciona con los visores RDP"

    **¡IMPORTANTE!**: Si hay un problema con el sistema operativo y nunca llegamos a tener dirección IP, no podremos acceder por RDP. Siempre hay que evitar esto, pero, con más razón en este apartado, forzar el apagado del escritorio desde la interfaz de IsardVDI, puede provocar una corrupción del disco (ya que es el equivalente a quitar el cable de corriente de un ordenador encendido) y que éste no consiga iniciar el sistema operativo y obtener dirección IP, con lo cual nos podremos quedar sin acceso al escritorio.

También se tiene que cambiar algunas configuraciones en el apartado **Hardware**.

Es importante que la configuración **Videos** tenga la opción **Only GPU** marcada. En **los escritorios con S.O. Windows con los controladores de NVIDIA instalados, no es compatible tener a la vez una tarjeta de vídeo QXL y una tarjeta vGPU.** 

En el siguiente apartado **Reservables** más abajo, se asigna el **perfil** de GPU disponible.

!!! example ""
    Los **Windows** solamente podrán utilizar los **visores RDP**, pero los **Linux** permiten utilizar los **visores Spice/RDP** y el **vídeo** en **Default**.

![](./gpu.images/gpu49.png)


### Start now o reserva

Existen dos formas de iniciar el escritorio con GPU: la primera es utilizar la función "start now" o "arranque al momento", que arranca el escritorio de inmediato si hay perfiles de GPU disponibles. La segunda opción es a través de una reserva, que requiere reservar el escritorio con una antelación definida por el administrador de IsardVDI.

#### Start now

Para iniciar el escritorio al momento, se puede ver que nos indica que "El escritorio con vGPU no está reservado". Para crear la reserva del escritorio, se pulsa el botón de "iniciar". Saldrá una ventana de diálogo con opciones de horas:

![](./gpu.images/gpu34.png){width=45%}
![](./gpu.images/gpu35.png){width=60%}


##### Tarjeta gráfica disponible

Si la tarjeta gráfica está disponible, el escritorio se iniciará sin problemas y mostrará la fecha y hora de finalización de la reserva. Una vez alcanzada esa fecha/hora, el escritorio se apagará y la reserva se eliminará automáticamente.

![](./gpu.images/gpu36.png){width=60%}


##### Tarjeta gráfica no disponible

Si la tarjeta gráfica no está disponible, aparecerá un modal con dos opciones: cambiar la tarjeta gráfica por una que esté disponible o reservar el escritorio con la tarjeta gráfica actual para cuando esté disponible.

![](./gpu.images/gpu37.png)

#### Reserva

Para reservar un escritorio o despliegue, consulta esta sección del manual: [Reservas](../user/bookings.es.md)

