# Inici ràpid

Hi ha dos tipus d'escriptoris:

* **Temporal**: Quan s'aturi un escriptori, **es perdrà** tot allò que s'hagi fet, desat o descarregat.
* **Persistent**: Quan s'aturi, **no es perdran** les dades o programaris que s'hagin instal·lat. 

## Vista principal

Per a crear un escriptori es prem el botó ![](create_desktop.ca.images/crear1.png){width="8%"} de la vista **Escriptoris**.

![](create_desktop.ca.images/crear2.png){width="80%"}

S'emplenen els camps, es **selecciona una plantilla** i es prem el botó ![](create_desktop.ca.images/crear6.png){width="6%"}.

![](create_desktop.ca.images/crear3.png){width="80%"}

Es pot canviar la configuració de l'escriptori a l'apartat **Opcions avançades**:

!!! info inline end "Enllaços"
        <font size="3">
            <ul>
            <li> [Visors](../user/edit_desktop.ca.md/#visors)</li>
            <li> [Login RDP](../user/edit_desktop.ca.md/#login-rdp)</li>
            <li> [Hardware](../user/edit_desktop.ca.md/#hardware)</li>
            <li> [Reservables](../user/edit_desktop.ca.md/#reservables)</li>
            <li> [Mitjans](../user/edit_desktop.ca.md/#media)</li>
            </ul>
        </font>

![](create_desktop.ca.images/visors_1.png){width="60%"}
![](create_desktop.ca.images/login_rdp_1.png){width="60%"}
![](create_desktop.ca.images/hardware_1.png){width="60%"}
![](create_desktop.ca.images/hardware_2.png){width="60%"}
![](create_desktop.ca.images/hardware_3.png){width="60%"}
