# Xarxes

Els escriptoris a IsardVDI poden disposar de múltiples targetes de xarxa simultàniament, i això permet configuracions avançades i flexibles. Aquestes targetes de xarxa poden ser de diferents tipus, cosa que proporciona una gran versatilitat per adaptar-se a diverses necessitats de connectivitat i rendiment a la plataforma.

!!! warning "Rols amb accés"
    Les [xarxes les crea l'usuari amb rol **administrador**](../admin/domains_resources.ca.md/#interfícies) del sistema i els altres usuaris hauran de demanar permís per crear-ne de noves

## Default

La xarxa Default permet la connexió dels escriptoris virtuals a Internet amb les característiques següents:

!!! info inline "Característiques"
    - Una adreça dinàmica (DHCP)
    - Servidor DNS
    - Passarel·la a través d'un router NAT
    - Estan aïllats d'altres escriptoris del sistema

![](./networks.images/networks1.png){width="50%"}

## Wireguard VPN

Una VPN ens permet comunicar equips per un canal xifrat extrem a extrem.

### Personal

La VPN personal permet connectar el PC de l'usuari amb els vostres escriptoris virtuals de forma segura i permet per exemple:

!!! info inline "Característiques"
    - Poder fer servir l'escriptori virtual com a servidor i el nostre ordinador com a client o al revés.
    - Poder connectar-nos per ssh cap als escriptoris d'Isard.
    - Compartir fitxers usant la compartició de xarxa de windows, nfs, scp, rsync...
    - Poder connectar-nos per RDP a través d'aquesta VPN (hi ha un visor per utilitzar aquesta configuració)

![](./networks.images/networks2.png){width="50%"}

Per tal d'obrir la VPN personal, heu d'anar a aquest [apartat](../user/vpn.ca.md) del manual.

!!! note 
    - Només es permet **una mateixa connexió VPN simultània**, si es té la VPN configurada en diferents equips, només es podrà tenir activa en una alhora.
    - Els Escriptoris virtuals estan aïllats d'altres escriptoris del sistema.

### Grup

!!! note ""
    Servidor de llicències o fitxers externs via VPN cap a la plataforma.

Es pot enllaçar un servidor extern via VPN permetent que a través del sistema de permisos usuaris o grups hi puguin accedir a través de la xarxa de VPN (Necessària intervenció de l'admin o suport d'IsardVDI)

![](./networks.images/networks3-2.png){width="50%"}

!!! note
    - Les **regles de tallafocs permeten o deneguen l'accés al servidor extern** per usuari o per grup.

## Xarxes personals

Les xarxes personals permeten que els escriptoris de l'usuari estiguin a la mateixa xarxa (VLAN):

!!! info inline "Característiques"
    - Amb xarxes personals, els usuaris poden configurar un entorn de xarxa privat per als seus escriptoris virtuals
    - Poden assignar les seves pròpies adreces IP, crear la configuració de la xarxa i controlar l'accés a la xarxa. 
    - Permet replicar les mateixes configuracions de xarxa entre usuaris en pràctiques de xarxes i tenir un entorn aïllat
    - Es poden preparar plantilles amb una ip fixa en aquesta xarxa i no entra en conflicte quan es creen escriptoris d'aquesta plantilla

![](./networks.images/networks4.png){width="50%"}

Es pot tenir més d'una xarxa personal per fer pràctiques de tallafocs, proxys, routing:

![](./networks.images/networks5.png){width="70%"}

## Xarxes de grup o privades

Les xarxes de grup o privades (OpenVSwitch) permeten connectar a la mateixa xarxa (VLAN) tots els escriptoris virtuals que l'hagin connectat, independentment del propietari de l'escriptori virtual.

!!! info inline "Característiques"
    - També es poden connectar a la mateixa xarxa VLAN en infraestructura, permetent així connectar els ordinadors físics a escriptoris virtuals
    - Pensades perquè es comuniquin escriptoris de diferents usuaris
    - Les adreces IP no es poden repetir si són usuaris diferents, ja que els escriptoris són a la mateixa xarxa
    - Es poden combinar amb la xarxa VPN personal i amb xarxes personals per fer pràctiques de xarxes complexes

![](./networks.images/networks6-2.png){width="50%"}

## Xarxes Mixtes

Es poden combinar amb la xarxa VPN personal i amb xarxes personals per fer pràctiques de xarxes complexes

![](./networks.images/networks7-2.png){width="70%"}

!!! note ""
    - Casos d'ús amb diferents tipus d'interfícies de xarxa:
        - [**Client - Entorn del servidor**](./client_server/client_server.ca.md): com crear una xarxa **Private (OVS)** i utilitzar-la entre el rol **advanced** i el rol **user**.
        - [**Proves de xarxa**](./network_tests/network_tests.ca.md): s'introdueixen diferents xarxes i com configurar-les als escriptoris virtuals Linux.
        - [**Xarxes personals**](./private_and_personal_networks/private_and_personal_networks.ca.md): com utilitzar les xarxes privades i les xarxes personals de l'usuari (des d'un usuari amb rol **administrador**).
