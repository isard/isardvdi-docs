# Bastion

The Bastion feature allows access to virtual desktops through the same server running the IsardVDI web interface, instead of relying exclusively on a VPN connection (Wireguard).

This is particularly useful in environments where public servers are required, as it allows these desktops to be accessible without needing a VPN.

When the Bastion is activated, the IsardVDI server acts as a gateway for external connections:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

The accessible ports for the desktops are:

- **2222/TCP:** SSH Service
- **80/TCP:** Web
- **443/TCP:** Web TLS


## Configuration

In the desktop configuration section, there is a **bastion** option if it is enabled on the system.

![](./bastion.images/bastion2.png){width="60%"}

The username and password set for RDP connection will also be used by the IsardVDI bastion to connect to the desktop.

Although the external ports are those mentioned above, within the desktop, we can redirect them to other ports if desired.

To use the SSH connection, one or more public SSH keys (one per line) must be added for each user requiring access. Each user must have their own generated public keys.

Once activated, save the form.

If the form is reopened, the UUID of the bastion will appear, which will allow connections to the desktop.

![](./bastion.images/bastion.png){width="50%"}

In the main desktop section, the button ![](./bastion.ca.images/bastion3.png) can be clicked to see the UUID along with the URLs for the activated protocols.

With this UUID, the following connections can be made to the desktop:

- **SSH:** ssh 62f3a39b-40b8-43f1-a486-05182b625fc8@domain.com -p 2222
- **HTTP:** http://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com
- **HTTPS:** https://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com

Where "domain.com" corresponds to the domain of the server hosting the IsardVDI web interface.

!!! info "Notes"
    If you want to access the web services, it is recommended to open the URL in an incognito window or a different browser if you have already visited the IsardVDI web interface.

## Examples

### Web service

Example of testing a web service inside a desktop.

During the test, to avoid other issues, we will disable the firewall:

- Disable firewall with `systemctl disable --now ufw`
- Install SSH server `apt install openssh-server` and enable with `systemctl enable --now ssh`

![](./bastion.ca.images/bastion4.png){width="80%"}

Generate a LetsEncrypt certificate in the desktop. First, try with `--dry-run`, and once successful, run it without the option to generate the certificate:

```bash
apt install cerbot

certbot certonly --agree-tos --email info@example.com -d 62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com --dry-run
```

With the generated certificates, you can start any web server. To test, we'll create an `index.html` file with some content and set up a simple `server.py` to run the server:

```python
import http.server
import ssl
import socketserver

LETS_PATH = "/etc/letsencrypt/live/62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com/"
PORT = 443
DIRECTORY = "."

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

httpd = socketserver.TCPServer(('0.0.0.0', PORT), Handler)

# Paths to your certificate and key files
CERT_FILE = LETS_PATH + "fullchain.pem"
KEY_FILE = LETS_PATH + "privkey.pem"

# Wrap the server's socket with SSL
httpd.socket = ssl.wrap_socket(httpd.socket,
                               certfile=CERT_FILE,
                               keyfile=KEY_FILE,
                               server_side=True)

print(f"Serving on https://0.0.0.0:{PORT}")
httpd.serve_forever()
```

Run:

```bash
python3 server.py
```

![](./bastion.ca.images/bastion5.png){width="80%"}

Now, visit the web service at the corresponding URL, and you should see the content from `index.html`.

![](./bastion.ca.images/bastion6.png){width="80%"}
