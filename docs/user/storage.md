# Storage

From the home page you can go to the "Storage" section to get a little more information about the desktops that have been created like the consumption of the desktops and templates.

![](./storage.images/storage1.png)

![](./storage.images/storage2.png)

In this section we can see:

1. **Desktops/templates**: The name given to the desktop/template. If you hover over the name, you will see the storage ID.
2. **Used Size**: Occupied size of the desktop on disk.

!!! Info
    The **Used size** provided in this section does not take into account the disk size of the template. It begins to occupy the size of the disk at the time of creation.

3. **Total Size**: Total size of the disk that can be used.
4. **% Quota**: Indicates the percentage of disk that has been consumed in respect to the quota that the user has.
5. **Last Access**: Indicates the last date the desktop was accessed or the template was created.

## Increase the storage disk space

![](./storage.images/storage3.png)

Users with a role of Advanced and above can increase the total storage size of their desktops. To do this, click on the Increase![Increase storage button](./storage.images/storage5.png) button and a modal will appear where you can enter the new size you want to assign to that desktop.

![Increase storage modal](./storage.images/storage4.png)
