# Redes

Los escritorios en IsardVDI pueden disponer de múltiples tarjetas de red simultáneamente, permitiendo así configuraciones avanzadas y flexibles. Estas tarjetas de red pueden ser de diferentes tipos, lo que proporciona una gran versatilidad para adaptarse a diversas necesidades de conectividad y rendimiento en la plataforma.

!!! warning "Roles con acceso"
    Las [redes las crea el usuario con rol **administrador**](../admin/domains_resources.es.md/#interfaces) del sistema y el resto de usuarios deberán pedir permiso para crear nuevas.

## Default

La red Default permite la conexión de los escritorios virtuales a Internet con las siguientes características:

!!! info inline "Características"
    - Una dirección dinámica (DHCP)
    - Servidor DNS
    - Pasarela a través de un router NAT
    - Están aislados de otros escritorios del sistema

![](./networks.images/networks1.png){width="50%"}

## Wireguard VPN

Una VPN nos permite comunicar equipos por un canal cifrado extremo a extremo.

### Personal

La VPN personal permite conectar el PC del usuario con sus escritorios virtuales de forma segura y permite por ejemplo:

!!! info inline "Características"
    - Poder usar el escritorio virtual como servidor y nuestro ordenador como cliente o al revés.
    - Poder conectarnos por ssh hacia los escritorios de Isard.
    - Compartir ficheros usando la compartición de red de windows, nfs, scp, rsync...
    - Poder conectarnos por RDP a través de esta VPN (existe un visor para usar esta configuración)

![](./networks.images/networks2.png){width="50%"}

Para obrtener la VPN personal, se tiene que ir a este [apartado](../user/vpn.es.md) del manual.

!!! note 
    - Sólo se permite **una misma conexión VPN simultánea**, si se tiene la VPN configurada en diferentes equipos, sólo se podrá tener activa en una a la vez
    - Los Escritorios virtuales están aislados de otros escritorios del sistema.

### Grupo

!!! note ""
    Servidor de licencias o ficheros externos vía VPN hacia la plataforma.

Se puede enlazar un servidor externo vía VPN permitiendo que a través del sistema de permisos usuarios o grupos puedan acceder a él a través de la red de VPN (Necesaria intervención del admin o soporte de IsardVDI)

![](./networks.images/networks3.png){width="50%"}

!!! note
    - Las **reglas de firewall permiten o deniegan el acceso al servidor externo** por usuario o por grupo.

## Redes personales

Las redes personales permiten que los escritorios del usuario estén en la misma red (VLAN):

!!! info inline "Características"
    - Con redes personales, los usuarios pueden configurar un entorno de red privado para sus escritorios virtuales
    - Pueden asignar sus propias direcciones IP, crear la configuración de la red y controlar el acceso a la red. 
    - Permite replicar las mismas configuraciones de red entre usuarios en prácticas de redes y tener un entorno aislado
    - Se puede preparar plantillas con una ip fija en esa red y no entra en conflicto cuando se crean escritorios de esta plantilla

![](./networks.images/networks4.png){width="50%"}

Se puede tener más de una red personal para realizar prácticas de cortafuegos, proxys, routing:

![](./networks.images/networks5.png){width="70%"}

## Redes de grupo o privadas

Las redes de grupo o privadas (OpenVSwitch) permiten conectar en la misma red (VLAN) todos los escritorios virtuales que lo hayan conectado, independientemente del propietario del escritorio virtual.

!!! info inline "Características"
    - También se pueden conectar en la misma red VLAN en infraestructura, permitiendo así conectar los ordenadores físicos a escritorios virtuales
    - Pensadas para que se comuniquen escritorios de distintos usuarios
    - Las direcciones IP no se pueden repetir si son usuarios distintos, ya que los escritorios están en la misma red
    - Se pueden combinar con la red VPN personal y con redes personales para realizar prácticas de redes complejas

![](./networks.images/networks6.png){width="50%"}

## Redes Mixtas

Se pueden combinar con la red VPN personal y con redes personales para realizar prácticas de redes complejas

![](./networks.images/networks7.png){width="70%"}

!!! note ""
    - Casos de uso con diferentes tipos de interfaces de red:
        * [**Cliente - Entorno de servidor**](./client_server/client_server.es.md): como crear una red **Private (OVS)** y utilizarla entre el rol **avanzado** y el rol de **usuario**.
        * [**Pruebas de red**](./network_tests/network_tests.es.md): se introducen diferentes redes y como configurarlas en los escritorios virtuales Linux.
        * [**Redes personales**](./private_and_personal_networks/private_and_personal_networks.es.md): como utilizar las redes privadas y las redes personales del usuario (desde un usuario con rol **administrador**).
