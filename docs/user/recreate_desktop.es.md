# Recrear escritorio

!!! warning "Aviso"
    Esta acción restablecerá el escritorio a su estado inicial, eliminando todos los cambios realizados.

Esta acción solo está disponible en escritorios que forman parte de un despliegue con los permisos adecuados.

Para recrear un escritorio, presiona el botón ![Icono recrear escritorio](./recreate_desktop.images/button.png)

![Desktop card](./recreate_desktop.es.images/card.png)

![Recreate notification](./recreate_desktop.es.images/notification.png)