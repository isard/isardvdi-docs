# Reserves

En aquest apartat veurem com fer reserves per als escriptoris amb tecnologia vGPU associades a recursos GPUs de targetes Nvidia, com afegir aquest tipus de maquinari als escriptoris virtuals i la gestió de les reserves associada.

!!! example ""
    Per a més informació de què és una vGPU, com es prepara un escriptori, com es fa la instal·lació dels controladors NVIDIA, etc., consultar l'[apartat de vGPU](../user/gpus.ca.md).


## Per què un sistema de reserves

IsardVDI està pensat perquè els usuaris de forma autònoma puguin crear i desplegar escriptoris, el límit de quant escriptoris poden córrer de forma simultània en el sistema ve condicionat pels recursos disponibles. Si els escriptoris no tenen vGPUs, el límit sol venir per la quantitat de memòria RAM disponible en els hipervisors, essent aconsellable que sempre la capacitat de memòria i vCPUs d'aquests servidors sigui suficient per sostenir la concorrència d'escriptoris. Podem limitar la quantitat de recursos que atorguem a cada usuari, grup o categoria amb les quotes i límits que IsardVDI permet establir.

En el cas de les vGPUs, són recursos cars (hi ha el cost de la targeta i el cost del llicenciament) i per accedir a aquests recursos s'ha establert un sistema de reserves, amb les següents característiques:

- L'administrador pot **planificar horaris amb diferents perfils** aplicats per a cada targeta disponible, permetent que en un determinat horari, s'estableixi un perfil amb molta memòria dedicada i pocs usuaris (p. ex.: llançar una renderització), i en un altre horari, un altre perfil amb poca memòria i molts usuaris (p. ex.: realitzar una formació).

- Existeix un **sistema de permisos** granular que permet definir quins usuaris tenen permís per usar un determinat perfil d'una targeta. Permet que només un grup reduït d'usuaris pugui accedir als perfils amb més memòria.

- Un usuari pot reservar un escriptori per a una determinada franja horària amb un **temps mínim previ** a la reserva i una **durada màxima de la reserva**. Permet que es puguin fer reserves amb poc temps d'antelació i que no es puguin reservar franges superiors a un nombre reduït d'hores.

- Un usuari avançat pot fer una reserva **per a un desplegament**, reservant tantes unitats de perfils de GPU com escriptoris conté el desplegament.

- Existeix també un sistema de **prioritats de reserves**, que permet posar regles perquè uns usuaris puguin sobreescriure i revocar reserves d'altres. Aquesta opció és útil si volem fomentar que la targeta GPU s'utilitzi per molts usuaris, però volem alhora garantir que per a uns determinats grups mai els falti capacitat de reserves, com per exemple, si un grup


## Reservar un escriptori/desplegament

Cal realitzar les **reserves** dels **escriptoris** amb vGPU. L'administrador haurà planificat uns perfils de GPU a les diferents targetes disponibles i l'usuari podrà fer reserves sempre que tingui permisos i coincideixi el perfil del vGPU amb una planificació disponible.

S'accedeix a reservar una GPU per a l'escriptori des de la darrera icona d'accions.

Per reservar un desplegament s'accedeix mitjançant el panell de desplegaments, amb el mateix botó, que està a la dreta del desplegament.

![](bookings.images/bookables2.png){width=30%}

![](bookings.images/bookables5.png)

Ens dona accés a la vista setmanal de la **disponibilitat** (podem canviar la vista mensual o diàriament), on trobem dues columnes per cada dia de la setmana. A la columna de l'esquerra apareix la disponibilitat per al perfil de targeta, i a la columna de la dreta és on apareixeran les reserves que tenim per a aquest escriptori o desplegament.

A la figura s'aprecia que hi ha **disponibilitat** durant la setmana i no hi ha feta **cap** reserva.

![](bookings.images/booking8.png)

Les reserves es poden crear mitjançant el botó del cantó superior dret ![](bookings.images/booking11.png){width=95} o mitjançant el cursor, **clicant a la franja Reserves i arrossegant** per seleccionar el rang d'hora desitjat. Al formulari que apareixerà podrem ajustar el rang de dates i hores de durada de la reserva que volem realitzar.

![](bookings.images/booking15.png){width=60%}

Un cop feta la reserva, apareixerà a la columna de la dreta de cada jornada.

![](bookings.images/booking18.png){width=20%}

## Eliminar reserva

!!! info
    No podeu modificar una reserva que ja està en curs.

Per eliminar una reserva, es prem a la franja grisa on hi ha la reserva i es prem el botó d'eliminar que apareix en editar-la.

![](bookings.images/booking21.png){width=60%}

## Videotutorials

Aquí oferim dos videotutorials que expliquen les configuracions prèvies d'un escriptori o desplegament, i com fer reserves per a ambdues opcions, amb els **dos mètodes de reserva que hi ha a IsardVDI**, a la primera part i segona part respectivament.

Subtítols en català disponibles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

