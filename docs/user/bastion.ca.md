# Bastió

El bastió d'IsardVDI és una funcionalitat que permet accedir als escriptoris virtuals a través del mateix servidor on s'executa al web d'IsardVDI, en comptes de dependre exclusivament d'una connexió VPN (Wireguard).

Ës especialment útil en entorns on es necessiten servidors públics, ja que permet que aquests escriptoris siguin accessibles sense necessitat d'una VPN.

Quan s'activa el bastió, el servidor d'IsardVDI fa de porta d'enllaç per a les connexions externes:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

Els ports per defecte que són accessibles dels escriptoris són:

- **2222/TCP:** Servei SSH
- **80/TCP:** Web
- **443/TCP:** Web TLS

## Configuració

A l'apartat d'edició de qualsevol escriptori es troba l'apartat de **bastió**, si l'opció està activada al sistema.

![](./bastion.ca.images/bastion.png){width="60%"}

L'usuari i la contrasenya configurats per a la connexió RDP seran els mateixos que utilitzarà el Bastió d'IsardVDI per connectar-se al nostre escriptori.

Tot i que els ports externs són els que s'han indicat anteriorment, dins de l'escriptori podem redirigir-los a altres ports, si ho desitgem.

Per tal de fer servir la connexió per SSH, cal afegir una o més claus públiques SSH (una per línia) per a cada usuari que necessiti accés. Cada usuari ha de tenir les seves pròpies claus públiques generades.

Una vegada activat, es desa el formulari.

Si es torna a obrir el formulari podrem veure que apareix l'UUID del bastió, que permetrà les connexions a l'escriptori.
![](./bastion.ca.images/bastion2.png){width="50%"}

En retornar a l'apartat principal d'escriptoris, es podrà accedir al botó ![](./bastion.ca.images/bastion3.png) per veure l'UUID juntament amb les URLs per als protocols activats

Amb aquest UUID es generaran les diferents connexions a aquest escriptori:

- **SSH:** ssh 62f3a39b-40b8-43f1-a486-05182b625fc8@domain.com -p 2222
- **HTTP:** http://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com
- **HTTPS:** https://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com

On "domain.com" correspon el domini del servidor on es troba la web d'IsardVDI.

!!! info "Notes"
    Si es vol accedir als serveis web, es recomana obrir la URL en una finestra d'incògnit o en un altre navegador si ja s'ha visitat prèviament la web d'IsardVDI.

## Exemples

### Servei web

Exemple d'una prova d'un servei web dins d'un escriptori.

Durant la prova, per evitar altres problemàtiques, desactivarem el tallafocs:

- Desactivar firewall `systemctl disable --now ufw`
- Instal.lar ssh server `apt install openssh-server` i activar-lo `systemctl enable --now ssh`

![](./bastion.ca.images/bastion4.png){width="80%"}

Generem a l'escriptori un certificat letsencrypt. Primer provem amb `--dry-run` i quan funcioni ho executem sense per generar-los:

```bash
apt install cerbot

certbot certonly --agree-tos --email info@example.com -d 62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com --dry-run
```

Amb els certificats generats, podem iniciar un servidor web qualsevol. Per provar farem un fitxer `index.html` amb qualsevol contingut i crearem un `server.py` amb aquest servidor senzill:

```python
import http.server
import ssl
import socketserver

LETS_PATH = "/etc/letsencrypt/live/62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com/"
PORT = 443
DIRECTORY = "."

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

httpd = socketserver.TCPServer(('0.0.0.0', PORT), Handler)

# Paths to your certificate and key files
CERT_FILE = LETS_PATH + "fullchain.pem"
KEY_FILE = LETS_PATH + "privkey.pem"

# Wrap the server's socket with SSL
httpd.socket = ssl.wrap_socket(httpd.socket,
                               certfile=CERT_FILE,
                               keyfile=KEY_FILE,
                               server_side=True)

print(f"Serving on https://0.0.0.0:{PORT}")
httpd.serve_forever()
```

Executem:

```bash
python3 server.py
```

![](./bastion.ca.images/bastion5.png){width="80%"}

I ens connectem al web on hauríem de veure el contingut que hem posat a `index.html`.

![](./bastion.ca.images/bastion6.png){width="80%"}
