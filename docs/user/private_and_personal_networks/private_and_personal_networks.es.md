# Redes privadas y personales

## Explicación

Una red **privada** se configura a múltiples escritorios a la vegada para establecer una comunicación entre máquinas de **múltiplos usuarios**. 

*- Ejemplo de casos de uso: arquitectura cliente-servidor entre escritorios y entre escritorio y máquina cliente.*

Una red **personal** se configura a múltiples escritorios a la vegada para establecer una comunicación entre máquinas del **mismo usuario**. 

*- Ejemplo de casos de uso: arquitectura cliente-servidor donde múltiples clientes usan la misma dirección IP.*

Se pueden **combinar** ambos tipos de red en un mismo escritorio y entorno virtual, así como también tarjetas de **salida a Internet**. Los escritorios virtuales de IsardVDI permiten también funcionar como ***routers** o **servidores** que salgan a la red con la tarjeta *WireGuard (crear una conexión **VPN** personal) y que sean así accesibles desde otros escritorios y/o desde la máquina cliente.

Se pueden **modificar los permisos de acceso** en estas redes para limitar qué usuarios o grupos pueden utilizarlas.

*- Ejemplo de casos de uso: arquitectura cliente-servidor *DHCP con dos escritorios respectivamente usando una red privada, permitiendo utilizar con posterioridad una red personal modificando el escritorio y sus redes.*


## Manual de usuario

### Rol advanced

#### Escritorio servidor

1.- [**Crear un escritorio**](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/#crear-escritorio) cogiendo plantilla base de ***Debian 11 *Desktop** configurada en IsardVDI

![](./private_and_personal_networks.images/0vZ63DU.png)


2.- Configurar redes (en el momento o [editarlo posteriormente](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#editar-escritorio)), añadiendo **‘*Defualt’** y **‘*badia1’** (**adaptador puente** y **privada** respectivamente)

![](./private_and_personal_networks.images/e4UOfKp.png)

![](./private_and_personal_networks.images/1V2ayAH.png)


3.- Arrancar el escritorio y [modificar la **configuración** de red](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware) de ambas tarjetas, correspondiendo la primera a ‘***Default**’ y la segunda a ‘***badia1**’ (la segunda tarjeta se configura con ***ip fija**)

![](./private_and_personal_networks.images/oihGAPD.png)

![](./private_and_personal_networks.images/VratvuJ.png)

![](./private_and_personal_networks.images/7J4O1kL.png)


> NOTA: la orden de tarjetas de red que se configuran en el escritorio al IsardVDI es la orden de tarjetas que aparecen configuradas al sistema del escritorio, por ejemplo, cuando se escribe al terminal el mando ```*ip -c a```.


4.- Actualizar los repositorios y **instalar el paquete ‘*apache2’**. Verificar su funcionamiento

![](./private_and_personal_networks.images/p9xAC9K.png)


#### Escritorio cliente

1.- [Crear un escritorio cogiendo plantilla base](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/#crear-escritorio) de ***Ubuntu 22.04 *Desktop** configurada a IsardVDI

![](./private_and_personal_networks.images/pYKsB50.png)


2.- [Configurar redes](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware), añadiendo solo la privada **‘*badia1’**

![](./private_and_personal_networks.images/bRbf6WK.png)


3.- Arrancar el escritorio y modificar la **configuración** de red

![](./private_and_personal_networks.images/rK3xhII.png)


- ***Ping al servidor**:

![](./private_and_personal_networks.images/W3AOtwN.png)


4.- Abrir un **navegador web** y verificar el acceso al **servidor web** mediante su **dirección IP**

![](./private_and_personal_networks.images/H6rCT5L.png)


5.- **Apagar** escritorios *adecuadamente* (desde el sistema, gráficamente o con mandos)


6.- [Convertir los escritorios a **plantilla**](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#plantillas), [habilitarlas (hacer **visible**)](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#hacer-visibleinvisible) y [**compartir** con los usuarios deseados](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#compartir) 

![](./private_and_personal_networks.images/Ej0xSKI.png)


- Se puede **hacer plantilla** también el **cliente** y así tener ya la configuración de red adecuada:

![](./private_and_personal_networks.images/qqO0Q8P.png)


### Rol user

#### Escritorios a partir de plantillas

Iniciar sesión a IsardVDI y comprobar la visibilidad de las plantillas creadas y compartidas por el **profesor**

![](./private_and_personal_networks.images/TNHdgNf.png)


> NOTA: crear un escritorio, si no se modifica, hereda las configuraciones de hardware de la plantilla. Se puede modificar al crear el escritorio o una vez ya creada con el botón de edición.


Se puede [**cambiar**](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#editar-escritorio), por ejemplo, la red privada **‘*badia1’** por la red **‘personal-bahía’** a **servidor** y a **cliente**, para que el alumno pueda realizar la práctica en un **segmento de red exclusivo**, y poder así cada alumno usar las **mismas direcciones IP sin colisionar**.

![](./private_and_personal_networks.images/gTwVVEH.png)

![](./private_and_personal_networks.images/dLx9n0z.png)

![](./private_and_personal_networks.images/iN98Rvq.png)

![](./private_and_personal_networks.images/dX5pLqa.png)

![](./private_and_personal_networks.images/FhUtMy6.png)

![](./private_and_personal_networks.images/b3gFOK2.png)