# Status

Desktops can have different statuses, reflecting their current availability to the user.

## In maintenance

A desktop can be in **In maintenance**. This status means that a storage disk operation is being performed on the desktop. Until the action finishes, the system prevents the user from starting the desktop to avoid any storage issues. Below the status text, the specific disk operation currently being performed on the desktop storage disk is displayed. These actions could have been triggered by the user or an administrator.

In some cases, the maintenance process may take longer than expected, possibly due to a stalled process. If this occurs, users can click the "Cancel operation" button. This action cancels the ongoing task and should return the desktop to an startable state.

![Desktop card with status "In maintenance" with a text that says "The registry is being edited"](./dektop_status.images/desktop_status.png){width="25%"}

!!! Warning Warning
    Cancelling the disk operation carries a small risk of rendering the storage disk corrupted, and therefore the desktop, unusable. Please be cautious.