# Networks

IsardVDI desktops can have multiple network cards simultaneously, thus allowing advanced and flexible configurations. These network cards can be of different types, providing great versatility to adapt to various connectivity and performance needs on the platform.

!!! warning "Role access"
    The [networks are created by the user with role **administrator**](../admin/domains_resources.md/#interfaces) of the system and the rest of the users must request permission to create new ones.

## Default

The Default network allows the connection of virtual desktops to the Internet with the following characteristics:

!!! info inline "Characteristics"
    - A dynamic address (DHCP)
    - DNS Server
    - Gateway through a NAT router
    - They are isolated from other desktops in the system

![](./networks.images/networks1.png){width="50%"}

## Wireguard VPN

A VPN allows us to communicate between computers through an end-to-end encrypted channel.

### Personal

The personal VPN allows you to connect the user's PC to their virtual desktops securely and allows, for example:

!!! info inline "Characteristics"
    - Being able to use the virtual desktop as a server and our computer as a client or vice versa.
    - Being able to connect via ssh to the Isard desktops.
    - Share files using Windows network sharing, nfs, scp, rsync...
    - Being able to connect by RDP through this VPN (there is a viewer to use this configuration)

![](./networks.images/networks2.png){width="50%"}

To obtain the personal VPN, you have to go to this [section](../user/vpn.md) of the manual.

!!! note 
    - Only **one simultaneous VPN connection** is allowed, if you have the VPN configured on different computers, it can only be active on one at a time
    - Virtual Desktops are isolated from other desktops on the system.

### Group

!!! note ""
    License server or external files via VPN to the platform.

An external server can be linked via VPN, allowing users or groups to access it through the VPN network through the permission system (IsardVDI admin or support intervention required)

![](./networks.images/networks3-3.png){width="50%"}

!!! note
    - **Firewall rules allow or deny access to the external server** per user or per group.

## Personal networks

Personal networks allow user desktops to be on the same network (VLAN):

!!! info inline "Characteristics"
    - With personal networks, users can set up a private network environment for their virtual desktops
    - They can assign their own IP addresses, create network configuration and control network access. 
    - Allows you to replicate the same network configurations between users in networking practices and have an isolated environment
    - You can prepare templates with a fixed IP on that network and it does not conflict when creating desktops from this template

![](./networks.images/networks4.png){width="50%"}

You can have more than one personal network to perform firewall, proxy, and routing practices:

![](./networks.images/networks5.png){width="70%"}

## Group or private networks

Group or private networks (OpenVSwitch) allow all virtual desktops that have connected it to be connected to the same network (VLAN), regardless of the owner of the virtual desktop.

!!! info inline "Characteristics"
    - They can also be connected to the same VLAN network in the infrastructure, thus allowing physical computers to be connected to virtual desktops
    - Designed for the desktops of different users to communicate
    - IP addresses cannot be repeated if they are different users, since the desktops are on the same network
    - Can be combined with personal VPN network and personal networks to perform complex networking practices

![](./networks.images/networks6-3.png){width="50%"}

## Mixed Networks

Can be combined with personal VPN and personal networks to perform complex networking practices

![](./networks.images/networks7-3.png){width="70%"}

!!! note ""
    - Use cases involving different types of network interfaces:
        - [**Client - Server environment**](./client_server/client_server.md): how to create a **Private OVS** network and use it between an advanced role and a user role.
        - [**Network tests**](./network_tests/network_tests.md): introduction of different IsardVDI networks and how to setup in Linux virtual desktops.
        - [**Personal networks**](./private_and_personal_networks/private_and_personal_networks.md): how to use the private networks and the user personal networks (from a user with **administrator** role).


