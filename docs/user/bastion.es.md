# Bastión

La funcionalidad _Bastión_ de IsardVDI permite acceder a los escritorios virtuales a través del mismo servidor donde se ejecuta la interfaz web de IsardVDI, en lugar de depender exclusivamente de una conexión VPN (Wireguard).

Es especialmente útil en entornos donde se requieren servidores públicos, ya que permite que estos escritorios sean accesibles sin necesidad de una VPN.

Cuando se activa el bastión, el servidor de IsardVDI actúa como un puente para las conexiones externas:

```mermaid
graph LR
  dt1(Client):::dt -.- dk1([IsardVDI Bastion]):::dk -.- dt2(Guest Desktop):::dt
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
```

Los puertos accesibles para los escritorios son:

- **2222/TCP:** Servicio SSH
- **80/TCP:** Web
- **443/TCP:** Web TLS

## Configuración

En la sección de configuración de cualquier escritorio, se encuentra la opción **bastión** si está activada en el sistema.

![](./bastion.es.images/bastion.png){width="60%"}

El nombre de usuario y la contraseña configurados para la conexión RDP serán los mismos que utilizará el bastión de IsardVDI para conectarse al escritorio.

Aunque los puertos externos son los mencionados anteriormente, dentro del escritorio podemos redirigirlos a otros puertos si lo deseamos.

Para usar la conexión SSH, se deben agregar una o más claves públicas SSH (una por línea) para cada usuario que necesite acceso. Cada usuario debe tener sus propias claves públicas generadas.

Una vez activado, se guarda el formulario.

Si se vuelve a abrir el formulario, aparecerá el UUID del bastión, lo que permitirá las conexiones al escritorio.

![](./bastion.es.images/bastion2.png){width="50%"}

Al volver a la sección principal de escritorios, se podrá acceder al botón ![](./bastion.ca.images/bastion3.png) para ver el identificador junto con las URLs para los protocolos activados.

Con este UUID, se pueden generar las siguientes conexiones al escritorio:

- **SSH:** ssh 62f3a39b-40b8-43f1-a486-05182b625fc8@domain.com -p 2222
- **HTTP:** http://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com
- **HTTPS:** https://62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com

Donde "domain.com" corresponde al dominio del servidor que aloja la web de IsardVDI.

!!! info "Nota"
    Si se quiere acceder a los servicios web, se recomienda abrir la URL en una ventana de incógnito o en otro navegador si ya se ha visitado previamente la web de IsardVDI.

## Ejemplos

### Servicio web

Ejemplo de prueba de un servicio web dentro de un escritorio.

Durante la prueba, para evitar otros problemas, desactivaremos el cortafuegos:

- Desactivar cortafuegos `systemctl disable --now ufw`
- Instalar servidor SSH `apt install openssh-server` y activarlo `systemctl enable --now ssh`

![](./bastion.ca.images/bastion4.png){width="80%"}

Generamos un certificado LetsEncrypt en el escritorio. Primero, probamos con `--dry-run`, y una vez funcione, lo ejecutamos sin esta opción para generar el certificado:

```bash
apt install cerbot

certbot certonly --agree-tos --email info@example.com -d 62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com --dry-run
```

Con los certificados generados, podemos iniciar cualquier servidor web. Para probar, crearemos un archivo `index.html` con algún contenido y configuraremos un sencillo `server.py` para ejecutar el servidor:

```python
import http.server
import ssl
import socketserver

LETS_PATH = "/etc/letsencrypt/live/62f3a39b-40b8-43f1-a486-05182b625fc8.domain.com/"
PORT = 443
DIRECTORY = "."

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

httpd = socketserver.TCPServer(('0.0.0.0', PORT), Handler)

# Paths to your certificate and key files
CERT_FILE = LETS_PATH + "fullchain.pem"
KEY_FILE = LETS_PATH + "privkey.pem"

# Wrap the server's socket with SSL
httpd.socket = ssl.wrap_socket(httpd.socket,
                               certfile=CERT_FILE,
                               keyfile=KEY_FILE,
                               server_side=True)

print(f"Serving on https://0.0.0.0:{PORT}")
httpd.serve_forever()
```

Ejecutar:

```bash
python3 server.py
```

![](./bastion.ca.images/bastion5.png){width="80%"}

Ahora, visitamos el servicio web en la URL correspondiente y deberíamos ver el contenido de `index.html`.

![](./bastion.ca.images/bastion6.png){width="80%"}
