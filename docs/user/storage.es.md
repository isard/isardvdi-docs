# Almacenamiento

Desde la página de inicio puedes ir a la sección "Almacenamiento" para obtener un poco más de información sobre los escritorios que se han creado como el consumo de los escritorios y plantillas.

![](./storage.es.images/storage1.png)

![](./storage.es.images/storage2.png)

En este apartado podemos ver:

1. **Escritorios/plantillas**: El nombre que se le ha dado al escritorio/plantilla. Si pasas el ratón por encima del nombre, verás el ID del almacenamiento.
2. **Tamaño ocupado**: Tamaño que ocupa el disco del escritorio o plantilla en GB.

!!! Info
    El **Tamaño ocupado** proporcionado en esta sección no tiene en cuenta el tamaño del disco de la plantilla. Comienza a ocupar el tamaño del disco en el momento de la creación.

3. **Tamaño total**: Tamaño total del disco que se puede utilizar.
4. **% Cuota**: Indica el porcentaje de disco que se ha consumido respecto a la cuota que tiene el usuario.
5. **Último acceso**: Indica la última vez que se accedió al escritorio o se creó la plantilla.

## Incrementar el espacio del disco

![](./storage.es.images/storage3.png)

Los usuarios con un rol de Avanzado o superior pueden aumentar el tamaño total del almacenamiento de sus escritorios. Para hacerlo, haz clic en el botón de Incrementar![Botón de incrementar almacenamiento](./storage.images/storage5.png) y aparecerá un modal donde puedes introducir el nuevo tamaño que deseas asignar a ese escritorio.

![Modal de incrementar almacenamiento](./storage.es.images/storage4.png)
